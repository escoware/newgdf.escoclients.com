<?php  
	
	// Php code for details of commision cycle
	$com_cycle_raw_query = "SELECT CommissionRunID FROM CommissionRunBrokers INNER JOIN CommissionRuns ON CommissionRunBrokers.CommissionRunID = CommissionRuns.ID WHERE BrokerId = {$_SESSION['life_user_id']} order by  CommissionRunID desc";

	$queryheading = mssql_query($com_cycle_raw_query);

	$resultheading = mssql_fetch_array( $queryheading );
	
    if($_REQUEST['comicycle'] == ''){
		$querypct = "Select SUM(CommissionAmount) as pct FROM dbo.BP2_CommissionRunDetails_GDF WHERE Level1BrokerID = {$_SESSION['life_user_id']} AND PaidBrokerID = {$_SESSION['life_user_id']} AND CommissionRunID = {$resultheading['CommissionRunID']} ";
	} 
	else{

		$querypct = "Select SUM(CommissionAmount) as pct FROM dbo.BP2_CommissionRunDetails_GDF WHERE Level1BrokerID = {$_SESSION['life_user_id']} AND PaidBrokerID = {$_SESSION['life_user_id']} AND CommissionRunID = {$_REQUEST['comicycle']}";
	}
	
	$datapct = mssql_query( $querypct );
	$resultpct = mssql_fetch_assoc( $datapct );



	// Queries for table data 
	if( $_REQUEST['order-by'] ){

		$order_key = explode('_', $_REQUEST['order-by']);
		$order_col = $order_key[0];
		$order_val = $order_key[1];
	}
	else{
		$order_col = 'CommissionRunDetailID';
		$order_val = 'asc';
	}

	$page = $_REQUEST['page'] ? (int)$_REQUEST['page'] : 1;

	$minlimit = ( $page > 1 ) ? ( ( ( $page - 1 ) * $num_of_rows_shown_in_table ) + 1 ) : 1;

	$maxlimit = $minlimit + $num_of_rows_shown_in_table;

	$query12 = mssql_query("SELECT CommissionRunID FROM CommissionRunBrokers INNER JOIN CommissionRuns ON CommissionRunBrokers.CommissionRunID = CommissionRuns.ID WHERE BrokerId = {$_SESSION['life_user_id']} order by CommissionRunID desc "); 
											
	$result2 = mssql_fetch_array( $query12 );

	// Main Query for table's Data
	$query_for_data = "Select * FROM dbo.BP2_CommissionRunDetails_GDF WHERE Level1BrokerID = {$_SESSION['life_user_id']} AND PaidBrokerID = {$_SESSION['life_user_id']}";

	if( $_REQUEST['comicycle'] == ''){
		$query_for_data .= " AND CommissionRunID = {$result2[0]}";
	} 
	else {
		$query_for_data .= " AND CommissionRunID = {$_REQUEST['comicycle']}";
	}

	if( $_REQUEST['search'] ){
		$search_key = explode('_', $_REQUEST['search']);
		$search_col = $search_key[0];
		$search_val = $search_key[1];

		$query_for_data .= " AND {$search_col} LIKE '%{$search_val}%'";
	}

	$query_for_data .= " ORDER BY {$order_col} {$order_val}";

	$rawsql_for_js = $query_for_data;

	$query_for_data .= " OFFSET 0 ROWS FETCH NEXT {$num_of_rows_shown_in_table} ROWS ONLY";

	$data_resource = mssql_query($query_for_data);

	// use raw query for js to find the total number of records
	$total_records_resource = mssql_query($rawsql_for_js);

	$total_data_table_rows = mssql_num_rows($data_resource);

	$total_rows = mssql_num_rows($total_records_resource);
?>