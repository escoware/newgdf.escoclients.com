<?php
	if( $total_data_table_rows > 0 ){

		echo '<tbody>';

		$i=0;

		while( $result = mssql_fetch_assoc($data_resource) ){

			$i++;

			if( $i % 2 == 0 ){
				$bgcolor = "#fff";
			}
			else{
				$bgcolor = "";
			}

			echo '<tr>';
			echo '<td class="CommissionRunDetailID_td"> '. $result['CommissionRunDetailID'] .' </td>';
			echo '<td class="CommissionRunID_td"> '. $result['CommissionRunID'] .' </td>';
			echo '<td class="CustomerFirstName_td"> '. $result['CustomerFirstName'] .' </td>';
			echo '<td class="CustomerLastName_td"> '. $result['CustomerLastName'] .' </td>';
			echo '<td class="CustomerCompanyName_td"> '. $result['CustomerCompanyName'] .' </td>';
			echo '<td class="ContractID_td"> '. $result['ContractID'] .' </td>';
			echo '<td class="ContractNumber_td"> '. $result['ContractNumber'] .' </td>';
			
			if( $result['EntryDate'] ){
				echo '<td class="EntryDate_td">'.date('m/d/Y',strtotime($result['EntryDate'])).'</td>';
			} 
			else {
			  	echo '<td class="EntryDate_td"></td>';
			}
			echo '<td class="CustomerTypeName_td"> '. $result['CustomerTypeName'] .' </td>';
			echo '<td class="UtilityAccountNumber_td"> '. $result['UtilityAccountNumber'] .' </td>';
			echo '<td class="MeterNumber_td"> '. $result['MeterNumber'] .' </td>';
			echo '<td class="UtilityName_td"> '. $result['UtilityName'] .' </td>';
			echo '<td class="StateCode_td"> '. $result['StateCode'] .' </td>'; 
			echo '<td class="CommodityName_td"> '.$result['CommodityName'] .' </td>';
			  
			if( $result['ServiceStartDate'] ) {
				echo '<td class="ServiceStartDate_td">'.date('m/d/Y',strtotime($result['ServiceStartDate'])).'</td>';
			} 
			else {
			  	echo '<td class="ServiceStartDate_td"></td>';
			}
			
			if( $result['ServiceEndDate'] ){
				echo '<td class="ServiceEndDate_td">'.date('m/d/Y',strtotime($result['ServiceEndDate'])).'</td>';
			} 
			else {
			  	echo '<td class="ServiceEndDate_td"></td>';
			}

		    echo '<td class="BilledEnergyUsage_td"> '. $result['BilledEnergyUsage'] .' </td>';
		    echo '<td class="CommissionRate_td"> '.(.001*($result['CommissionRate'])).'</td>';   
		    echo '<td class="RateUOM_td"> '. $result['RateUOM'] .' </td>'; 
			echo '<td class="CommissionAmount_td"> '.money_format('%(.2n', $result['CommissionAmount']) .' </td>'; 
			echo '<td class="PaymentTypeName_td"> '. $result['PaymentTypeName'] .' </td>'; 
			echo '<td class="PaidBrokerID_td"> '. $result['PaidBrokerID'] .' </td>'; 
			echo '<td class="PaidSigningAgentCode_td"> '. $result['PaidSigningAgentCode'] .' </td>'; 
			echo '<td class="PaidSigningAgentLevel_td"> '. $result['PaidSigningAgentLevel'] .' </td>'; 
			echo '<td class="PaidBrokerFirstName_td"> '. $result['PaidBrokerFirstName'] .' </td>'; 
			echo '<td class="PaidBrokerLastName_td"> '. $result['PaidBrokerLastName'] .' </td>'; 
			echo '<td class="PaidBrokerCompanyName_td"> '. $result['PaidBrokerCompanyName'] .' </td>'; 
			echo '<td class="Level1BrokerID_td"> '. $result['Level1BrokerID'] .' </td>'; 
			echo '<td class="Level1BrokerFirstName_td"> '. $result['Level1BrokerFirstName'] .' </td>'; 
			echo '<td class="Level1BrokerLastName_td"> '. $result['Level1BrokerLastName'] .' </td>'; 

			echo '<td class="Level1BrokerCompanyName_td"> '. $result['Level1BrokerCompanyName'] .' </td>'; 
			echo '<td class="Level1SigningAgentCode_td"> '. $result['Level1SigningAgentCode'] .' </td>'; 
			echo '<td class="SerivceAddress1_td"> '. $result['SerivceAddress1'] .' </td>'; 
			echo '<td class="ServiceAddress2_td"> '. $result['ServiceAddress2'] .' </td>'; 
			echo '<td class="SerivceCity_td"> '. $result['SerivceCity'] .' </td>'; 
			echo '<td class="ServiceCounty_td"> '. $result['ServiceCounty'] .' </td>'; 
			echo '<td class="ServiceStateCode_td"> '. $result['ServiceStateCode'] .' </td>'; 
			echo '<td class="ServiceZipCode_td"> '. $result['ServiceZipCode'] .' </td>'; 
			echo '<td class="ServiceCountry_td"> '. $result['ServiceCountry'] .' </td>'; 

			if( preg_match( '/^\+\d(\d{3})(\d{3})(\d{4})$/', str_ireplace('-', '', trim($result['HomePhone'])),  $matches ) ){
			    echo '<td class="HomePhone_td"> '. $matches[1] . '-' .$matches[2] . '-' . $matches[3].' </td>';
			}
			elseif( preg_match( '/^(\d{3})(\d{3})(\d{4})$/', str_ireplace('-', '', trim($result['HomePhone'])),  $matches ) ){
				echo '<td class="HomePhone_td"> '. $matches[1] . '-' .$matches[2] . '-' . $matches[3].' </td>';
			}
			else{
				echo '<td class="HomePhone_td"> '. $result['HomePhone'].'</td>';
			}

			if( preg_match( '/^\+\d(\d{3})(\d{3})(\d{4})$/', str_ireplace('-', '', trim($result['WorkPhone'])),  $matches ) ){
			    echo '<td class="WorkPhone_td"> '. $matches[1] . '-' .$matches[2] . '-' . $matches[3].' </td>';
			}
			elseif( preg_match( '/^(\d{3})(\d{3})(\d{4})$/', str_ireplace('-', '', trim($result['WorkPhone'])),  $matches ) ){
				echo '<td class="WorkPhone_td"> '. $matches[1] . '-' .$matches[2] . '-' . $matches[3].' </td>';
			}
			else{
				echo '<td class="WorkPhone_td"> '. $result['WorkPhone'] .'</td>';
			}

			if( preg_match( '/^\+\d(\d{3})(\d{3})(\d{4})$/', str_ireplace('-', '', trim($result['CellularPhone'])),  $matches ) ){
			    echo '<td class="CellularPhone_td"> '. $matches[1] . '-' .$matches[2] . '-' . $matches[3].' </td>';
			}
			elseif( preg_match( '/^(\d{3})(\d{3})(\d{4})$/', str_ireplace('-', '', trim($result['CellularPhone'])),  $matches ) ){
				echo '<td class="CellularPhone_td"> '. $matches[1] . '-' .$matches[2] . '-' . $matches[3].' </td>';
			}
			else{
				echo '<td class="CellularPhone_td"> '. $result['CellularPhone'].' </td>';
			}

			echo '<td class="AltPhone_td"> '. $result['AltPhone'] .' </td>'; 
			echo '<td class="Email_td"> '. $result['Email'] .' </td>'; 
			echo '<td class="AltEmail_td"> '. $result['AltEmail'] .' </td>'; 
			echo '<td class="ProductCode_td"> '. $result['ProductCode'] .' </td>'; 
			echo '<td class="RateCode_td"> '. $result['RateCode'] .' </td>'; 
			echo '<td class="ContractTerm_td"> '. $result['ContractTerm'] .' </td>'; 
			echo '<td class="BillingServiceStatus_td"> '. $result['BillingServiceStatus'] .' </td>'; 
			echo '<td class="BillingServiceDescription_td"> '. $result['BillingServiceDescription'] .' </td>'; 
			echo '<td class="ContractStatus_td"> '. $result['ContractStatus'] .' </td>'; 
			
			if( $result['AccountConfirmedStartDate'] ){
				echo '<td class="AccountConfirmedStartDate_td">'.date('m/d/Y',strtotime($result['AccountConfirmedStartDate'])).'</td>';
			} 
			else {
				echo '<td class="AccountConfirmedStartDate_td">'.$result['AccountConfirmedStartDate'] .' </td>';
			}

			if( $result['FlowEndDate'] ){
				echo '<td class="FlowEndDate_td">'.date('m/d/Y',strtotime($result['FlowEndDate'])).'</td>';
			} 
			else {
			  	echo '<td class="FlowEndDate_td"></td>';
			}

			if( $result['ContractEndDate'] ){
			 	echo '<td class="ContractEndDate_td">'.date('m/d/Y',strtotime($result['ContractEndDate'])).'</td>';
			} 
			else {
			  	echo '<td class="ContractEndDate_td"></td>';
			}

			echo '<td class="UtilityID_td"> '. $result['UtilityID'] .' </td>'; 
			echo '<td class="UtilityShortName_td"> '. $result['UtilityShortName'] .' </td>'; 
			echo '<td class="PreEnrollRejectReason_td"> '. $result['PreEnrollRejectReason'] .' </td>'; 
			echo '<td class="DateFlowTransReceived_td"> '. $result['DateFlowTransReceived'] .' </td>'; 

			if( $result['FlowStartDate'] ){
				echo '<td class="FlowStartDate_td">'.date('m/d/Y',strtotime($result['FlowStartDate'])).'</td>';
			} 
			else {
			  echo '<td class="FlowStartDate_td"></td>';
			}
			
			echo '<td class="DropReason_td"> '. $result['DropReason'] .' </td>'; 
			 
			if( $result['UtilityRejectionDate'] ){
			 	echo '<td class="UtilityRejectionDate_td">'.date('m/d/Y',strtotime($result['UtilityRejectionDate'])).'</td>';
			} 
			else {
			  	echo '<td class="UtilityRejectionDate_td"></td>'; 
			}

			echo '<td class="UtilityRejectReason_td"> '. $result['UtilityRejectReason'] .' </td>';
        }

		echo '</tbody>';

	}
	else{
		echo '<tbody>
			<tr>
				<td colspan="8">
					<font face="Arial, Helvetica, sans-serif" color="#FF0000" size="+1">No data in column</font>
				</td>
			</tr>
		</tbody>';
	}
?>