/* Search on click of custom input group class in table header's search input fields STARTS*/
$(document).on('click','.custom-input-group-class',function(e){
	e.preventDefault();

	// Searched Value
	var searched_val = $(this).siblings('input[type="text"]').val();

	if( searched_val != '' ){

		// refresh page with searched value
		var loc = location.origin +location.pathname;

		var searched_key = $(this).siblings('input[type="text"]').attr('placeholder').replace('Search ','');

		var temp_col_class = $(this).siblings('input[type="text"]').attr('placeholder').replace('Search ','') +'_td';

		var flag = false;

		$('td.'+temp_col_class).each(function(v,f){

			if( $.trim($(f).text()) != '' ){
				flag = true;
				return false;
			}

		});

		if( flag ){
			location.href = modifyUrl([searched_key,searched_val],'search');
		}
		else{
			alert('No data in column');
		}

	}
	else{
		alert('Please enter value to search');
	}
});

/* Search on click of custom input group class in table header's search input fields ENDS*/


/* Sorting On arrow click as per the columns in table's header STARTS */

$(document).on('click','.custom-sort-btn',function(e){
	e.preventDefault();

	var temp_col_class = $(this).data('order-by-key') +'_td';

	var flag = false;

	$('td.'+temp_col_class).each(function(v,f){

		if( $.trim($(f).text()) != '' ){
			flag = true;
			return false;
		}

	});

	if( flag ){
		var col = $(this).data('order-by-key');
		var order = $(this).data('order-by-val');

		// refresh page with searched value
		location.href = modifyUrl([col,order], 'order-by');
	}
	else{
		alert('No data in column');
	}

});

/* Sorting On arrow click as per the columns in table's header ENDS */

/* Function to modify the URL as per new parameters */

function modifyUrl( values, param ){

	var loc = location.origin +location.pathname;

	if( location.search == '' ){
		loc = loc + '?'+param+'='+ values[0]+'_'+values[1];
	}
	else{
		if( location.search.indexOf(param) != -1 ){
			var search_query_arr = location.search.replace('?','').split('&');

			$(search_query_arr).each(function(i,v){

				if( v.indexOf(param) != -1 ){

					var f = v.split('=');

					loc = loc + location.search.replace(f[1],values[0]+'_'+values[1]);

				}

			});
		}
		else{
			loc = loc + location.search + '&'+param+'='+ values[0]+'_'+values[1];
		}
	}

	return loc;
}

/* Function to modify the URL as per new parameters */


/* Javascript for organization commission */

/* Search on click of custom input group class in table header's search input fields STARTS*/
$(document).on('click','.org-custom-input-group-class',function(e){
	e.preventDefault();

	// Searched Value
	var searched_val = $(this).siblings('input[type="text"]').val();

	if( searched_val != '' ){

		// refresh page with searched value
		var loc = location.origin +location.pathname;

		var searched_key = $(this).siblings('input[type="text"]').attr('placeholder').replace('Search ','');

		// location.href = modifyUrl([searched_key,searched_val],'org-search');

	}
});

/* Search on click of custom input group class in table header's search input fields ENDS*/


/* Sorting On arrow click as per the columns in table's header STARTS */

$(document).on('click','.org-custom-sort-btn',function(e){
	e.preventDefault();

	var col = $(this).data('order-by-key');
	var order = $(this).data('order-by-val');

	// refresh page with searched value
	// location.href = modifyUrl([col,order], 'org-order-by');
});

/* Sorting On arrow click as per the columns in table's header ENDS */


/* Javscript to export data */

function setses(t){
	$.ajax({
		url : 'exports.php',
		data : {
			'page' : 'commission',
			'image': vfg,
			'type' : t
		},
		method   : 'POST',
		success  : function(res){

			if( res == 'success' ){
				$('body').append($('<a>',{'href':'exports.php', 'target' : '_blank','id':'external_link_for_export'}));

				$(document).find('#external_link_for_export').get(0).click();
			}
			else if( res == 'fail' ){
				location.href = 'index.php';
			}
			else{
				setses(t);
			}
		}
	});
}


$(document).on('click','#csv_commission,#excel_commission,#pdf_commission',function(e){
	setses($(this).data('type'));
});

$(document).on('click','#print_commission',function(){

	var divToPrint = document.getElementById($(this).data('source'));
    newWin= window.open("");
    newWin.document.write(divToPrint.outerHTML);
    newWin.print();
    newWin.close();

});

$(document).on('change','select#num_of_rows_shown',function(e){

	var loc = location.origin +location.pathname;

	if( location.search == '' ){
		loc = loc + '?rows='+ $(this).val();
	}
	else{
		if( location.search.indexOf('rows') != -1 ){
			var search_query_arr = location.search.replace('?','').split('&');

			$(search_query_arr).each(function(i,v){

				if( v.indexOf('rows') != -1 ){

					var f = v.split('=');

					var r_p = 'rows=' + f[1];
					var r_e = 'rows=' + $(e.target).val();

					loc = loc + location.search.replace(r_p,r_e);

				}

			});
		}
		else{
			loc = loc + location.search + '&rows='+ $(this).val();
		}
	}

	location.href = loc;

});

/* Javascript to export data */

$(document).ready(function(){

	/* Scroll down to the table if search keywords exists in URI */

	if( $('#commision_tbl').length > 0 ){
		if( (location.search.indexOf('search') != -1) || (location.search.indexOf('order-by') != -1) ){
			$('html,body').animate({
		    	scrollTop: $("#commision_tbl").offset().top},
		    'slow');
		}
	}

	/* Scroll down to the table if search keywords exists in URI */
});

