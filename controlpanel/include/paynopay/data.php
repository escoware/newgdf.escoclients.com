<?php
	require_once 'include/paynopay/query.php';

	if( $total_data_table_rows > 0 ){

		echo '<tbody>';

		$i=0;

		while( $result = mssql_fetch_assoc($data_resource) ){

			$i++;

			if( $i % 2 == 0 ){
				$bgcolor = "#fff";
			}
			else{
				$bgcolor = "";
			}
			?>
			<tr bgcolor="<?php echo $bgcolor;?>">					                            		
        		<td class="CommissionRunDetailID CommissionRunDetailID_td"><?php echo $result['CommissionRunDetailID']; ?></td>
	            <td class="CommissionRunID CommissionRunID_td"><?php echo $result['CommissionRunID'];?></td>
              	<td class="CommissionRunDate CommissionRunDate_td"><?php echo $result['CommissionRunDate']?></td>
              	<td class="ContractID_td"><?php echo $result['ContractID'];?></td>
			    <td class="ContractNumber_td"><?php echo $result['ContractNumber'];?></td>
			    <td class="LeadID_td"><?php echo $result['LeadID'];?></td>

			    <?php 
			    	if( $result['EntryDate'] ) {
						$EntryDate = date('m/d/Y',strtotime($result['EntryDate'])); 
					} 
					else{
						$EntryDate = $result['EntryDate'];
					} 
				?>

				<td class="EntryDate_td"><?php echo $EntryDate;?></td>
				<td class="PaidBrokerID_td"><?php echo $result['PaidBrokerID'];?></td>
				<td class="PaidBrokerCompanyName_td"><?php echo $result['PaidBrokerCompanyName'];?></td>
				<td class="CustomerTypeName_td"><?php echo $result['CustomerTypeName'];?></td>
				<td class="CustomerFirstName_td"><?php echo $result['CustomerFirstName'];?></td>
				<td class="CustomerLastName_td"><?php echo $result['CustomerLastName'];?></td>
				<td class="CustomerCompanyName_td"><?php echo $result['CustomerCompanyName'];?></td>
				<td class="SerivceAddress1_td"><?php echo $result['SerivceAddress1'];?></td>
				<td class="ServiceAddress2_td"><?php echo $result['ServiceAddress2'];?></td>
				<td class="SerivceCity_td"><?php echo $result['SerivceCity'];?></td>
				<td class="ServiceStateCode_td"><?php echo $result['ServiceStateCode'];?></td>
				<td class="ServiceZipCode_td"><?php echo $result['ServiceZipCode'];?></td>
				<td class="ProductName_td"><?php echo $result['ProductName'];?></td>
				<td class="ContractRateValue_td"><?php echo number_format($result['ContractRateValue'],3);?></td>
				<td class="ContractTerm_td"><?php echo $result['ContractTerm'];?></td>
				<td class="PlanDescription_td"><?php echo $result['PlanDescription'];?></td>
				<td class="CommodityName_td"><?php echo $result['CommodityName'];?></td>
				<td class="UtilityAccountNumber_td"><?php echo $result['UtilityAccountNumber'];?></td>
				<td class="MeterNumber_td"><?php echo $result['MeterNumber'];?></td>
				<td class="BillingServiceStatus_td"><?php echo $result['BillingServiceStatus'];?></td>
				<td class="BillingServiceDescription_td"><?php echo $result['BillingServiceDescription']; ?></td>
				<td class="ContractStatus_td"><?php echo $result['ContractStatus'];?></td>
				<td class="BillingCycleStartDate_td"><?php echo $result['BillingCycleStartDate'];?></td>

				<?php 
					if( $result['ContractEndDate'] ) {
						$ContractEndDate=date('m/d/Y',strtotime($result['ContractEndDate']));
					} 
					else{
						$ContractEndDate=$result['ContractEndDate'];
					}
				?>

				<td class="ContractEndDate_td"><?php echo $ContractEndDate;?></td>
				<?php
					if($result['AccountConfirmedStartDate']!=''){
						$AccountConfirmedStartDate = date('m/d/Y',strtotime($result['AccountConfirmedStartDate']));
					}
					else{
						$AccountConfirmedStartDate = $result['AccountConfirmedStartDate'];
					}
				?>
				<td class="AccountConfirmedStartDate_td"><?php echo $AccountConfirmedStartDate;?></td>
				<td class="UtilityID_td"><?php echo $result['UtilityID'];?></td>
				<td class="UtilityName_td"><?php echo $result['UtilityName'];?></td>
				<td class="UtilityShortName_td"><?php echo $result['UtilityShortName'];?></td>
				<td class="PreEnrollRejectReason_td"><?php echo $result['PreEnrollRejectReason'];?></td>
				<td class="PaidSigningAgentCode_td"><?php echo $result['PaidSigningAgentCode'];?></td>
				<td class="TPVLink_td"><?php echo $result['TPVLink'];?></td>
				<td class="servicestatusbillingvalue_td"><?php echo $result['servicestatusbillingvalue'];?></td>
				<td class="CommissionRate_td"><?php echo money_format('%(.2n', $result['CommissionRate']);?></td>
				<td class="RateUOM_td"><?php echo $result['RateUOM'];?></td>
				<td class="forecastusage_td"><?php echo $result['forecastusage'];?></td>
				<td class="CommissionAmount_td"><?php echo money_format('%(.2n', $result['CommissionAmount']);?></td>
				<td class="PaymentTypeName_td"><?php echo $result['PaymentTypeName'];?></td>

				<?php
					if( $result['DateFlowTransReceived'] ) {
						$DateFlowTransReceived=date('m/d/Y',strtotime($result['DateFlowTransReceived']));
					} 
					else{
						$DateFlowTransReceived=$result['DateFlowTransReceived'];
					} 
				?>

				<td class="DateFlowTransReceived_td"><?php echo $DateFlowTransReceived;?></td>
				<?php 
					if( $result['FlowStartDate'] ) {
						$FlowStartDate=date('m/d/Y',strtotime($result['FlowStartDate']));
					} 
					else {
						$FlowStartDate=$result['FlowStartDate'];
					} 
				?>
				<td class="FlowStartDate_td"><?php echo $FlowStartDate;?></td>

				<?php 
					if($result['FlowEndDate']) {
						$FlowEndDate=date('m/d/Y',strtotime($result['FlowEndDate'])); 
					} 
					else{
						$FlowEndDate=$result['FlowEndDate'];
					}
				 	if($result['DropDateReceived']) {
						$DropDateReceived=date('m/d/Y',strtotime($result['DropDateReceived'])); 
					} 
					else {
						$DropDateReceived=$result['DropDateReceived'];
					}
				 	if($result['UtilityRejectionDate']) {
						$UtilityRejectionDate=date('m/d/Y',strtotime($result['UtilityRejectionDate']));
					} 
					else {
						$UtilityRejectionDate=$result['UtilityRejectionDate'];
					}
				?>

				<td class="FlowEndDate_td"><?php echo $FlowEndDate;?></td>
				<td class="DropDateReceived_td"><?php echo $DropDateReceived;?></td>
				<td class="DropReason_td"><?php echo $result['DropReason'];?></td>
				<td class="UtilityRejectionDate_td"><?php echo $UtilityRejectionDate;?></td>
				<td class="UtilityRejectReason_td"><?php echo $result['UtilityRejectReason'];?></td>
				<td class="CallCenterDropCode_td"><?php echo $result['CallCenterDropCode'];?></td>
				<td class="ForecastAnnualUsage_td"><?php echo $result['ForecastAnnualUsage'];?></td>
				<td class="ForecastAnnualUsageCoverage_td"><?php echo $result['ForecastAnnualUsageCoverage'];?></td>
				<td class="ForecastAnnualUsageUOM_td"><?php echo $result['ForecastAnnualUsageUOM'];?></td>
				<td class="DwellingType_td"><?php echo $result['DwellingType'];?></td>
				<td class="TrueUpIndicator_td"><?php echo $result['TrueUpIndicator'];?></td>
				<td class="DualFuelIndicator_id"><?php echo $result['DualFuelIndicator'];?></td>
        	</tr>
        	<?php
        }

		echo '</tbody>';

	}
	else{
		echo '<tbody>
			<tr>
				<td colspan="8">
					<font face="Arial, Helvetica, sans-serif" color="#FF0000" size="+1">No data in column</font>
				</td>
			</tr>
		</tbody>';
	}
?>