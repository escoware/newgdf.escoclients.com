<div class="dataTables_info" id="example_info" role="status" aria-live="polite">
	<?php $temp_maxlimit = ( $total_rows > $num_of_rows_shown_in_table) ? ($maxlimit - 1) : $total_rows; echo "Showing {$minlimit} to {$temp_maxlimit} of {$total_rows} entries"; ?>
</div>
<?php if( $total_rows > $num_of_rows_shown_in_table ){ ?>
<div class="dataTables_paginate paging_simple_numbers">
	
	<ul class="pagination">
		
		<?php

			$num_of_pages = (int)($total_rows / $num_of_rows_shown_in_table);

			$paginate_nums = ( $num_of_pages > 10 ) ? 10 : $num_of_pages;

			if( isset($_REQUEST['page']) ){
				unset($_REQUEST['page']);
			}

			$string = '?';

			foreach ($_REQUEST as $key => $value) {
				$string .= $key.'='.$value.'&';
			}

			$string .= 'page=';

			$base_link = $_SERVER['PHP_SELF'] . $string;

			$pre_link = ( $page > 1 ) ? $base_link .($page - 1) : $base_link.'1';

			$next_link = ( ($page >= 1) && ($page < $paginate_nums) ) ? $base_link .($page + 1) : $base_link.'1';

			echo "<li class='paginate_button previous'>
					<a href='".$pre_link."'>
						Previous
					</a>
				  </li>";

			for ($i = $page; $i <= $page+9; $i++) {
				
				$is_active = ( $page == $i ) ? 'class="paginate_button active"' : 'class="paginate_button"';

				echo "<li {$is_active}>
						<a href='".$base_link.$i."'>
							{$i}
						</a>
					 </li>";
			
			}

			echo "<li class='paginate_button previous'>
					<a href='".$next_link."'>
						Next
					</a>
				  </li>";
		?>

	</ul>

</div>
<?php } ?>