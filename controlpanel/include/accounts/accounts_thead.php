<th id="">
	<span class="htitle">Account ID</span>
    <a href="javascript:void(0)" class="custom-sort-btn account_icon" data-order-by-key = "AccountID" data-order-by-val = "desc">
   			<span class="sort-btn btn-asc">▲</span>
   		</a>
   		<a href="javascript:void(0)" class="custom-sort-btn account_icon" data-order-by-key = "AccountID" data-order-by-val = "asc">
   			<span class="sort-btn btn-desc ">▼</span>
   		</a>
    <div id="" class="sr_bx">
   		<div class="input-group">
   			<input type="text" placeholder="Search AccountID">
		  	<span class="input-group-addon custom-input-group-class">
		  		<i class="fa fa-search"></i>
		  	</span>
		</div>
   		
   </div>
</th>

<th rowspan="1" colspan="1" style="width: 182px;" aria-label="ContractID" id="">
	<span class="htitle">Contract ID</span>
    <a href="javascript:void(0)" class="custom-sort-btn account_icon" data-order-by-key = "ContractID" data-order-by-val = "desc">
   			<span class="sort-btn btn-asc">▲</span>
   		</a>
   		<a href="javascript:void(0)" class="custom-sort-btn account_icon" data-order-by-key = "ContractID" data-order-by-val = "asc">
   			<span class="sort-btn btn-desc ">▼</span>
   		</a>
	<div id="" class="sr_bx">
		<div class="input-group">
   			<input type="text" placeholder="Search ContractID">
   			<span class="input-group-addon custom-input-group-class">
		  		<i class="fa fa-search"></i>
		  	</span>
		</div>
   		
   		
	</div>
</th>

<th id="">
	<span class="htitle">Entry Date</span>
    <a href="javascript:void(0)" class="custom-sort-btn account_icon" data-order-by-key = "EnrollmentDate" data-order-by-val = "desc">
   			<span class="sort-btn btn-asc">▲</span>
   		</a>
   		<a href="javascript:void(0)" class="custom-sort-btn account_icon" data-order-by-key = "EnrollmentDate" data-order-by-val = "asc">
   			<span class="sort-btn btn-desc ">▼</span>
   		</a>
	<div id="" class="sr_bx">
		<div class="input-group">
			<input type="text" placeholder="Search EnrollmentDate">
			<span class="input-group-addon custom-input-group-class">
		  		<i class="fa fa-search"></i>
		  	</span>
		</div>
   		
   		
	</div>
</th>

<th  rowspan="1" colspan="1" style="width: 182px;" aria-label="StatusName">
	<span class="htitle">Account Status</span>
    	<a href="javascript:void(0)" class="custom-sort-btn account_icon" data-order-by-key = "StatusName" data-order-by-val = "desc">
   			<span class="sort-btn btn-asc">▲</span>
   		</a>
   		<a href="javascript:void(0)" class="custom-sort-btn account_icon" data-order-by-key = "StatusName" data-order-by-val = "asc">
   			<span class="sort-btn btn-desc ">▼</span>
   		</a>
	<div id="StatusName" class="sr_bx">
		<div class="input-group">
			<input type="text" placeholder="Search StatusName">
			<span class="input-group-addon custom-input-group-class">
		  		<i class="fa fa-search"></i>
		  	</span>
		</div>
   		
   	
	</div>
</th>

<th  rowspan="1" colspan="1" style="width: 182px;" aria-label="SigningAgentCode">
	<span class="htitle">Signing Agent Code</span>
    	<a href="javascript:void(0)" class="custom-sort-btn account_icon" data-order-by-key = "SigningAgentCode" data-order-by-val = "desc">
   			<span class="sort-btn btn-asc">▲</span>
   		</a>
   		<a href="javascript:void(0)" class="custom-sort-btn account_icon" data-order-by-key = "SigningAgentCode" data-order-by-val = "asc">
   			<span class="sort-btn btn-desc ">▼</span>
   		</a>
	<div id="BrokerID" class="sr_bx">
		<div class="input-group">
			<input type="text" placeholder="Search SigningAgentCode">
			<span class="input-group-addon custom-input-group-class">
		  		<i class="fa fa-search"></i>
		  	</span>
		</div>
   		
   	
	</div>
</th>

<th rowspan="1" colspan="1" style="width: 182px;" aria-label="CustomerTypeName">
	<span class="htitle">Customer Type</span>
    <a href="javascript:void(0)" class="custom-sort-btn account_icon" data-order-by-key = "CustomerTypeName" data-order-by-val = "desc">
   			<span class="sort-btn btn-asc">▲</span>
   		</a>
   		<a href="javascript:void(0)" class="custom-sort-btn account_icon" data-order-by-key = "CustomerTypeName" data-order-by-val = "asc">
   			<span class="sort-btn btn-desc ">▼</span>
   		</a>
	<div id="Cnumber" class="sr_bx">
		<div class="input-group">
			<input type="text" placeholder="Search CustomerTypeName">
			<span class="input-group-addon custom-input-group-class">
		  		<i class="fa fa-search"></i>
		  	</span>
		</div>
   		
   		
	</div>
</th>

<th rowspan="1" colspan="1" style="width: 182px;" aria-label="FirstName">
	<span class="htitle">First Name</span>
    <a href="javascript:void(0)" class="custom-sort-btn account_icon" data-order-by-key = "FirstName" data-order-by-val = "desc">
   			<span class="sort-btn btn-asc">▲</span>
   		</a>
   		<a href="javascript:void(0)" class="custom-sort-btn account_icon" data-order-by-key = "FirstName" data-order-by-val = "asc">
   			<span class="sort-btn btn-desc ">▼</span>
   		</a>
	<div id="FirstName" class="sr_bx">
		<div class="input-group">
			<input type="text" placeholder="Search FirstName">
			<span class="input-group-addon custom-input-group-class">
		  		<i class="fa fa-search"></i>
		  	</span>		
		</div>
   		
   		
	</div>
</th>

<th rowspan="1" colspan="1" style="width: 182px;" aria-label="LastName">
	<span class="htitle">Last Name</span>
    <a href="javascript:void(0)" class="custom-sort-btn account_icon" data-order-by-key = "LastName" data-order-by-val = "desc">
   			<span class="sort-btn btn-asc">▲</span>
   		</a>
   		<a href="javascript:void(0)" class="custom-sort-btn account_icon" data-order-by-key = "LastName" data-order-by-val = "asc">
   			<span class="sort-btn btn-desc ">▼</span>
   		</a>
	<div id="LastName" class="sr_bx">           
		<div class="input-group">
			<input type="text" placeholder="Search LastName">
			<span class="input-group-addon custom-input-group-class">
		  		<i class="fa fa-search"></i>
		  	</span>
		</div>
   		
   		
	</div>
</th>

<th rowspan="1" colspan="1" style="width: 182px;" aria-label="CompanyName">
	<span class="htitle">Company Name</span>
    <a href="javascript:void(0)" class="custom-sort-btn account_icon" data-order-by-key = "CompanyName" data-order-by-val = "desc">
   			<span class="sort-btn btn-asc">▲</span>
   		</a>
   		<a href="javascript:void(0)" class="custom-sort-btn account_icon" data-order-by-key = "CompanyName" data-order-by-val = "asc">
   			<span class="sort-btn btn-desc ">▼</span>
   		</a>
	<div id="paidbroker" class="sr_bx">
		<div class="input-group">
			<input type="text" placeholder="Search CompanyName">
			<span class="input-group-addon custom-input-group-class">
		  		<i class="fa fa-search"></i>
		  	</span>
		</div>
   		
   		
	</div>
</th>

<th rowspan="1" colspan="1" style="width: 182px;" aria-label="Address1">
	<span class="htitle">Service Address</span>
    <a href="javascript:void(0)" class="custom-sort-btn account_icon" data-order-by-key = "Address1" data-order-by-val = "desc">
   			<span class="sort-btn btn-asc">▲</span>
   		</a>
   		<a href="javascript:void(0)" class="custom-sort-btn account_icon" data-order-by-key = "Address1" data-order-by-val = "asc">
   			<span class="sort-btn btn-desc ">▼</span>
   		</a>
	<div id="PaidBrokercompany" class="sr_bx">
		<div class="input-group">
			<input type="text" placeholder="Search Address1">
			<span class="input-group-addon custom-input-group-class">
				<i class="fa fa-search"></i>
			</span>
		</div>
   		
   		
	</div>
</th>

<th rowspan="1" colspan="1" style="width: 182px;" aria-label="Address2">
	<span class="htitle">Service Address2</span>
    <a href="javascript:void(0)" class="custom-sort-btn account_icon" data-order-by-key = "Address2" data-order-by-val = "desc">
   			<span class="sort-btn btn-asc">▲</span>
   		</a>
   		<a href="javascript:void(0)" class="custom-sort-btn account_icon" data-order-by-key = "Address2" data-order-by-val = "asc">
   			<span class="sort-btn btn-desc ">▼</span>
   		</a>
	<div id="Address2" class="sr_bx">
		<div class="input-group">
			<input type="text" placeholder="Search Address2">
			<span class="input-group-addon custom-input-group-class">
		  		<i class="fa fa-search"></i>
		  	</span>
		</div>
   		
   		
	</div>
</th>

<th rowspan="1" colspan="1" style="width: 182px;" aria-label="City">
	<span class="htitle">City</span>
    	<a href="javascript:void(0)" class="custom-sort-btn account_icon" data-order-by-key = "City" data-order-by-val = "desc">
   			<span class="sort-btn btn-asc">▲</span>
   		</a>
   		<a href="javascript:void(0)" class="custom-sort-btn account_icon" data-order-by-key = "City" data-order-by-val = "asc">
   			<span class="sort-btn btn-desc ">▼</span>
   		</a>
	<div id="City" class="sr_bx">
		<div class="input-group">
			<input type="text" placeholder="Search City">
			<span class="input-group-addon custom-input-group-class">
		  		<i class="fa fa-search"></i>
		  	</span>
		</div>
   		
   	
	</div>
</th>

<th rowspan="1" colspan="1" style="width: 182px;" aria-label="StateCode">
	<span class="htitle">State</span>
    <a href="javascript:void(0)" class="custom-sort-btn account_icon" data-order-by-key = "StateCode" data-order-by-val = "desc">
   			<span class="sort-btn btn-asc">▲</span>
   		</a>
   		<a href="javascript:void(0)" class="custom-sort-btn account_icon" data-order-by-key = "StateCode" data-order-by-val = "asc">
   			<span class="sort-btn btn-desc ">▼</span>
   		</a>
	<div id="StateCode" class="sr_bx">
		<div class="input-group">
			<input type="text" placeholder="Search StateCode">
			<span class="input-group-addon custom-input-group-class">
		  		<i class="fa fa-search"></i>
		  	</span>
		</div>
   		
   		
	</div>
</th>

<th rowspan="1" colspan="1" style="width: 182px;" aria-label="PostalCode">
	<span class="htitle">Zip Code</span>
    <a href="javascript:void(0)" class="custom-sort-btn account_icon" data-order-by-key = "PostalCode" data-order-by-val = "desc">
   			<span class="sort-btn btn-asc">▲</span>
   		</a>
   		<a href="javascript:void(0)" class="custom-sort-btn account_icon" data-order-by-key = "PostalCode" data-order-by-val = "asc">
   			<span class="sort-btn btn-desc ">▼</span>
   		</a>
	<div id="PostalCode" class="sr_bx">
		<div class="input-group">
			<input type="text" placeholder="Search PostalCode">
			<span class="input-group-addon custom-input-group-class">
		  		<i class="fa fa-search"></i>
		  	</span>
		</div>
   		
   		
	</div>
</th>

<th rowspan="1" colspan="1" style="width: 182px;" aria-label="HomePhone">
	<span class="htitle">Phone</span>
    <a href="javascript:void(0)" class="custom-sort-btn account_icon" data-order-by-key = "HomePhone" data-order-by-val = "desc">
   			<span class="sort-btn btn-asc">▲</span>
   		</a>
   		<a href="javascript:void(0)" class="custom-sort-btn account_icon" data-order-by-key = "HomePhone" data-order-by-val = "asc">
   			<span class="sort-btn btn-desc ">▼</span>
   		</a>
	<div id="serviceaddress1" class="sr_bx">
		<div class="input-group">
			<input type="text" placeholder="Search HomePhone">
			<span class="input-group-addon custom-input-group-class">
		  		<i class="fa fa-search"></i>
		  	</span>
		</div>
   		
   		
	</div>
</th>

<th rowspan="1" colspan="1" style="width: 182px;" aria-label="Email">
	<span class="htitle">Email</span>
    	<a href="javascript:void(0)" class="custom-sort-btn account_icon" data-order-by-key = "Email" data-order-by-val = "desc">
   			<span class="sort-btn btn-asc">▲</span>
   		</a>
   		<a href="javascript:void(0)" class="custom-sort-btn account_icon" data-order-by-key = "Email" data-order-by-val = "asc">
   			<span class="sort-btn btn-desc ">▼</span>
   		</a>
	<div id="Servicesddress2" class="sr_bx">
		<div class="input-group">
			<input type="text" placeholder="Search Email">
			<span class="input-group-addon custom-input-group-class" style="width: auto;">
		  		<i class="fa fa-search"></i>
		  	</span>
		</div>
   		
   	
	</div>
</th>

<th rowspan="1" colspan="1" style="width: 182px;" aria-label="ProductCode">
	<span class="htitle">Product Code</span>
    <a href="javascript:void(0)" class="custom-sort-btn account_icon" data-order-by-key = "ProductCode" data-order-by-val = "desc">
   			<span class="sort-btn btn-asc">▲</span>
   		</a>
   		<a href="javascript:void(0)" class="custom-sort-btn account_icon" data-order-by-key = "ProductCode" data-order-by-val = "asc">
   			<span class="sort-btn btn-desc ">▼</span>
   		</a>
	<div id="servicecity" class="sr_bx">           
		<div class="input-group">
			<input type="text" placeholder="Search ProductCode">
			<span class="input-group-addon custom-input-group-class">
		  		<i class="fa fa-search"></i>
		  	</span>
		</div>
   		
   		
	</div>           
</th>

<th rowspan="1" colspan="1" style="width: 182px;" aria-label="RateValue">
	<span class="htitle">Rate</span>
    <a href="javascript:void(0)" class="custom-sort-btn account_icon" data-order-by-key = "RateValue" data-order-by-val = "desc">
   			<span class="sort-btn btn-asc">▲</span>
   		</a>
   		<a href="javascript:void(0)" class="custom-sort-btn account_icon" data-order-by-key = "RateValue" data-order-by-val = "asc">
   			<span class="sort-btn btn-desc ">▼</span>
   		</a>
	<div id="RateValue" class="sr_bx">
		<div class="input-group">
			<input type="text" placeholder="Search RateValue">
			<span class="input-group-addon custom-input-group-class">
		  		<i class="fa fa-search"></i>
		  	</span>
		</div>
		
	</div>
</th>

<th rowspan="1" colspan="1" style="width: 182px;" aria-label="Term">
	<span class="htitle">Term</span>
    <a href="javascript:void(0)" class="custom-sort-btn account_icon" data-order-by-key = "Term" data-order-by-val = "desc">
   			<span class="sort-btn btn-asc">▲</span>
   		</a>
   		<a href="javascript:void(0)" class="custom-sort-btn account_icon" data-order-by-key = "Term" data-order-by-val = "asc">
   			<span class="sort-btn btn-desc ">▼</span>
   		</a>
	<div id="Term" class="sr_bx">
		<div class="input-group">
			<input type="text" placeholder="Search Term">
			<span class="input-group-addon custom-input-group-class">
		  		<i class="fa fa-search"></i>
		  	</span>
		</div>
   		
   		
	</div>
</th>

<th rowspan="1" colspan="1" style="width: 182px;" aria-label="MarkupInfo">
	<span class="htitle">Markup Info</span>
    <a href="javascript:void(0)" class="custom-sort-btn account_icon" data-order-by-key = "MarkupInfo" data-order-by-val = "desc">
   			<span class="sort-btn btn-asc">▲</span>
   		</a>
   		<a href="javascript:void(0)" class="custom-sort-btn account_icon" data-order-by-key = "MarkupInfo" data-order-by-val = "asc">
   			<span class="sort-btn btn-desc ">▼</span>
   		</a>
	<div id="MarkupInfo" class="sr_bx">
		<div class="input-group">
			<input type="text" placeholder="Search MarkupInfo">
			<span class="input-group-addon custom-input-group-class">
		  		<i class="fa fa-search"></i>
		  	</span>
		</div>
   		
   		
	</div>
</th>