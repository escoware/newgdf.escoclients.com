<?php
	
	if( $_REQUEST['search'] ){

		$temp_reset_request = $_REQUEST;

		unset($temp_reset_request['search']);

		$output = implode('&', array_map(
					    function ($v, $k) { 
					    	return sprintf("%s=%s", $k, $v); 
					    },
					    $temp_reset_request,
					    array_keys($temp_reset_request)
					));

		$temp_reset_request_search_uri = $_SERVER['PHP_SELF'] . ( (count($temp_reset_request) > 0 ) ? '?'.$output : '');

		echo '<a href="'.$temp_reset_request_search_uri.'" class="btn btn-info">
				Exit Search View
			 </a>';
	}

	if( $_REQUEST['order-by'] ){
		$temp_reset_request = $_REQUEST;

		unset($temp_reset_request['order-by']);

		$output = implode('&', array_map(
					    function ($v, $k) { 
					    	return sprintf("%s=%s", $k, $v); 
					    },
					    $temp_reset_request,
					    array_keys($temp_reset_request)
					));

		$temp_reset_request_search_uri = $_SERVER['PHP_SELF'] . ( (count($temp_reset_request) > 0 ) ? '?'.$output : '');

		echo '<a href="'.$temp_reset_request_search_uri.'" class="btn btn-info">
				Exit Sort View
			 </a>';
	}
?>