<?php 
include("../include/config.php");
validate_admin();
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo SITE_TITLE; ?></title>
    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
 
    <link href="css/custom.css" rel="stylesheet">
    <link href="css/buttons.dataTables.min.css" rel="stylesheet" type="text/css">
    
  </head>
  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <?php include("left-menu.php");?>
        </div>
        <!-- top navigation -->
        <div class="top_nav">
          <?php include("top-menu.php"); ?>
        </div>
        <!-- /top navigation -->
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="col-md-12 col-lg-12 col-lg-12 cnt_area">
             <div class="page-title">
              <div class="title_left">
                <h3>
                     
                  </h3>
              </div>

              <div class="">
                   <div class="col-md-9 col-lg-9 dt-buttons">
                    <a class="dt-button buttons-copy buttons-html5" tabindex="0" aria-controls="example" href="#"><span>Copy</span></a>
                    <a class="dt-button buttons-csv buttons-html5" tabindex="0" aria-controls="example" href="#"><span>CSV</span></a>
                    <a class="dt-button buttons-excel buttons-html5" tabindex="0" aria-controls="example" href="#"><span>Excel</span></a>
                    <a class="dt-button buttons-pdf buttons-html5" tabindex="0" aria-controls="example" href="#"><span>PDF</span></a>
                    <a class="dt-button buttons-print" tabindex="0" aria-controls="example" href="#"><span>Print</span></a>
				</div>
                <div class="col-md-3 col-sm-3 col-xs-12 form-group pull-right top_search">
                <form method="post" class="" action="member-list.php">
                  <div class="input-group srch">
                    <input type="text" name="keyword" class="form-control" value="<?php echo $_REQUEST['keyword']; ?>" placeholder="Search for...">
                    <span class="input-group-btn">
                              <div class=" advnc search" style="float:right;">
                               		 <a class="btn-sm btn btn-primary" href="commission_detail.php?comicycle=1034">Advanced Search/Sort</a>
                               </div>
                             
                          </span> 
                  </div></form>
                </div>
              </div>
            </div>
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                  <div class="col-md-9">
                    <h2>View Country Phone Code</h2>
                    </div>
                     <div class="col-md-3 ">
                     
                     <button type="button" class="btn btn-primary pull-right" onClick="location.href='phonecountrycode-addf.php'">Add Country Phone Code</button>
                     </div>
                    
                    <div class="clearfix"></div>
                    
                  </div>
                  <div class="x_content">
                  <div class="col-md-12 error-msg" align="center"><?php echo $_SESSION['sess_msg'];$_SESSION['sess_msg']=''; ?></div>
                    <div class="clearfix"></div>
                    <form name="frm" method="post" action="phonecountrycode-del.php" enctype="multipart/form-data">
                    <table id="datatable-responsive" class="table table-striped dt-responsive nowrap" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th>S.No</th>
                          <th>Country</th>
                          <th>Phone Code</th>
                             <th>Flag image</th>
                          <th align="center">Status</th>
                          <th align="center">Action</th>
                          <th ><input name="check_all" type="checkbox"   id="check_all"  value="check_all" /></th>
                         
                        </tr>
                      </thead>
                      <?php 
$where='';
if($_REQUEST['keyword']!=''){
	$keyword=mysql_real_escape_string($_REQUEST['keyword']);
$where.=" and country like '$keyword%' ";	
}
$start=0;
if(isset($_GET['start'])) $start=$_GET['start'];
$pagesize=15;
if(isset($_GET['pagesize'])) $pagesize=$_GET['pagesize'];
$order_by='id';
if(isset($_GET['order_by'])) $order_by=$_GET['order_by'];
$order_by2='desc';
if(isset($_GET['order_by2'])) $order_by2=$_GET['order_by2'];
$sql=$obj->Query("select * from $tbl_phonecountrycode where 1=1 $where order by $order_by $order_by2 limit $start, $pagesize");
$sql2=$obj->query("select * from $tbl_phonecountrycode where 1=1 $where order by $order_by $order_by2",$debug=-1);
$reccnt=$obj->numRows($sql2);
if($reccnt==0)
{
?>
                        <tbody>
                      
                        <tr>
                        <td colspan="5"><font face="Arial, Helvetica, sans-serif"   color="#FF0000" size="+1">No Record</font></td>
                        </tr></tbody>
                            <?php }else{ ?>
                      <tbody>
                      <?php
                            $i=0;
                            while($line=$obj->fetchNextObject($sql))
                            {
                            $i++;


                            if($i%2==0)
                            {
                            $bgcolor = "#f6f6f6";
                            }
                            else
                            {
                            $bgcolor = "";
                            }
                            ?>
                        <tr bgcolor="<?php echo $bgcolor;?>">
                          <td><strong><?php echo $i+$start; ?>.</strong></td>
                          <td><?php echo stripslashes($line->country);?></td>
  <td><?php echo stripslashes($line->countrycode);?></td>
<td><?php if(is_file("../upload_images/country/thumb/".$line->icon)){?>
                                <img src="../upload_images/country/thumb/<?php echo  $line->icon;?>" height="50" width="50"/>
                                <?php } ?></td>
             
                          <td ><?php if($line->status==1){?><img src="images/enable.gif" border="0" title="Activated" /> <?php } else{ ?><img src="images/disable.gif" border="0" title="Deactivated" /><?php }?>	</td>
                          <td><a href="phonecountrycode-addf.php?id=<?php echo $line->id;?>" ><img src="images/edit3.gif" border="0" title="Edit" /></a>             </td>
                          <td ><input type="checkbox" class="checkall" name="ids[]" value="<?php echo $line->id;?>" /></td>
                         
                        </tr>
                        <?php } ?>
                       <tr><td colspan="7"><?php include("../include/paging.inc.php"); ?></td> 
                       <tr><td colspan="7">
                       
                     
                       
                       </td></tr>
                      </tbody>
                      
                      <?php }  ?>
                    </table></form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
        <!-- footer content -->
        <footer>
          <?php  include("footer.php");?>
        </footer>
        <!-- /footer content -->
      </div>
    </div>
    <!-- jQuery -->
    <script src="../vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
  
    <!-- Custom Theme Scripts -->
    <script src="js/custom.js"></script>
    <!-- Datatables -->
    
    <!-- /Datatables -->
    <script type="text/javascript">
$(document).ready(function(){
	$("#check_all").click(function(){
		
		if($(this).is(":checked")){
	        $(".checkall").attr("checked","checked");	
	      }else{
	    	$(".checkall").removeAttr("checked");
	      } 
		
		})
	})
</script>
<script>
  
	function checkall(objForm)
    {
	len = objForm.elements.length;
	var i=0;
	for( i=0 ; i<len ; i++){
		if (objForm.elements[i].type=='checkbox') 
		objForm.elements[i].checked=objForm.check_all.checked;
	}
   }
	function del_prompt(frmobj,comb)
		{
		//alert(comb);
			if(comb=='Delete'){
				if(confirm ("Are you sure you want to delete record(s)"))
				{
					frmobj.action = "phonecountrycode-del.php";
					frmobj.what.value="Delete";
					frmobj.submit();
					
				}
				else{ 
				return false;
				}
		}
		else if(comb=='Deactivate'){
			frmobj.action = "phonecountrycode-del.php";
			frmobj.what.value="Deactivate";
			frmobj.submit();
		}
		else if(comb=='Activate'){
			frmobj.action = "phonecountrycode-del.php";
			frmobj.what.value="Activate";
			frmobj.submit();
		}
		
		
	}
</script>
  </body>
</html>