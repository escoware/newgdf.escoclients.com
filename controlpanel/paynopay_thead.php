<th id="cmrdIDhidesearch">
	<span class="htitle">CommissionRunDetailID</span>
    <a href="javascript:void(0)" class="custom-sort-btn" data-order-by-key = "CommissionRunDetailID" data-order-by-val = "desc">
   			<span class="sort-btn btn-desc  no_pay no_pay">▲</span>
   		</a>
   		<a href="javascript:void(0)" class="custom-sort-btn" data-order-by-key = "CommissionRunDetailID" data-order-by-val = "asc">
   			<span class="sort-btn btn-desc  no_pay no_pay">▼</span>
   		</a>
    <div id="cmrdID" class="sr_bx">
   		<div class="input-group">
   			<input type="text" placeholder="Search CommissionRunDetailID">
		  	<span class="input-group-addon custom-input-group-class">
		  		<i class="fa fa-search"></i>
		  	</span>
		</div>
   		
   </div>
</th>

<th rowspan="1" colspan="1" style="width: 182px;" aria-label="CommissionRunID" id="cmrIDhidesearch">
	<span class="htitle">CommissionRunID</span>
    <a href="javascript:void(0)" class="custom-sort-btn" data-order-by-key = "CommissionRunID" data-order-by-val = "desc">
   			<span class="sort-btn btn-desc  no_pay no_pay">▲</span>
   		</a>
   		<a href="javascript:void(0)" class="custom-sort-btn" data-order-by-key = "CommissionRunID" data-order-by-val = "asc">
   			<span class="sort-btn btn-desc  no_pay no_pay ">▼</span>
   		</a>
	<div id="cmrID" class="sr_bx">
		<div class="input-group">
   			<input type="text" placeholder="Search CommissionRunID">
   			<span class="input-group-addon custom-input-group-class">
		  		<i class="fa fa-search"></i>
		  	</span>
		</div>
   		
   		
	</div>
</th>

<th id="cmrDatehidesearch">
	<span class="htitle">CommissionRunDate</span>
    <a href="javascript:void(0)" class="custom-sort-btn" data-order-by-key = "CommissionRunDate" data-order-by-val = "desc">
   			<span class="sort-btn btn-desc  no_pay">▲</span>
   		</a>
   		<a href="javascript:void(0)" class="custom-sort-btn" data-order-by-key = "CommissionRunDate" data-order-by-val = "asc">
   			<span class="sort-btn btn-desc  no_pay ">▼</span>
   		</a>
	<div id="cmrDate" class="sr_bx">
		<div class="input-group">
			<input type="text" placeholder="Search CommissionRunDate">
			<span class="input-group-addon custom-input-group-class">
		  		<i class="fa fa-search"></i>
		  	</span>
		</div>
   		
   		
	</div>
</th>

<th  rowspan="1" colspan="1" style="width: 182px;" aria-label="ContractID">
	<span class="htitle">ContractID</span>
    	<a href="javascript:void(0)" class="custom-sort-btn" data-order-by-key = "ContractID" data-order-by-val = "desc">
   			<span class="sort-btn btn-desc  no_pay">▲</span>
   		</a>
   		<a href="javascript:void(0)" class="custom-sort-btn" data-order-by-key = "ContractID" data-order-by-val = "asc">
   			<span class="sort-btn btn-desc  no_pay ">▼</span>
   		</a>
	<div id="ContractID" class="sr_bx">
		<div class="input-group">
			<input type="text" placeholder="Search ContractID">
			<span class="input-group-addon custom-input-group-class">
		  		<i class="fa fa-search"></i>
		  	</span>
		</div>
   		
   	
	</div>
</th>

<th rowspan="1" colspan="1" style="width: 182px;" aria-label="ContractNumber">
	<span class="htitle">ContractNumber</span>
    <a href="javascript:void(0)" class="custom-sort-btn" data-order-by-key = "ContractNumber" data-order-by-val = "desc">
   			<span class="sort-btn btn-desc  no_pay">▲</span>
   		</a>
   		<a href="javascript:void(0)" class="custom-sort-btn" data-order-by-key = "ContractNumber" data-order-by-val = "asc">
   			<span class="sort-btn btn-desc  no_pay ">▼</span>
   		</a>
	<div id="Cnumber" class="sr_bx">
		<div class="input-group">
			<input type="text" placeholder="Search ContractNumber">
			<span class="input-group-addon custom-input-group-class">
		  		<i class="fa fa-search"></i>
		  	</span>
		</div>
   		
   		
	</div>
</th>

<th rowspan="1" colspan="1" style="width: 182px;" aria-label="LeadID">
	<span class="htitle">LeadID</span>
    <a href="javascript:void(0)" class="custom-sort-btn" data-order-by-key = "LeadID" data-order-by-val = "desc">
   			<span class="sort-btn btn-desc  no_pay">▲</span>
   		</a>
   		<a href="javascript:void(0)" class="custom-sort-btn" data-order-by-key = "LeadID" data-order-by-val = "asc">
   			<span class="sort-btn btn-desc  no_pay ">▼</span>
   		</a>
	<div id="leadid" class="sr_bx">
		<div class="input-group">
			<input type="text" placeholder="Search LeadID">
			<span class="input-group-addon custom-input-group-class">
		  		<i class="fa fa-search"></i>
		  	</span>		
		</div>
   		
   		
	</div>
</th>

<th rowspan="1" colspan="1" style="width: 182px;" aria-label="EntryDate">
	<span class="htitle">EntryDate</span>
    <a href="javascript:void(0)" class="custom-sort-btn" data-order-by-key = "EntryDate" data-order-by-val = "desc">
   			<span class="sort-btn btn-desc  no_pay">▲</span>
   		</a>
   		<a href="javascript:void(0)" class="custom-sort-btn" data-order-by-key = "EntryDate" data-order-by-val = "asc">
   			<span class="sort-btn btn-desc  no_pay ">▼</span>
   		</a>
	<div id="Entrydate" class="sr_bx">           
		<div class="input-group">
			<input type="text" placeholder="Search EntryDate">
			<span class="input-group-addon custom-input-group-class">
		  		<i class="fa fa-search"></i>
		  	</span>
		</div>
   		
   		
	</div>
</th>

<th rowspan="1" colspan="1" style="width: 182px;" aria-label="PaidBrokerID">
	<span class="htitle">PaidBrokerID</span>
    <a href="javascript:void(0)" class="custom-sort-btn" data-order-by-key = "PaidBrokerID" data-order-by-val = "desc">
   			<span class="sort-btn btn-desc  no_pay">▲</span>
   		</a>
   		<a href="javascript:void(0)" class="custom-sort-btn" data-order-by-key = "PaidBrokerID" data-order-by-val = "asc">
   			<span class="sort-btn btn-desc  no_pay ">▼</span>
   		</a>
	<div id="paidbroker" class="sr_bx">
		<div class="input-group">
			<input type="text" placeholder="Search PaidBrokerID">
			<span class="input-group-addon custom-input-group-class">
		  		<i class="fa fa-search"></i>
		  	</span>
		</div>
   		
   		
	</div>
</th>

<th rowspan="1" colspan="1" style="width: 182px;" aria-label="PaidBrokerCompanyName">
	<span class="htitle">PaidBrokerCompanyName</span>
    <a href="javascript:void(0)" class="custom-sort-btn" data-order-by-key = "PaidBrokerCompanyName" data-order-by-val = "desc">
   			<span class="sort-btn btn-desc  no_pay">▲</span>
   		</a>
   		<a href="javascript:void(0)" class="custom-sort-btn" data-order-by-key = "PaidBrokerCompanyName" data-order-by-val = "asc">
   			<span class="sort-btn btn-desc  no_pay ">▼</span>
   		</a>
	<div id="PaidBrokercompany" class="sr_bx">
		<div class="input-group">
			<input type="text" placeholder="Search PaidBrokerCompanyName">
			<span class="input-group-addon custom-input-group-class">
				<i class="fa fa-search"></i>
			</span>
		</div>
   		
   		
	</div>
</th>

<th rowspan="1" colspan="1" style="width: 182px;" aria-label="CustomerTypeName">
	<span class="htitle">CustomerTypeName</span>
    <a href="javascript:void(0)" class="custom-sort-btn" data-order-by-key = "CustomerTypeName" data-order-by-val = "desc">
   			<span class="sort-btn btn-desc  no_pay">▲</span>
   		</a>
   		<a href="javascript:void(0)" class="custom-sort-btn" data-order-by-key = "CustomerTypeName" data-order-by-val = "asc">
   			<span class="sort-btn btn-desc  no_pay ">▼</span>
   		</a>
	<div id="customertypename" class="sr_bx">
		<div class="input-group">
			<input type="text" placeholder="Search CustomerTypeName">
			<span class="input-group-addon custom-input-group-class">
		  		<i class="fa fa-search"></i>
		  	</span>
		</div>
   		
   		
	</div>
</th>

<th rowspan="1" colspan="1" style="width: 182px;" aria-label="CustomerFirstName">
	<span class="htitle">CustomerFirstName</span>
    <a href="javascript:void(0)" class="custom-sort-btn" data-order-by-key = "CustomerFirstName" data-order-by-val = "desc">
   			<span class="sort-btn btn-desc  no_pay">▲</span>
   		</a>
   		<a href="javascript:void(0)" class="custom-sort-btn" data-order-by-key = "CustomerFirstName" data-order-by-val = "asc">
   			<span class="sort-btn btn-desc  no_pay ">▼</span>
   		</a>
	<div id="customerfirstname" class="sr_bx">
		<div class="input-group">
			<input type="text" placeholder="Search CustomerFirstName">
			<span class="input-group-addon custom-input-group-class">
		  		<i class="fa fa-search"></i>
		  	</span>
		</div>
   		
   		
	</div>
</th>

<th rowspan="1" colspan="1" style="width: 182px;" aria-label="CustomerLastName">
	<span class="htitle">CustomerLastName</span>
    <a href="javascript:void(0)" class="custom-sort-btn" data-order-by-key = "CustomerLastName" data-order-by-val = "desc">
   			<span class="sort-btn btn-desc  no_pay">▲</span>
   		</a>
   		<a href="javascript:void(0)" class="custom-sort-btn" data-order-by-key = "CustomerLastName" data-order-by-val = "asc">
   			<span class="sort-btn btn-desc  no_pay ">▼</span>
   		</a>
	<div id="customerlastname" class="sr_bx">
		<div class="input-group">
			<input type="text" placeholder="Search CustomerLastName">
			<span class="input-group-addon custom-input-group-class">
		  		<i class="fa fa-search"></i>
		  	</span>
		</div>
   		
   		
	</div>
</th>

<th rowspan="1" colspan="1" style="width: 182px;" aria-label="CustomerCompanyName">
	<span class="htitle">CustomerCompanyName</span>
    <a href="javascript:void(0)" class="custom-sort-btn" data-order-by-key = "CustomerCompanyName" data-order-by-val = "desc">
   			<span class="sort-btn btn-desc  no_pay">▲</span>
   		</a>
   		<a href="javascript:void(0)" class="custom-sort-btn" data-order-by-key = "CustomerCompanyName" data-order-by-val = "asc">
   			<span class="sort-btn btn-desc  no_pay ">▼</span>
   		</a>
	<div id="customercompanyname" class="sr_bx">
		<div class="input-group">
			<input type="text" placeholder="Search CustomerCompanyName">
			<span class="input-group-addon custom-input-group-class">
		  		<i class="fa fa-search"></i>
		  	</span>
		</div>
   		
   		
	</div>
</th>

<th rowspan="1" colspan="1" style="width: 182px;" aria-label="SerivceAddress1">
	<span class="htitle">SerivceAddress1</span>
    <a href="javascript:void(0)" class="custom-sort-btn" data-order-by-key = "SerivceAddress1" data-order-by-val = "desc">
   			<span class="sort-btn btn-desc  no_pay">▲</span>
   		</a>
   		<a href="javascript:void(0)" class="custom-sort-btn" data-order-by-key = "SerivceAddress1" data-order-by-val = "asc">
   			<span class="sort-btn btn-desc  no_pay ">▼</span>
   		</a>
	<div id="serviceaddress1" class="sr_bx">
		<div class="input-group">
			<input type="text" placeholder="Search SerivceAddress1">
			<span class="input-group-addon custom-input-group-class">
		  		<i class="fa fa-search"></i>
		  	</span>
		</div>
   		
   		
	</div>
</th>

<th rowspan="1" colspan="1" style="width: 182px;" aria-label="ServiceAddress2">
	<span class="htitle">ServiceAddress2</span>
    <a href="javascript:void(0)" class="custom-sort-btn" data-order-by-key = "ServiceAddress2" data-order-by-val = "desc">
   			<span class="sort-btn btn-desc  no_pay">▲</span>
   		</a>
   		<a href="javascript:void(0)" class="custom-sort-btn" data-order-by-key = "ServiceAddress2" data-order-by-val = "asc">
   			<span class="sort-btn btn-desc  no_pay ">▼</span>
   		</a>
	<div id="Servicesddress2" class="sr_bx">
		<div class="input-group">
			<input type="text" placeholder="Search ServiceAddress2">
			<span class="input-group-addon custom-input-group-class">
		  		<i class="fa fa-search"></i>
		  	</span>
		</div>
   		
   		
	</div>
</th>

<th rowspan="1" colspan="1" style="width: 182px;" aria-label="SerivceCity">
	<span class="htitle">SerivceCity</span>
    	<a href="javascript:void(0)" class="custom-sort-btn" data-order-by-key = "SerivceCity" data-order-by-val = "desc">
   			<span class="sort-btn btn-desc  no_pay">▲</span>
   		</a>
   		<a href="javascript:void(0)" class="custom-sort-btn" data-order-by-key = "SerivceCity" data-order-by-val = "asc">
   			<span class="sort-btn btn-desc  no_pay ">▼</span>
   		</a>
	<div id="servicecity" class="sr_bx">           
		<div class="input-group">
			<input type="text" placeholder="Search SerivceCity">
			<span class="input-group-addon custom-input-group-class">
		  		<i class="fa fa-search"></i>
		  	</span>
		</div>
   		
   	
	</div>           
</th>

<th rowspan="1" colspan="1" style="width: 182px;" aria-label="ServiceStateCode">
	<span class="htitle">ServiceStateCode</span>
    <a href="javascript:void(0)" class="custom-sort-btn" data-order-by-key = "ServiceStateCode" data-order-by-val = "desc">
   			<span class="sort-btn btn-desc  no_pay">▲</span>
   		</a>
   		<a href="javascript:void(0)" class="custom-sort-btn" data-order-by-key = "ServiceStateCode" data-order-by-val = "asc">
   			<span class="sort-btn btn-desc  no_pay ">▼</span>
   		</a>
	<div id="servicestatecode" class="sr_bx">
		<div class="input-group">
			<input type="text" placeholder="Search ServiceStateCode">
			<span class="input-group-addon custom-input-group-class">
		  		<i class="fa fa-search"></i>
		  	</span>
		</div>
		
	</div>
</th>

<th rowspan="1" colspan="1" style="width: 182px;" aria-label="ServiceZipCode">
	<span class="htitle">ServiceZipCode</span>
    <a href="javascript:void(0)" class="custom-sort-btn" data-order-by-key = "ServiceZipCode" data-order-by-val = "desc">
   			<span class="sort-btn btn-desc  no_pay">▲</span>
   		</a>
   		<a href="javascript:void(0)" class="custom-sort-btn" data-order-by-key = "ServiceZipCode" data-order-by-val = "asc">
   			<span class="sort-btn btn-desc  no_pay ">▼</span>
   		</a>
	<div id="servicezipcode" class="sr_bx">
		<div class="input-group">
			<input type="text" placeholder="Search ServiceZipCode">
			<span class="input-group-addon custom-input-group-class">
		  		<i class="fa fa-search"></i>
		  	</span>
		</div>
   		
   		
	</div>
</th>

<th rowspan="1" colspan="1" style="width: 182px;" aria-label="ProductName">
	<span class="htitle">ProductName</span>
    <a href="javascript:void(0)" class="custom-sort-btn" data-order-by-key = "ProductName" data-order-by-val = "desc">
   			<span class="sort-btn btn-desc  no_pay">▲</span>
   		</a>
   		<a href="javascript:void(0)" class="custom-sort-btn" data-order-by-key = "ProductName" data-order-by-val = "asc">
   			<span class="sort-btn btn-desc  no_pay ">▼</span>
   		</a>
	<div id="productname" class="sr_bx">
		<div class="input-group">
			<input type="text" placeholder="Search ProductName">
			<span class="input-group-addon custom-input-group-class">
		  		<i class="fa fa-search"></i>
		  	</span>
		</div>
   		
   		
	</div>
</th>

<th rowspan="1" colspan="1" style="width: 182px;" aria-label="ContractRateValue">
	<span class="htitle">ContractRateValue</span>
    <a href="javascript:void(0)" class="custom-sort-btn" data-order-by-key = "ContractRateValue" data-order-by-val = "desc">
   			<span class="sort-btn btn-desc  no_pay">▲</span>
   		</a>
   		<a href="javascript:void(0)" class="custom-sort-btn" data-order-by-key = "ContractRateValue" data-order-by-val = "asc">
   			<span class="sort-btn btn-desc  no_pay ">▼</span>
   		</a>
	<div id="contractratevalue" class="sr_bx">
		<div class="input-group">
			<input type="text" placeholder="Search ContractRateValue">
			<span class="input-group-addon custom-input-group-class">
		  		<i class="fa fa-search"></i>
		  	</span>
		</div>
   		
   		
	</div>
</th>

<th rowspan="1" colspan="1" style="width: 182px;" aria-label="ContractTerm">
	<span class="htitle">ContractTerm</span>
    <a href="javascript:void(0)" class="custom-sort-btn" data-order-by-key = "ContractTerm" data-order-by-val = "desc">
   			<span class="sort-btn btn-desc  no_pay">▲</span>
   		</a>
   		<a href="javascript:void(0)" class="custom-sort-btn" data-order-by-key = "ContractTerm" data-order-by-val = "asc">
   			<span class="sort-btn btn-desc  no_pay ">▼</span>
   		</a>
	<div id="contractterm" class="sr_bx">
		<div class="input-group">
			<input type="text" placeholder="Search ContractTerm">
			<span class="input-group-addon custom-input-group-class">
		  		<i class="fa fa-search"></i>
		  	</span>
		</div>
   		
   		
	</div>           
</th>

<th rowspan="1" colspan="1" style="width: 182px;" aria-label="PlanDescription">
	<span class="htitle">PlanDescription</span>
    <a href="javascript:void(0)" class="custom-sort-btn" data-order-by-key = "PlanDescription" data-order-by-val = "desc">
   			<span class="sort-btn btn-desc  no_pay">▲</span>
   		</a>
   		<a href="javascript:void(0)" class="custom-sort-btn" data-order-by-key = "PlanDescription" data-order-by-val = "asc">
   			<span class="sort-btn btn-desc  no_pay ">▼</span>
   		</a>
	<div id="plandescription" class="sr_bx">
		<div class="input-group">
			<input type="text" placeholder="Search PlanDescription">
			<span class="input-group-addon custom-input-group-class">
		  		<i class="fa fa-search"></i>
		  	</span>
		</div>
   		
   		
	</div>           
</th>

<th rowspan="1" colspan="1" style="width: 182px;" aria-label="CommodityName">
	<span class="htitle">CommodityName</span>
    <a href="javascript:void(0)" class="custom-sort-btn" data-order-by-key = "CommodityName" data-order-by-val = "desc">
   			<span class="sort-btn btn-desc  no_pay">▲</span>
   		</a>
   		<a href="javascript:void(0)" class="custom-sort-btn" data-order-by-key = "CommodityName" data-order-by-val = "asc">
   			<span class="sort-btn btn-desc  no_pay ">▼</span>
   		</a>
	<div id="commodityname" class="sr_bx">
		<div class="input-group">
			<input type="text" placeholder="Search CommodityName">
			<span class="input-group-addon custom-input-group-class">
		  		<i class="fa fa-search"></i>
		  	</span>
		</div>
   		
   		
	</div>
</th>

<th rowspan="1" colspan="1" style="width: 182px;" aria-label="UtilityAccountNumber">
	<span class="htitle">UtilityAccountNumber</span>
    <a href="javascript:void(0)" class="custom-sort-btn" data-order-by-key = "UtilityAccountNumber" data-order-by-val = "desc">
   			<span class="sort-btn btn-desc  no_pay">▲</span>
   		</a>
   		<a href="javascript:void(0)" class="custom-sort-btn" data-order-by-key = "UtilityAccountNumber" data-order-by-val = "asc">
   			<span class="sort-btn btn-desc  no_pay ">▼</span>
   		</a>
	<div id="utilityaccname" class="sr_bx">
		<div class="input-group">
			<input type="text" placeholder="Search UtilityAccountNumber">
			<span class="input-group-addon custom-input-group-class">
		  		<i class="fa fa-search"></i>
		  	</span>
		</div>
   		
   		
	</div>
</th>

<th rowspan="1" colspan="1" style="width: 182px;" aria-label="MeterNumber">
	<span class="htitle">MeterNumber</span>
    <a href="javascript:void(0)" class="custom-sort-btn" data-order-by-key = "MeterNumber" data-order-by-val = "desc">
   			<span class="sort-btn btn-desc  no_pay">▲</span>
   		</a>
   		<a href="javascript:void(0)" class="custom-sort-btn" data-order-by-key = "MeterNumber" data-order-by-val = "asc">
   			<span class="sort-btn btn-desc  no_pay ">▼</span>
   		</a>
	<div id="metername" class="sr_bx">           
		<div class="input-group">
			<input type="text" placeholder="Search MeterNumber">
			<span class="input-group-addon custom-input-group-class">
		  		<i class="fa fa-search"></i>
		  	</span>
		</div>
   		
   		
	</div>           
</th>

<th rowspan="1" colspan="1" style="width: 182px;" aria-label="BillingServiceStatus">
	<span class="htitle">BillingServiceStatus</span>
    <a href="javascript:void(0)" class="custom-sort-btn" data-order-by-key = "BillingServiceStatus" data-order-by-val = "desc">
   			<span class="sort-btn btn-desc  no_pay">▲</span>
   		</a>
   		<a href="javascript:void(0)" class="custom-sort-btn" data-order-by-key = "BillingServiceStatus" data-order-by-val = "asc">
   			<span class="sort-btn btn-desc  no_pay ">▼</span>
   		</a>
	<div id="billingservicestatus" class="sr_bx">
		<div class="input-group">
			<input type="text" placeholder="Search BillingServiceStatus">
			<span class="input-group-addon custom-input-group-class">
		  		<i class="fa fa-search"></i>
		  	</span>
		</div>
   		
   		
	</div>
</th>

<th rowspan="1" colspan="1" style="width: 182px;" aria-label="BillingServiceDescription">
	<span class="htitle">BillingServiceDescription</span>
    <a href="javascript:void(0)" class="custom-sort-btn" data-order-by-key = "BillingServiceDescription" data-order-by-val = "desc">
   			<span class="sort-btn btn-desc  no_pay">▲</span>
   		</a>
   		<a href="javascript:void(0)" class="custom-sort-btn" data-order-by-key = "BillingServiceDescription" data-order-by-val = "asc">
   			<span class="sort-btn btn-desc  no_pay ">▼</span>
   		</a>
	<div id="billingservicedescription" class="sr_bx">
		<div class="input-group">
			<input type="text" placeholder="Search BillingServiceDescription">
			<span class="input-group-addon custom-input-group-class">
		  		<i class="fa fa-search"></i>
		  	</span>
		</div>
   		
   		
	</div>           
</th>

<th rowspan="1" colspan="1" style="width: 182px;" aria-label="ContractStatus">
	<span class="htitle">ContractStatus</span>
    <a href="javascript:void(0)" class="custom-sort-btn" data-order-by-key = "ContractStatus" data-order-by-val = "desc">
   			<span class="sort-btn btn-desc  no_pay">▲</span>
   		</a>
   		<a href="javascript:void(0)" class="custom-sort-btn" data-order-by-key = "ContractStatus" data-order-by-val = "asc">
   			<span class="sort-btn btn-desc  no_pay ">▼</span>
   		</a>
	<div id="contractstatus" class="sr_bx">           
		<div class="input-group">
			<input type="text" placeholder="Search ContractStatus">
			<span class="input-group-addon custom-input-group-class">
		  		<i class="fa fa-search"></i>
		  	</span>
		</div>

		
	</div>
</th>

<th rowspan="1" colspan="1" style="width: 182px;" aria-label="BillingCycleStartDate">
	<span class="htitle">BillingCycleStartDate</span>
    <a href="javascript:void(0)" class="custom-sort-btn" data-order-by-key = "BillingCycleStartDate" data-order-by-val = "desc">
   			<span class="sort-btn btn-desc  no_pay">▲</span>
   		</a>
   		<a href="javascript:void(0)" class="custom-sort-btn" data-order-by-key = "BillingCycleStartDate" data-order-by-val = "asc">
   			<span class="sort-btn btn-desc  no_pay ">▼</span>
   		</a>
	<div id="billingcyclestartdate" class="sr_bx">
		<div class="input-group">
			<input type="text" placeholder="Search BillingCycleStartDate">
			<span class="input-group-addon custom-input-group-class">
		  		<i class="fa fa-search"></i>
		  	</span>
		</div>
   		
   		
	</div>
</th>

<th rowspan="1" colspan="1" style="width: 182px;" aria-label="BillingCycleEndDate">
	<span class="htitle">BillingCycleEndDate</span>
    <a href="javascript:void(0)" class="custom-sort-btn" data-order-by-key = "BillingCycleEndDate" data-order-by-val = "desc">
   			<span class="sort-btn btn-desc  no_pay">▲</span>
   		</a>
   		<a href="javascript:void(0)" class="custom-sort-btn" data-order-by-key = "BillingCycleEndDate" data-order-by-val = "asc">
   			<span class="sort-btn btn-desc  no_pay ">▼</span>
   		</a>
	<div id="billingcysleenddate" class="sr_bx">           
		<div class="input-group">
			<input type="text" placeholder="Search BillingCycleEndDate">
			<span class="input-group-addon custom-input-group-class">
		  		<i class="fa fa-search"></i>
		  	</span>
		</div>
   		
   		
	</div>           
</th>

<!-- From here span remain -->
<th rowspan="1" colspan="1" style="width: 182px;" aria-label="ContractEndDate">
	<span class="htitle">ContractEndDate</span>
    <a href="javascript:void(0)" class="custom-sort-btn" data-order-by-key = "ContractEndDate" data-order-by-val = "desc">
   			<span class="sort-btn btn-desc  no_pay">▲</span>
   		</a>
   		<a href="javascript:void(0)" class="custom-sort-btn" data-order-by-key = "ContractEndDate" data-order-by-val = "asc">
   			<span class="sort-btn btn-desc  no_pay ">▼</span>
   		</a>
	<div id="contractenddate" class="sr_bx">
		<div class="input-group">
			<input type="text" placeholder="Search ContractEndDate">
			<span class="input-group-addon custom-input-group-class">
		  		<i class="fa fa-search"></i>
		  	</span>
		</div>
   		
   		
	</div>
</th>

<th rowspan="1" colspan="1" style="width: 182px;" aria-label="AccountConfirmedStartDate">
	<span class="htitle">AccountConfirmedStartDate</span>
    	<a href="javascript:void(0)" class="custom-sort-btn" data-order-by-key = "AccountConfirmedStartDate" data-order-by-val = "desc">
   			<span class="sort-btn btn-desc  no_pay">▲</span>
   		</a>
   		<a href="javascript:void(0)" class="custom-sort-btn" data-order-by-key = "AccountConfirmedStartDate" data-order-by-val = "asc">
   			<span class="sort-btn btn-desc  no_pay ">▼</span>
   		</a>
	<div id="accountconfirmedstartdate" class="sr_bx">
		<div class="input-group">
			<input type="text" placeholder="Search AccountConfirmedStartDate">
			<span class="input-group-addon custom-input-group-class">
		  		<i class="fa fa-search"></i>
		  	</span>
		</div>
   		
   	
	</div>
</th>

<th rowspan="1" colspan="1" style="width: 182px;" aria-label="UtilityID">
	<span class="htitle">UtilityID</span>
    <a href="javascript:void(0)" class="custom-sort-btn" data-order-by-key = "UtilityID" data-order-by-val = "desc">
   			<span class="sort-btn btn-desc  no_pay">▲</span>
   		</a>
   		<a href="javascript:void(0)" class="custom-sort-btn" data-order-by-key = "UtilityID" data-order-by-val = "asc">
   			<span class="sort-btn btn-desc  no_pay ">▼</span>
   		</a>
	<div id="utilityid" class="sr_bx">
		<div class="input-group">
			<input type="text" placeholder="Search UtilityID">
			<span class="input-group-addon custom-input-group-class">
		  		<i class="fa fa-search"></i>
		  	</span>
		</div>
   		
   		
	</div>
</th>

<th rowspan="1" colspan="1" style="width: 182px;" aria-label="UtilityName">
	<span class="htitle">UtilityName</span>
    <a href="javascript:void(0)" class="custom-sort-btn" data-order-by-key = "UtilityName" data-order-by-val = "desc">
   			<span class="sort-btn btn-desc  no_pay">▲</span>
   		</a>
   		<a href="javascript:void(0)" class="custom-sort-btn" data-order-by-key = "UtilityName" data-order-by-val = "asc">
   			<span class="sort-btn btn-desc  no_pay ">▼</span>
   		</a>
	<div id="utilityname" class="sr_bx">
		<div class="input-group">
			<input type="text" placeholder="Search UtilityName">
			<span class="input-group-addon custom-input-group-class">
		  		<i class="fa fa-search"></i>
		  	</span>
		</div>
   		
   		
	</div>
</th>

<th rowspan="1" colspan="1" style="width: 182px;" aria-label="UtilityShortName">
	<span class="htitle">UtilityShortName</span>
    	<a href="javascript:void(0)" class="custom-sort-btn" data-order-by-key = "UtilityShortName" data-order-by-val = "desc">
   			<span class="sort-btn btn-desc  no_pay">▲</span>
   		</a>
   		<a href="javascript:void(0)" class="custom-sort-btn" data-order-by-key = "UtilityShortName" data-order-by-val = "asc">
   			<span class="sort-btn btn-desc  no_pay ">▼</span>
   		</a>
	<div id="utilityshortname" class="sr_bx">           
		<div class="input-group">
			<input type="text" placeholder="Search UtilityShortName">
			<span class="input-group-addon custom-input-group-class">
		  		<i class="fa fa-search"></i>
		  	</span>
		</div>
   		
   	
	</div>
</th>

<th rowspan="1" colspan="1" style="width: 182px;" aria-label="PreEnrollRejectReason">
	<span class="htitle">PreEnrollRejectReason</span>
    <a href="javascript:void(0)" class="custom-sort-btn" data-order-by-key = "PreEnrollRejectReason" data-order-by-val = "desc">
   			<span class="sort-btn btn-desc  no_pay">▲</span>
   		</a>
   		<a href="javascript:void(0)" class="custom-sort-btn" data-order-by-key = "PreEnrollRejectReason" data-order-by-val = "asc">
   			<span class="sort-btn btn-desc  no_pay ">▼</span>
   		</a>
	<div id="preenrollrejectreason" class="sr_bx">
		<div class="input-group">
			<input type="text" placeholder="Search PreEnrollRejectReason">
			<span class="input-group-addon custom-input-group-class">
		  		<i class="fa fa-search"></i>
		  	</span>
		</div>
   		
   		
	</div>
</th>

<th rowspan="1" colspan="1" style="width: 182px;" aria-label="PaidSigningAgentCode">
	<span class="htitle">PaidSigningAgentCode</span>
    <a href="javascript:void(0)" class="custom-sort-btn" data-order-by-key = "PaidSigningAgentCode" data-order-by-val = "desc">
   			<span class="sort-btn btn-desc  no_pay">▲</span>
   		</a>
   		<a href="javascript:void(0)" class="custom-sort-btn" data-order-by-key = "PaidSigningAgentCode" data-order-by-val = "asc">
   			<span class="sort-btn btn-desc  no_pay ">▼</span>
   		</a>
	<div id="paidsigningagentcode" class="sr_bx">
		<div class="input-group">
			<input type="text" placeholder="Search PaidSigningAgentCode">
			<span class="input-group-addon custom-input-group-class">
		  		<i class="fa fa-search"></i>
		  	</span>
		</div>
   		
   		
	</div>
</th>

<th rowspan="1" colspan="1" style="width: 182px;" aria-label="TPVLink">
	<span class="htitle">TPVLink</span>
    <a href="javascript:void(0)" class="custom-sort-btn" data-order-by-key = "TPVLink" data-order-by-val = "desc">
   			<span class="sort-btn btn-desc  no_pay">▲</span>
   		</a>
   		<a href="javascript:void(0)" class="custom-sort-btn" data-order-by-key = "TPVLink" data-order-by-val = "asc">
   			<span class="sort-btn btn-desc  no_pay ">▼</span>
   		</a>
	<div id="tpvlink" class="sr_bx">
		<div class="input-group">
			<input type="text" placeholder="Search TPVLink">
			<span class="input-group-addon custom-input-group-class">
		  		<i class="fa fa-search"></i>
		  	</span>
		</div>
   		
   		
	</div>
</th>

<th rowspan="1" colspan="1" style="width: 182px;" aria-label="Servicestatusbillingvalue">
	<span class="htitle">Servicestatusbillingvalue</span>
    	<a href="javascript:void(0)" class="custom-sort-btn" data-order-by-key = "Servicestatusbillingvalue" data-order-by-val = "desc">
   			<span class="sort-btn btn-desc  no_pay">▲</span>
   		</a>
   		<a href="javascript:void(0)" class="custom-sort-btn" data-order-by-key = "Servicestatusbillingvalue" data-order-by-val = "asc">
   			<span class="sort-btn btn-desc  no_pay ">▼</span>
   		</a>
	<div id="servicestatusbillingvalue" class="sr_bx">
		<div class="input-group">
			<input type="text" placeholder="Search Servicestatusbillingvalue">
			<span class="input-group-addon custom-input-group-class">
		  		<i class="fa fa-search"></i>
		  	</span>
		</div>
   		
   	
	</div>
</th>

<th rowspan="1" colspan="1" style="width: 182px;" aria-label="CommissionRate">
	<span class="htitle">CommissionRate</span>
    	<a href="javascript:void(0)" class="custom-sort-btn" data-order-by-key = "CommissionRate" data-order-by-val = "desc">
   			<span class="sort-btn btn-desc  no_pay">▲</span>
   		</a>
   		<a href="javascript:void(0)" class="custom-sort-btn" data-order-by-key = "CommissionRate" data-order-by-val = "asc">
   			<span class="sort-btn btn-desc  no_pay ">▼</span>
   		</a>
	<div id="commissionrate" class="sr_bx">
		<div class="input-group">
			<input type="text" placeholder="Search CommissionRate">
			<span class="input-group-addon custom-input-group-class">
		  		<i class="fa fa-search"></i>
		  	</span>
		</div>
   		
   	
	</div>
</th>

<th rowspan="1" colspan="1" style="width: 182px;" aria-label="RateUOM">
	<span class="htitle">RateUOM</span>
    <a href="javascript:void(0)" class="custom-sort-btn" data-order-by-key = "RateUOM" data-order-by-val = "desc">
   			<span class="sort-btn btn-desc  no_pay">▲</span>
   		</a>
   		<a href="javascript:void(0)" class="custom-sort-btn" data-order-by-key = "RateUOM" data-order-by-val = "asc">
   			<span class="sort-btn btn-desc  no_pay ">▼</span>
   		</a>
	<div id="rateuom" class="sr_bx">
		<div class="input-group">
			<input type="text" placeholder="Search RateUOM">
			<span class="input-group-addon custom-input-group-class">
		  		<i class="fa fa-search"></i>
		  	</span>
		</div>
   		
   		
	</div>
</th>

<th rowspan="1" colspan="1" style="width: 182px;" aria-label="forecastusage">
	<span class="htitle">forecastusage</span>
    <a href="javascript:void(0)" class="custom-sort-btn" data-order-by-key = "forecastusage" data-order-by-val = "desc">
   			<span class="sort-btn btn-desc  no_pay">▲</span>
   		</a>
   		<a href="javascript:void(0)" class="custom-sort-btn" data-order-by-key = "forecastusage" data-order-by-val = "asc">
   			<span class="sort-btn btn-desc  no_pay ">▼</span>
   		</a>
	<div id="forecastusage" class="sr_bx">
		<div class="input-group">
			<input type="text" placeholder="Search forecastusage">
			<span class="input-group-addon custom-input-group-class">
		  		<i class="fa fa-search"></i>
		  	</span>
		</div>
   		
   		
	</div>
</th>

<th rowspan="1" colspan="1" style="width: 182px;" aria-label="CommissionAmount">
	<span class="htitle">CommissionAmount</span>
    <a href="javascript:void(0)" class="custom-sort-btn" data-order-by-key = "CommissionAmount" data-order-by-val = "desc">
   			<span class="sort-btn btn-desc  no_pay">▲</span>
   		</a>
   		<a href="javascript:void(0)" class="custom-sort-btn" data-order-by-key = "CommissionAmount" data-order-by-val = "asc">
   			<span class="sort-btn btn-desc  no_pay ">▼</span>
   		</a>
	<div id="commissionamount" class="sr_bx">
		<div class="input-group">
			<input type="text" placeholder="Search CommissionAmount">
			<span class="input-group-addon custom-input-group-class">
		  		<i class="fa fa-search"></i>
		  	</span>
		</div>
   		
   		
	</div>
</th>

<th rowspan="1" colspan="1" style="width: 182px;" aria-label="PaymentTypeName">
	<span class="htitle">PaymentTypeName</span>
    <a href="javascript:void(0)" class="custom-sort-btn" data-order-by-key = "PaymentTypeName" data-order-by-val = "desc">
   			<span class="sort-btn btn-desc  no_pay">▲</span>
   		</a>
   		<a href="javascript:void(0)" class="custom-sort-btn" data-order-by-key = "PaymentTypeName" data-order-by-val = "asc">
   			<span class="sort-btn btn-desc  no_pay ">▼</span>
   		</a>
	<div id="paymenttypename" class="sr_bx">
		<div class="input-group">
			<input type="text" placeholder="Search PaymentTypeName">
			<span class="input-group-addon custom-input-group-class">
		  		<i class="fa fa-search"></i>
		  	</span>
		</div>
   		
   		
	</div>
</th>

<th rowspan="1" colspan="1" style="width: 182px;" aria-label="DateFlowTransReceived">
	<span class="htitle">DateFlowTransReceived</span>
    <a href="javascript:void(0)" class="custom-sort-btn" data-order-by-key = "DateFlowTransReceived" data-order-by-val = "desc">
   			<span class="sort-btn btn-desc  no_pay">▲</span>
   		</a>
   		<a href="javascript:void(0)" class="custom-sort-btn" data-order-by-key = "DateFlowTransReceived" data-order-by-val = "asc">
   			<span class="sort-btn btn-desc  no_pay ">▼</span>
   		</a>
	<div id="dftreceived" class="sr_bx">
		<div class="input-group">
			<input type="text" placeholder="Search DateFlowTransReceived">
			<span class="input-group-addon custom-input-group-class">
		  		<i class="fa fa-search"></i>
		  	</span>
		</div>
   		
   		
	</div>
</th>

<th rowspan="1" colspan="1" style="width: 182px;" aria-label="FlowStartDate">
	<span class="htitle">FlowStartDate</span>
    <a href="javascript:void(0)" class="custom-sort-btn" data-order-by-key = "FlowStartDate" data-order-by-val = "desc">
   			<span class="sort-btn btn-desc  no_pay">▲</span>
   		</a>
   		<a href="javascript:void(0)" class="custom-sort-btn" data-order-by-key = "FlowStartDate" data-order-by-val = "asc">
   			<span class="sort-btn btn-desc  no_pay ">▼</span>
   		</a>
	<div id="flowstartdate" class="sr_bx">
		<div class="input-group">
			<input type="text" placeholder="Search FlowStartDate">
			<span class="input-group-addon custom-input-group-class">
		  		<i class="fa fa-search"></i>
		  	</span>
		</div>
   		
   		
	</div>
</th>

<th rowspan="1" colspan="1" style="width: 182px;" aria-label="FlowEndDate">
	<span class="htitle">FlowEndDate</span>
    <a href="javascript:void(0)" class="custom-sort-btn" data-order-by-key = "FlowEndDate" data-order-by-val = "desc">
   			<span class="sort-btn btn-desc  no_pay">▲</span>
   		</a>
   		<a href="javascript:void(0)" class="custom-sort-btn" data-order-by-key = "FlowEndDate" data-order-by-val = "asc">
   			<span class="sort-btn btn-desc  no_pay ">▼</span>
   		</a>
	<div id="flowenddate" class="sr_bx">
		<div class="input-group">
			<input type="text" placeholder="Search FlowEndDate">
			<span class="input-group-addon custom-input-group-class">
		  		<i class="fa fa-search"></i>
		  	</span>
		</div>
   		
   		
	</div>
</th>

<th rowspan="1" colspan="1" style="width: 182px;" aria-label="DropReason">
	<span class="htitle">DropReason</span>
    <a href="javascript:void(0)" class="custom-sort-btn" data-order-by-key = "DropReason" data-order-by-val = "desc">
   			<span class="sort-btn btn-desc  no_pay">▲</span>
   		</a>
   		<a href="javascript:void(0)" class="custom-sort-btn" data-order-by-key = "DropReason" data-order-by-val = "asc">
   			<span class="sort-btn btn-desc  no_pay ">▼</span>
   		</a>
	<div id="dropreason" class="sr_bx">
		<div class="input-group">
			<input type="text" placeholder="Search DropReason">
			<span class="input-group-addon custom-input-group-class">
		  		<i class="fa fa-search"></i>
		  	</span>
		</div>
   		
   		
	</div>
</th>

<th rowspan="1" colspan="1" style="width: 182px;" aria-label="UtilityRejectionDate">
	<span class="htitle">UtilityRejectionDate</span>
    <a href="javascript:void(0)" class="custom-sort-btn" data-order-by-key = "UtilityRejectionDate" data-order-by-val = "desc">
   			<span class="sort-btn btn-desc  no_pay">▲</span>
   		</a>
   		<a href="javascript:void(0)" class="custom-sort-btn" data-order-by-key = "UtilityRejectionDate" data-order-by-val = "asc">
   			<span class="sort-btn btn-desc  no_pay ">▼</span>
   		</a>
	<div id="utilityrejectiondate" class="sr_bx">
		<div class="input-group">
			<input type="text" placeholder="Search UtilityRejectionDate">
			<span class="input-group-addon custom-input-group-class">
		  		<i class="fa fa-search"></i>
		  	</span>
		</div>
   		
   		
	</div>
</th>

<th rowspan="1" colspan="1" style="width: 182px;" aria-label="UtilityRejectReason">
	<span class="htitle">UtilityRejectReason</span>
    <a href="javascript:void(0)" class="custom-sort-btn" data-order-by-key = "UtilityRejectReason" data-order-by-val = "desc">
   			<span class="sort-btn btn-desc  no_pay">▲</span>
   		</a>
   		<a href="javascript:void(0)" class="custom-sort-btn" data-order-by-key = "UtilityRejectReason" data-order-by-val = "asc">
   			<span class="sort-btn btn-desc  no_pay ">▼</span>
   		</a>
	<div id="utilityrejectreason" class="sr_bx">
		<div class="input-group">
			<input type="text" placeholder="Search UtilityRejectReason">
			<span class="input-group-addon custom-input-group-class">
		  		<i class="fa fa-search"></i>
		  	</span>
		</div>
   		
   		
	</div>
</th>

<th rowspan="1" colspan="1" style="width: 182px;" aria-label="CallCenterDropCode">
	<span class="htitle">CallCenterDropCode</span>
    <a href="javascript:void(0)" class="custom-sort-btn" data-order-by-key = "CallCenterDropCode" data-order-by-val = "desc">
   			<span class="sort-btn btn-desc  no_pay">▲</span>
   		</a>
   		<a href="javascript:void(0)" class="custom-sort-btn" data-order-by-key = "CallCenterDropCode" data-order-by-val = "asc">
   			<span class="sort-btn btn-desc  no_pay ">▼</span>
   		</a>
	<div id="ccdc" class="sr_bx">
		<div class="input-group">
			<input type="text" placeholder="Search CallCenterDropCode">
			<span class="input-group-addon custom-input-group-class">
		  		<i class="fa fa-search"></i>
		  	</span>
		</div>
   		
   		
	</div>
</th>

<th rowspan="1" colspan="1" style="width: 182px;" aria-label="ForecastAnnualUsage">
	<span class="htitle">ForecastAnnualUsage</span>
    <a href="javascript:void(0)" class="custom-sort-btn" data-order-by-key = "ForecastAnnualUsage" data-order-by-val = "desc">
   			<span class="sort-btn btn-desc  no_pay">▲</span>
   		</a>
   		<a href="javascript:void(0)" class="custom-sort-btn" data-order-by-key = "ForecastAnnualUsage" data-order-by-val = "asc">
   			<span class="sort-btn btn-desc  no_pay ">▼</span>
   		</a>
	<div id="forecastannualusage" class="sr_bx">
		<div class="input-group">
			<input type="text" placeholder="Search ForecastAnnualUsage">
			<span class="input-group-addon custom-input-group-class">
		  		<i class="fa fa-search"></i>
		  	</span>
		</div>
   		
   		
	</div>
</th>

<th rowspan="1" colspan="1" style="width: 196px;" aria-label="ForecastAnnualUsageCoverage">
	<span class="htitle">ForecastAnnualUsageCoverage</span>
    <a href="javascript:void(0)" class="custom-sort-btn" data-order-by-key = "ForecastAnnualUsageCoverage" data-order-by-val = "desc">
   			<span class="sort-btn btn-desc  no_pay">▲</span>
   		</a>
   		<a href="javascript:void(0)" class="custom-sort-btn" data-order-by-key = "ForecastAnnualUsageCoverage" data-order-by-val = "asc">
   			<span class="sort-btn btn-desc  no_pay ">▼</span>
   		</a>
	<div id="fauc" class="sr_bx">
		<div class="input-group">
			<input type="text" placeholder="Search ForecastAnnualUsageCoverage">
			<span class="input-group-addon custom-input-group-class">
		  		<i class="fa fa-search"></i>
		  	</span>
		</div>
   		
   		
	</div>
</th>

<th rowspan="1" colspan="1" style="width: 182px;" aria-label="ForecastAnnualUsageUOM">
	<span class="htitle">ForecastAnnualUsageUOM</span>
    <a href="javascript:void(0)" class="custom-sort-btn" data-order-by-key = "ForecastAnnualUsageUOM" data-order-by-val = "desc">
   			<span class="sort-btn btn-desc  no_pay">▲</span>
   		</a>
   		<a href="javascript:void(0)" class="custom-sort-btn" data-order-by-key = "ForecastAnnualUsageUOM" data-order-by-val = "asc">
   			<span class="sort-btn btn-desc  no_pay ">▼</span>
   		</a>
	<div id="ForecastAnnualUsageUOM" class="sr_bx">
		<div class="input-group">
			<input type="text" placeholder="Search ForecastAnnualUsageUOM">
			<span class="input-group-addon custom-input-group-class">
		  		<i class="fa fa-search"></i>
		  	</span>
		</div>
   		
   		
	</div>
</th>

<th rowspan="1" colspan="1" style="width: 182px;" aria-label="DwellingType">
	<span class="htitle">DwellingType</span>
    	<a href="javascript:void(0)" class="custom-sort-btn" data-order-by-key = "DwellingType" data-order-by-val = "desc">
   			<span class="sort-btn btn-desc  no_pay">▲</span>
   		</a>
   		<a href="javascript:void(0)" class="custom-sort-btn" data-order-by-key = "DwellingType" data-order-by-val = "asc">
   			<span class="sort-btn btn-desc  no_pay ">▼</span>
   		</a>
	<div id="dwellingtime" class="sr_bx">
		<div class="input-group">
			<input type="text" placeholder="Search DwellingType">
			<span class="input-group-addon custom-input-group-class">
		  		<i class="fa fa-search"></i>
		  	</span>
		</div>
   		
   	
	</div>
</th>

<th rowspan="1" colspan="1" style="width: 182px;" aria-label="TrueUpIndicator">
	<span class="htitle">TrueUpIndicator</span>
    <a href="javascript:void(0)" class="custom-sort-btn" data-order-by-key = "TrueUpIndicator" data-order-by-val = "desc">
   			<span class="sort-btn btn-desc  no_pay">▲</span>
   		</a>
   		<a href="javascript:void(0)" class="custom-sort-btn" data-order-by-key = "TrueUpIndicator" data-order-by-val = "asc">
   			<span class="sort-btn btn-desc  no_pay ">▼</span>
   		</a>
	<div id="tui" class="sr_bx">
		<div class="input-group">
			<input type="text" placeholder="Search TrueUpIndicator">
			<span class="input-group-addon custom-input-group-class">
		  		<i class="fa fa-search"></i>
		  	</span>
		</div>
   		
   		
	</div>
</th>

<th rowspan="1" colspan="1" style="width: 182px;" aria-label="DualFuelIndicator">
	<span class="htitle">DualFuelIndicator</span>
    <a href="javascript:void(0)" class="custom-sort-btn" data-order-by-key = "DualFuelIndicator" data-order-by-val = "desc">
   			<span class="sort-btn btn-desc  no_pay">▲</span>
   		</a>
   		<a href="javascript:void(0)" class="custom-sort-btn" data-order-by-key = "DualFuelIndicator" data-order-by-val = "asc">
   			<span class="sort-btn btn-desc  no_pay ">▼</span>
   		</a>
	<div id="dualhuelindicator" class="sr_bx">
		<div class="input-group">
			<input type="text" placeholder="Search DualFuelIndicator">
			<span class="input-group-addon custom-input-group-class">
		  		<i class="fa fa-search"></i>
		  	</span>
		</div>
   		
	</div>
</th>