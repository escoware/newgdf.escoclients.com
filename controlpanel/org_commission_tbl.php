<div class="col-md-12 col-lg-12">
		        		<div class="x_panel">
		        			<div class="x_title">
		        				<h4>
		        					Organization Commissions
		        				</h4>
		        			</div>

		        			<div class="x_content">

		        				<?php if( $_REQUEST['org-search'] || $_REQUEST['org-order-by'] ){ ?>
		        					<a href="<?php echo $_SERVER['PHP_SELF']; ?>" class="btn btn-info">
			        					Reset Filters
			        				</a>
		        				<?php } ?>

		        				<table class="table table-hover table-striped nowrap" cellspacing="0" width="100%">
		        					<thead>
		        						<?php
		        							include 'org_commission_thead.php';
		        						?>
		        					</thead>
		        					<tbody>
		        						
		        						<?php

		        							$org_page = $_REQUEST['starts']+1;

											if($org_page == '' ){
												$org_page=1;
											} 
											else{
												$org_page = $org_page;
											}

											if($org_page >1){
										   		$limitdt = ($org_page-1)*100;
										   		$orgminlimit = $limitdt;
										    } 
										    else{
										   		$orgminlimit = 0;
										    }

										    $orgmaxlimit = $orgminlimit + 100;

		        							// queries for Organizational commissions table
		        							$org_com_query_temp = mssql_query("SELECT CommissionRunID FROM CommissionRunBrokers INNER JOIN CommissionRuns ON CommissionRunBrokers.CommissionRunID = CommissionRuns.ID WHERE BrokerId = {$_SESSION['life_user_id']} order by CommissionRunID desc"); 

											$org_com_query_temp_res = mssql_fetch_array($org_com_query_temp);


											$org_com_query = "Select * FROM dbo.BP2_CommissionRunDetails_GDF WHERE Level1BrokerID != {$_SESSION['life_user_id']} AND PaidBrokerID = {$_SESSION['life_user_id']}";

											if($_REQUEST['comicycle'] == ''){
												
												$org_com_query = $org_com_query . " AND CommissionRunID = {$org_com_query_temp_res[0]}";
											} 
											else{ 
												$org_com_query = $org_com_query . " AND CommissionRunID = {$_REQUEST['comicycle']}";
											}

											// Add search in query as per column name, if present in url
											if( $_REQUEST['search'] ){
												$search_key = explode('_', $_REQUEST['search']);
												$search_col = $search_key[0];
												$search_val = $search_key[1];

												$org_com_query = $org_com_query . " AND {$search_col} LIKE '%{$search_val}%'";
											}

											if( $_REQUEST['order-by'] ){

												$order_key = explode('_', $_REQUEST['order-by']);
												$order_col = $order_key[0];
												$order_val = $order_key[1];

												$org_com_query = $org_com_query . " ORDER BY {$order_col} {$order_val}";

											}

											$org_com_min_query = $org_com_query . " OFFSET {$orgminlimit} ROWS FETCH NEXT 100 ROWS ONLY";

											$org_com_max_query = $org_com_query . " OFFSET {$orgmaxlimit} ROWS FETCH NEXT 100 ROWS ONLY";

											//Execute the SQL query and return records
											$org_com_query_data = mssql_query($org_com_min_query);

											if(mssql_num_rows($org_com_query_data) >0) {
												while ( $result = mssql_fetch_assoc($org_com_query_data) ){
													
													echo '<td>'. $result['CommissionRunDetailID'] .'</td>'; 
													echo '<td>'. $result['CommissionRunID'] .'</td>'; 
													echo '<td>'. $result['CustomerFirstName'] .'</td>'; 
													echo '<td>'. $result['CustomerLastName'] .'</td>'; 
													echo '<td>'. $result['CustomerCompanyName'] .'</td>'; 
													echo '<td>'. $result['ContractID'] .'</td>'; 
													echo '<td>'. $result['ContractNumber'] .'</td>'; 
													
													if( $result['EntryDate'] ){
														echo '<td>'. date('M d, Y',strtotime($result['EntryDate'])) .'</td>'; 
													} 
													else {
													  	echo '<td></td>'; 
													}
													echo '<td>'. $result['CustomerTypeName'] .'</td>'; 
													echo '<td>'. $result['UtilityAccountNumber'] .'</td>'; 
													echo '<td>'. $result['MeterNumber'] .'</td>'; 
													echo '<td>'. $result['UtilityName'] .'</td>'; 
													echo '<td>'. $result['StateCode'] .'</td>';  
													echo '<td>'. $result['CommodityName'] .'</td>'; 
													  
													if( $result['ServiceStartDate'] ) {
														echo '<td>'. date('M d, Y',strtotime($result['ServiceStartDate'])) .'</td>'; 
													} 
													else {
													  	echo '<td></td>'; 
													}
													
													if( $result['ServiceEndDate'] ){
														echo '<td>'. date('M d, Y',strtotime($result['ServiceEndDate'])) .'</td>'; 
													} 
													else {
														echo '<td></td>'; 
													}

												    echo '<td>'. $result['BilledEnergyUsage'] .'</td>'; 
												    echo '<td>'. (.001*($result['CommissionRate'])) .'</td>';    
												    echo '<td>'. $result['RateUOM'] .'</td>';  
													echo '<td>'. money_format('%(#10.2n', $result['CommissionAmount']) .'</td>';  
													echo '<td>'. $result['PaymentTypeName'] .'</td>';  
													echo '<td>'. $result['PaidBrokerID'] .'</td>';  
													echo '<td>'. $result['PaidSigningAgentCode'] .'</td>';  
													echo '<td>'. $result['PaidSigningAgentLevel'] .'</td>';  
													echo '<td>'. $result['PaidBrokerFirstName'] .'</td>';  
													echo '<td>'. $result['PaidBrokerLastName'] .'</td>';  
													echo '<td>'. $result['PaidBrokerCompanyName'] .'</td>';  
													echo '<td>'. $result['Level1BrokerID'] .'</td>';  
													echo '<td>'. $result['Level1BrokerFirstName'] .'</td>';  
													echo '<td>'. $result['Level1BrokerLastName'] .'</td>';  

													echo '<td>'. $result['Level1BrokerCompanyName'] .'</td>';  
													echo '<td>'. $result['Level1SigningAgentCode'] .'</td>';  
													echo '<td>'. $result['SerivceAddress1'] .'</td>';  
													echo '<td>'. $result['ServiceAddress2'] .'</td>';  
													echo '<td>'. $result['SerivceCity'] .'</td>';  
													echo '<td>'. $result['ServiceCounty'] .'</td>';  
													echo '<td>'. $result['ServiceStateCode'] .'</td>';  
													echo '<td>'. $result['ServiceZipCode'] .'</td>';  
													echo '<td>'. $result['ServiceCountry'] .'</td>';  

													if( preg_match( '/^\+\d(\d{3})(\d{3})(\d{4})$/', str_ireplace('-', '', trim($result['HomePhone'])),  $matches ) ){
													    echo '<td> '. $matches[1] . '-' .$matches[2] . '-' . $matches[3].' </td>';
													}
													elseif( preg_match( '/^(\d{3})(\d{3})(\d{4})$/', str_ireplace('-', '', trim($result['HomePhone'])),  $matches ) ){
														echo '<td> '. $matches[1] . '-' .$matches[2] . '-' . $matches[3].' </td>';
													}
													else{
														echo '<td> '. $result['HomePhone'].'</td>';
													}

													if( preg_match( '/^\+\d(\d{3})(\d{3})(\d{4})$/', str_ireplace('-', '', trim($result['WorkPhone'])),  $matches ) ){
													    echo '<td> '. $matches[1] . '-' .$matches[2] . '-' . $matches[3].' </td>';
													}
													elseif( preg_match( '/^(\d{3})(\d{3})(\d{4})$/', str_ireplace('-', '', trim($result['WorkPhone'])),  $matches ) ){
														echo '<td> '. $matches[1] . '-' .$matches[2] . '-' . $matches[3].' </td>';
													}
													else{
														echo '<td> '. $result['WorkPhone'] .'</td>';
													}

													if( preg_match( '/^\+\d(\d{3})(\d{3})(\d{4})$/', str_ireplace('-', '', trim($result['CellularPhone'])),  $matches ) ){
													    echo '<td> '. $matches[1] . '-' .$matches[2] . '-' . $matches[3].' </td>';
													}
													elseif( preg_match( '/^(\d{3})(\d{3})(\d{4})$/', str_ireplace('-', '', trim($result['CellularPhone'])),  $matches ) ){
														echo '<td> '. $matches[1] . '-' .$matches[2] . '-' . $matches[3].' </td>';
													}
													else{
														echo '<td> '. $result['CellularPhone'].' </td>';
													}

													echo '<td>'. $result['AltPhone'] .'</td>';  
													echo '<td>'. $result['Email'] .'</td>';  
													echo '<td>'. $result['AltEmail'] .'</td>';  
													echo '<td>'. $result['ProductCode'] .'</td>';  
													echo '<td>'. $result['RateCode'] .'</td>';  
													echo '<td>'. $result['ContractTerm'] .'</td>';  
													echo '<td>'. $result['ProductDescription'] .'</td>';  
													echo '<td>'. $result['BillingSerivceStatus'] .'</td>';  
													echo '<td>'. $result['BillingServiceDescription'] .'</td>';  
													echo '<td>'. $result['ContractStatus'] .'</td>';  
													
													if( $result['AccountConfirmedStartDate'] ){
														echo '<td>'. date('M d, Y',strtotime($result['AccountConfirmedStartDate'])) .'</td>'; 
													} 
													else {
													  	echo '<td>'. $result['AccountConfirmedStartDate'] .'</td>'; 
													}

													if( $result['FlowEndDate'] ){
													 	echo '<td>'. date('M d, Y',strtotime($result['FlowEndDate'])) .'</td>'; 
													} 
													else {
													  	echo '<td></td>'; 
													}

													if( $result['ContractEndDate'] ){
													 	echo '<td>'. date('M d, Y',strtotime($result['ContractEndDate'])) .'</td>'; 
													} 
													else {
													  	echo '<td></td>'; 
													}

													echo '<td>'. $result['UtilityID'] .'</td>';  
													echo '<td>'. $result['UtilityShortName'] .'</td>';  
													echo '<td>'. $result['PreEnrollRejectReason'] .'</td>';  
													echo '<td>'. $result['DateFlowTransReceived'] .'</td>';  

													if( $result['FlowStartDate'] ){
													 	echo '<td>'. date('M d, Y',strtotime($result['FlowStartDate'])) .'</td>'; 
													} 
													else {
													  	echo '<td></td>'; 
													}

													echo '<td>'. $result['DropReason'] .'</td>';   

													if( $result['UtilityRejectionDate'] ){
														echo '<td>'. date('M d, Y',strtotime($result['UtilityRejectionDate'])) .'</td>'; 
													} 
													else {
													  echo '<td></td>'; 
													}

													echo '<td>'. $result['UtilityRejectReason'] .'</td>'; 

												}
											}
											else{
												echo '
														<tr>
								                        	<td colspan="8">
								                        		<font face="Arial, Helvetica, sans-serif" color="#FF0000" size="+1">No Record</font>
								                        	</td>
								                    	</tr>
													';
											}
		        						?>

		        					</tbody>
		        				</table>

		        				<?php
		        					$org_pagination_query = mssql_query($org_com_max_query);

		        					if( mssql_num_rows($org_pagination_query) > 0 ){ ?>
		        				<div class="dataTables_paginate paging_simple_numbers" id="example_paginate">
		                            <ul class="pagination">
		                            	<?php 
		                            		if($page != 1){ 
		                            	?>
		                                	<li class="paginate_button previous" id="example_previous">
		                                		<?php
	                                				$_REQUEST['start'] = $_REQUEST['start'] - 1;
	                                				$r = [];
	                                				foreach ($_REQUEST as $key => $value) {
	                                					$r[] = $key.'='.$value;
	                                				}
	                                				$next_p = $PHP_SELF . '?' .implode('&', $r);
	                                			?>

		                                		<a href="<?php echo $next_p; ?>" aria-controls="example" data-dt-idx="0" tabindex="0">
		                                			Previous
		                                		</a>
		                                	</li>
		                                <?php 
		                            		} 
		                            	?>
		                                	<li class="paginate_button active">
		                                		<a href="#" aria-controls="example" data-dt-idx="1" tabindex="0">
		                                			<?php echo $page; ?>
		                                		</a>
		                                	</li>

	                                	<?php 
	                                		if($reccnt1 > 1){ 
	                                	?>

	                                		<li class="paginate_button next " id="example_next">
	                                			<?php
	                                				$_REQUEST['start'] = $_REQUEST['start'] + 1;
	                                				$r = [];
	                                				foreach ($_REQUEST as $key => $value) {
	                                					$r[] = $key.'='.$value;
	                                				}
	                                				$next_p = $PHP_SELF . '?' .implode('&', $r);
	                                			?>
	                                			<a href="<?php echo $next_p;?>" aria-controls="example" data-dt-idx="2" tabindex="0">
	                                				Next
	                                			</a>
	                                		</li>
	                                	<?php 
	                                		} 
	                                	?>
	                            	</ul>
								</div>
								<?php } ?>
		        			</div>
		        		</div>
		        	</div>	