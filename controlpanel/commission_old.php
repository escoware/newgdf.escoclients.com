<?php
	include("../include/config.php");

	if($_SESSION['life_user_id']==''){
		header("location:index.php");
		exit();
	}

	// Php code for details of commision cycle
	$com_cycle_raw_query = "SELECT CommissionRunID FROM CommissionRunBrokers INNER JOIN CommissionRuns ON CommissionRunBrokers.CommissionRunID = CommissionRuns.ID WHERE BrokerId = {$_SESSION['life_user_id']} order by  CommissionRunID desc";

	$queryheading = mssql_query($com_cycle_raw_query);

	$resultheading = mssql_fetch_array( $queryheading );
	
    if($_REQUEST['comicycle'] == ''){
		$querypct = "Select SUM(CommissionAmount) as pct FROM dbo.BP2_CommissionRunDetails_GDF WHERE Level1BrokerID = {$_SESSION['life_user_id']} AND PaidBrokerID = {$_SESSION['life_user_id']} AND CommissionRunID = {$resultheading['CommissionRunID']} ";
	} 
	else{

		$querypct = "Select SUM(CommissionAmount) as pct FROM dbo.BP2_CommissionRunDetails_GDF WHERE Level1BrokerID = {$_SESSION['life_user_id']} AND PaidBrokerID = {$_SESSION['life_user_id']} AND CommissionRunID = {$_REQUEST['comicycle']}";
	}
	
	$datapct = mssql_query( $querypct );
	$resultpct = mssql_fetch_assoc( $datapct );


	/* Second commission Cycle for organization */
    if($_REQUEST['comicycle'] == ''){
		$queryoct = "Select SUM(CommissionAmount) as oct FROM dbo.BP2_CommissionRunDetails_GDF WHERE Level1BrokerID != {$_SESSION['life_user_id']} AND PaidBrokerID = {$_SESSION['life_user_id']} AND CommissionRunID = {$resultheading['CommissionRunID']}";
	} 
	else{
		$queryoct = "Select SUM(CommissionAmount) as oct FROM dbo.BP2_CommissionRunDetails_GDF WHERE Level1BrokerID != {$_SESSION['life_user_id']} AND PaidBrokerID = {$_SESSION['life_user_id']} AND CommissionRunID = {$_REQUEST['comicycle']}";
	}
	$dataoct = mssql_query($queryoct);
 	$resultoct = mssql_fetch_assoc($dataoct);
 	setlocale(LC_MONETARY, 'en_US');
 	/* Third Grand commision code */

?>

<!DOCTYPE html>
<html lang="en">

  	<head>
    	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

	    <!-- Meta, title, CSS, favicons, etc. -->
	    <meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">

	    <title><?php echo SITE_TITLE; ?></title>

	    <!-- Bootstrap -->
	    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

	    <!-- Font Awesome -->
	    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">

	    <!-- iCheck -->
	    <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet"> 
    	<link href="css/custom.css" rel="stylesheet">
    	<link href="css/buttons.dataTables.min.css" rel="stylesheet" type="text/css">

    	<style type="text/css">
    		.custom-input-group-class{
    			background-color: #fff;
    			cursor: pointer; 
    		}
    		input[type="text"]{
    			color: #000 !important;
    			padding: 5px !important;
    		}
    	</style>
  	</head>

  	<body class="nav-md">
  		<div class="container body">
  			<div class="main_container">
  				<div class="col-md-3 left_col">
		          	<?php 
		          		// Include Left Menu
		          		include("left-menu.php");
		          	?>
		        </div>

		        <!-- top navigation -->
		        <div class="top_nav">
		          	<?php 
		          		// Include Top Menu
		          		include("top-menu.php"); 
		          	?>
		        </div>

		        <!-- page content -->
		        <div class="right_col" role="main">

		        	<!--  First box div  -->
		        	<div class="col-md-12 col-lg-12 cnt_area">
		        		<div class="x_panel">
		              		<div class="x_title">
		                		<h4 class="m-b-20">
		                			Commissions
		                		</h4>
		                	</div>

		                	<div class="x_content">
		                		<table id="datatable-responsive" class="table dt-responsive nowrap pay_on comsn_tbl" cellspacing="0" width="100%">
		                			<thead>
		                				<tr>
		                					<th class="tbl_hdng">
		                						Commission Cycle
		                					</th>

		                					<th class="tbl_hdng">
		                						Commission Total
		                					</th>

		                					<th class="tbl_hdng">
		                						Run Date
		                					</th>
		                				</tr>
		                			</thead>

		                			<?php 

		                				$commission_raw_query = "SELECT CommissionRunID, CAST(ROUND(CommissionRunBrokers.TotalCommission, 2) AS NUMERIC(35, 2)) AS TotalCommission, CONVERT(VARCHAR(10), RunDate, 111) AS RunDate FROM CommissionRunBrokers INNER JOIN CommissionRuns ON CommissionRunBrokers.CommissionRunID = CommissionRuns.ID WHERE BrokerId = '".$_SESSION['life_user_id']."' order by  CommissionRunID desc";

		                				$query = mssql_query($commission_raw_query);

		                				echo '<tbody>';
		                				
		                				while($result=mssql_fetch_assoc($query)){
		                					
		                					echo '<tr>
				                					<th>
				                						<a href="javascript:void(0)">
				                							'.$result['CommissionRunID'].'
				                						</a>	
				                					</th>
				                					<th class="com_total">
				                						'.money_format('%(.2n',$result['TotalCommission']).'
				                					</th>
				                					<th class="com_total">
				                						'.date('M d, Y',strtotime($result['RunDate'])).'
				                					</th>
				                				</tr>';
		                				}

		                				echo '</tbody>';
		                			?>
		                		</table>
		                	</div>
		                </div>
		        	</div>

		        	<!-- Second box div -->
		        	<!-- <div class="col-md-12 col-lg-12">
		        		<div class="x_panel">
		        			
		        			<div class="x_title">
		        				<h4 class="m-b-20">
		        					Details for Commission Cycle <?php echo (($_REQUEST['comicycle'] != '' ) ? $_REQUEST['comicycle'] : $resultheading['CommissionRunID']); ?>
		        				</h4>
		        			</div>

		        			<div class="x_content">
		        				<div class="row">
		        					<div class="col-md-4">
		        						<h3 class="text-center">
		        							Personal Commission Total
		        						</h3>
		        						<h4 class="text-center">
		        							<?php 
		        								echo number_format(round($resultpct['pct'], 2, PHP_ROUND_HALF_DOWN),2); 
		        							?>
		        						</h4>
		        					</div>

		        					<div class="col-md-4">
		        						<h3 class="text-center">
		        							Organization Commission Total
		        						</h3>
		        						<h4 class="text-center">
		        							<?php 
		        								echo number_format(round($resultoct['oct'], 2, PHP_ROUND_HALF_DOWN),2);
		        							?>
		        						</h4>
		        					</div>

		        					<div class="col-md-4">
		        						<h3 class="text-center">
		        							Grand Commission Total
		        						</h3>
		        						<h4 class="text-center">
		        							<?php 
		        								echo number_format(round(($resultpct['pct']+$resultoct['oct']),2,PHP_ROUND_HALF_DOWN),2); 
		        							?>
		        						</h4>
		        					</div>
		        				</div>
		        			</div>
		        		</div>
		        	</div> -->

		        	<div class="col-md-12 col-lg-12">
		        		<div class="x_panel">
		        			
		        			<div class="x_title">
		        				<h4>
		        					Personal Commissions
		        				</h4>
		        			</div>

		        			<div class="x_content">
		        				<div class="col-md-9 col-lg-9 dt-buttons">
				              		<a class="dt-button buttons-copy buttons-html5 hide" tabindex="0" aria-controls="example" id="copy_commission" href="javascript:void(0)">
				                    	<span>Copy</span>
				                    </a>
				                    <a class="dt-button buttons-csv buttons-html5" tabindex="0" aria-controls="example" id="csv_commission" data-type="csv" href="javascript:void(0)">
				                    	<span>CSV</span>
				                    </a>
				                    <a class="dt-button buttons-excel buttons-html5" tabindex="0" aria-controls="example" id="excel_commission" data-type="xlsx" href="javascript:void(0)">
				                    	<span>Excel</span>
				                    </a>
				                    <a class="dt-button buttons-pdf buttons-html5 hide" tabindex="0" aria-controls="example" id="pdf_commission" data-type="pdf" href="javascript:void(0)">
				                    	<span>PDF</span>
				                    </a>
				                    <a class="dt-button buttons-print" tabindex="0" aria-controls="example" id="print_commission" data-type="print" data-source="datatable-responsives" href="javascript:void(0)">
				                    	<span>Print</span>
				                    </a>
				                    <form target="_blank" id="export-form" class="hide"></form>
								</div>

		        				<?php

		        					if( $_REQUEST['search'] ){

		        						$temp_reset_request = $_REQUEST;

		        						unset($temp_reset_request['search']);

		        						$output = implode('&', array_map(
													    function ($v, $k) { 
													    	return sprintf("%s=%s", $k, $v); 
													    },
													    $temp_reset_request,
													    array_keys($temp_reset_request)
													));

		        						$temp_reset_request_search_uri = $_SERVER['PHP_SELF'] . ( (count($temp_reset_request) > 0 ) ? '?'.$output : '');

		        						echo '<a href="'.$temp_reset_request_search_uri.'" class="btn btn-info">
				        						Exit Search View
					        				 </a>';
		        					}

		        					if( $_REQUEST['order-by'] ){
		        						$temp_reset_request = $_REQUEST;

		        						unset($temp_reset_request['order-by']);

		        						$output = implode('&', array_map(
													    function ($v, $k) { 
													    	return sprintf("%s=%s", $k, $v); 
													    },
													    $temp_reset_request,
													    array_keys($temp_reset_request)
													));

		        						$temp_reset_request_search_uri = $_SERVER['PHP_SELF'] . ( (count($temp_reset_request) > 0 ) ? '?'.$output : '');

		        						echo '<a href="'.$temp_reset_request_search_uri.'" class="btn btn-info">
				        						Exit Sort View
					        				 </a>';
		        					}
		        				?>

		        				<table id="datatable-responsives" class="table table-hover table-striped nowrap" cellspacing="0" width="100%" id="commision_tbl">
		        					<thead>
		        						<?php 
		        							include 'include/commissions/commission_thead.php';
		        						?>
		        					</thead>

		        					<tbody>
		        						<?php

		        							$page=$_REQUEST['start']+1;

											if($page=='' ){
												$page=1;
											} 
											else{
												$page=$page;
											}

											if($page >1){
										   		$limitdt=($page-1)*100;
										   		$minlimit=$limitdt;
										    } 
										    else{
										   		$minlimit=0;
										    }

		        							$query12 = mssql_query("SELECT CommissionRunID FROM CommissionRunBrokers INNER JOIN CommissionRuns ON CommissionRunBrokers.CommissionRunID = CommissionRuns.ID WHERE BrokerId = {$_SESSION['life_user_id']} order by  CommissionRunID desc "); 
											
											$result2 = mssql_fetch_array( $query12 );


											$query = "Select * FROM dbo.BP2_CommissionRunDetails_GDF WHERE Level1BrokerID = {$_SESSION['life_user_id']} AND PaidBrokerID = {$_SESSION['life_user_id']}";

											if( $_REQUEST['comicycle'] == ''){
												$query = $query . " AND CommissionRunID = {$result2[0]}";
											} 
											else {
												$query = $query . " AND CommissionRunID = {$_REQUEST['comicycle']}";
											}

											// Add search in query as per column name, if present in url
											if( $_REQUEST['search'] ){
												$search_key = explode('_', $_REQUEST['search']);
												$search_col = $search_key[0];
												$search_val = $search_key[1];

												$query = $query . " AND {$search_col} LIKE '%{$search_val}%'";
											}

											$rawsql_for_js = $query . ($_REQUEST['order-by'] ? " ORDER BY {$order_col} {$order_val}" : " ORDER BY CommissionRunDetailID ASC");

											if( $_REQUEST['order-by'] ){

												$order_key = explode('_', $_REQUEST['order-by']);
												$order_col = $order_key[0];
												$order_val = $order_key[1];

												$query = $query . " ORDER BY {$order_col} {$order_val} OFFSET {$minlimit} ROWS FETCH NEXT 100 ROWS ONLY";

											}
											else{
												$query = $query . " ORDER BY CommissionRunDetailID ASC OFFSET {$minlimit} ROWS FETCH NEXT 100 ROWS ONLY";
											}



											//Execute the SQL query and return records
											$data = mssql_query($query);

											/* Code block for pagination */

											$maxlimit = $minlimit + 100;

											$query_max = "Select * FROM dbo.BP2_CommissionRunDetails_GDF WHERE Level1BrokerID = {$_SESSION['life_user_id']} AND PaidBrokerID = {$_SESSION['life_user_id']}";

											if( $_REQUEST['comicycle'] == ''){
												$query_max = $query_max . " AND CommissionRunID = {$result2[0]}";
											} 
											else {
												$query_max = $query_max . " AND CommissionRunID = {$_REQUEST['comicycle']}";
											}

											// Add search in query as per column name, if present in url
											if( $_REQUEST['search'] ){
												$search_key = explode('_', $_REQUEST['search']);
												$search_col = $search_key[0];
												$search_val = $search_key[1];

												$query_max = $query_max . " AND {$search_col} LIKE '%{$search_val}%'";
											}

											if( $_REQUEST['order-by'] ){

												$order_key = explode('_', $_REQUEST['order-by']);
												$order_col = $order_key[0];
												$order_val = $order_key[1];

												$query_max = $query_max . " ORDER BY {$order_col} {$order_val} OFFSET {$maxlimit} ROWS FETCH NEXT 100 ROWS ONLY";

											}
											else{
												$query_max = $query_max . " ORDER BY CommissionRunDetailID ASC OFFSET {$maxlimit} ROWS FETCH NEXT 100 ROWS ONLY";
											}

											$query_max_res = mssql_query($query_max);

											$reccnt1 = mssql_num_rows($query_max_res);

											/* Code block for pagination */
											

											/* Iterate the result from users query */
											if(mssql_num_rows($data) >0) {
												while ( $result = mssql_fetch_assoc($data) ){

													echo '<tr>';
													echo '<td class="CommissionRunDetailID_td"> '. $result['CommissionRunDetailID'] .' </td>';
													echo '<td class="CommissionRunID_td"> '. $result['CommissionRunID'] .' </td>';
													echo '<td class="CustomerFirstName_td"> '. $result['CustomerFirstName'] .' </td>';
													echo '<td class="CustomerLastName_td"> '. $result['CustomerLastName'] .' </td>';
													echo '<td class="CustomerCompanyName_td"> '. $result['CustomerCompanyName'] .' </td>';
													echo '<td class="ContractID_td"> '. $result['ContractID'] .' </td>';
													echo '<td class="ContractNumber_td"> '. $result['ContractNumber'] .' </td>';
													
													if( $result['EntryDate'] ){
														echo '<td class="EntryDate_td">'.date('M d, Y',strtotime($result['EntryDate'])).'</td>';
													} 
													else {
													  	echo '<td class="EntryDate_td"></td>';
													}
													echo '<td class="CustomerTypeName_td"> '. $result['CustomerTypeName'] .' </td>';
													echo '<td class="UtilityAccountNumber_td"> '. $result['UtilityAccountNumber'] .' </td>';
													echo '<td class="MeterNumber_td"> '. $result['MeterNumber'] .' </td>';
													echo '<td class="UtilityName_td"> '. $result['UtilityName'] .' </td>';
													echo '<td class="StateCode_td"> '. $result['StateCode'] .' </td>'; 
													echo '<td class="CommodityName_td"> '.$result['CommodityName'] .' </td>';
													  
													if( $result['ServiceStartDate'] ) {
														echo '<td class="ServiceStartDate_td">'.date('M d, Y',strtotime($result['ServiceStartDate'])).'</td>';
													} 
													else {
													  	echo '<td class="ServiceStartDate_td"></td>';
													}
													
													if( $result['ServiceEndDate'] ){
														echo '<td class="ServiceEndDate_td">'.date('M d, Y',strtotime($result['ServiceEndDate'])).'</td>';
													} 
													else {
													  	echo '<td class="ServiceEndDate_td"></td>';
													}

												    echo '<td class="BilledEnergyUsage_td"> '. $result['BilledEnergyUsage'] .' </td>';
												    echo '<td class="CommissionRate_td"> '.(.001*($result['CommissionRate'])).'</td>';   
												    echo '<td class="RateUOM_td"> '. $result['RateUOM'] .' </td>'; 
													echo '<td class="CommissionAmount_td"> '.money_format('%(.2n', $result['CommissionAmount']) .' </td>'; 
													echo '<td class="PaymentTypeName_td"> '. $result['PaymentTypeName'] .' </td>'; 
													echo '<td class="PaidBrokerID_td"> '. $result['PaidBrokerID'] .' </td>'; 
													echo '<td class="PaidSigningAgentCode_td"> '. $result['PaidSigningAgentCode'] .' </td>'; 
													echo '<td class="PaidSigningAgentLevel_td"> '. $result['PaidSigningAgentLevel'] .' </td>'; 
													echo '<td class="PaidBrokerFirstName_td"> '. $result['PaidBrokerFirstName'] .' </td>'; 
													echo '<td class="PaidBrokerLastName_td"> '. $result['PaidBrokerLastName'] .' </td>'; 
													echo '<td class="PaidBrokerCompanyName_td"> '. $result['PaidBrokerCompanyName'] .' </td>'; 
													echo '<td class="Level1BrokerID_td"> '. $result['Level1BrokerID'] .' </td>'; 
													echo '<td class="Level1BrokerFirstName_td"> '. $result['Level1BrokerFirstName'] .' </td>'; 
													echo '<td class="Level1BrokerLastName_td"> '. $result['Level1BrokerLastName'] .' </td>'; 

													echo '<td class="Level1BrokerCompanyName_td"> '. $result['Level1BrokerCompanyName'] .' </td>'; 
													echo '<td class="Level1SigningAgentCode_td"> '. $result['Level1SigningAgentCode'] .' </td>'; 
													echo '<td class="SerivceAddress1_td"> '. $result['SerivceAddress1'] .' </td>'; 
													echo '<td class="ServiceAddress2_td"> '. $result['ServiceAddress2'] .' </td>'; 
													echo '<td class="SerivceCity_td"> '. $result['SerivceCity'] .' </td>'; 
													echo '<td class="ServiceCounty_td"> '. $result['ServiceCounty'] .' </td>'; 
													echo '<td class="ServiceStateCode_td"> '. $result['ServiceStateCode'] .' </td>'; 
													echo '<td class="ServiceZipCode_td"> '. $result['ServiceZipCode'] .' </td>'; 
													echo '<td class="ServiceCountry_td"> '. $result['ServiceCountry'] .' </td>'; 

													if( preg_match( '/^\+\d(\d{3})(\d{3})(\d{4})$/', str_ireplace('-', '', trim($result['HomePhone'])),  $matches ) ){
													    echo '<td class="HomePhone_td"> '. $matches[1] . '-' .$matches[2] . '-' . $matches[3].' </td>';
													}
													elseif( preg_match( '/^(\d{3})(\d{3})(\d{4})$/', str_ireplace('-', '', trim($result['HomePhone'])),  $matches ) ){
														echo '<td class="HomePhone_td"> '. $matches[1] . '-' .$matches[2] . '-' . $matches[3].' </td>';
													}
													else{
														echo '<td class="HomePhone_td"> '. $result['HomePhone'].'</td>';
													}

													if( preg_match( '/^\+\d(\d{3})(\d{3})(\d{4})$/', str_ireplace('-', '', trim($result['WorkPhone'])),  $matches ) ){
													    echo '<td class="WorkPhone_td"> '. $matches[1] . '-' .$matches[2] . '-' . $matches[3].' </td>';
													}
													elseif( preg_match( '/^(\d{3})(\d{3})(\d{4})$/', str_ireplace('-', '', trim($result['WorkPhone'])),  $matches ) ){
														echo '<td class="WorkPhone_td"> '. $matches[1] . '-' .$matches[2] . '-' . $matches[3].' </td>';
													}
													else{
														echo '<td class="WorkPhone_td"> '. $result['WorkPhone'] .'</td>';
													}

													if( preg_match( '/^\+\d(\d{3})(\d{3})(\d{4})$/', str_ireplace('-', '', trim($result['CellularPhone'])),  $matches ) ){
													    echo '<td class="CellularPhone_td"> '. $matches[1] . '-' .$matches[2] . '-' . $matches[3].' </td>';
													}
													elseif( preg_match( '/^(\d{3})(\d{3})(\d{4})$/', str_ireplace('-', '', trim($result['CellularPhone'])),  $matches ) ){
														echo '<td class="CellularPhone_td"> '. $matches[1] . '-' .$matches[2] . '-' . $matches[3].' </td>';
													}
													else{
														echo '<td class="CellularPhone_td"> '. $result['CellularPhone'].' </td>';
													}

													echo '<td class="AltPhone_td"> '. $result['AltPhone'] .' </td>'; 
													echo '<td class="Email_td"> '. $result['Email'] .' </td>'; 
													echo '<td class="AltEmail_td"> '. $result['AltEmail'] .' </td>'; 
													echo '<td class="ProductCode_td"> '. $result['ProductCode'] .' </td>'; 
													echo '<td class="RateCode_td"> '. $result['RateCode'] .' </td>'; 
													echo '<td class="ContractTerm_td"> '. $result['ContractTerm'] .' </td>'; 
													echo '<td class="BillingSerivceStatus_td"> '. $result['BillingSerivceStatus'] .' </td>'; 
													echo '<td class="BillingServiceDescription_td"> '. $result['BillingServiceDescription'] .' </td>'; 
													echo '<td class="ContractStatus_td"> '. $result['ContractStatus'] .' </td>'; 
													
													if( $result['AccountConfirmedStartDate'] ){
														echo '<td class="AccountConfirmedStartDate_td">'.date('M d, Y',strtotime($result['AccountConfirmedStartDate'])).'</td>';
													} 
													else {
														echo '<td class="AccountConfirmedStartDate_td">'.$result['AccountConfirmedStartDate'] .' </td>';
													}

													if( $result['FlowEndDate'] ){
														echo '<td class="FlowEndDate_td">'.date('M d, Y',strtotime($result['FlowEndDate'])).'</td>';
													} 
													else {
													  	echo '<td class="FlowEndDate_td"></td>';
													}

													if( $result['ContractEndDate'] ){
													 	echo '<td class="ContractEndDate_td">'.date('M d, Y',strtotime($result['ContractEndDate'])).'</td>';
													} 
													else {
													  	echo '<td class="ContractEndDate_td"></td>';
													}

													echo '<td class="UtilityID_td"> '. $result['UtilityID'] .' </td>'; 
													echo '<td class="UtilityShortName_td"> '. $result['UtilityShortName'] .' </td>'; 
													echo '<td class="PreEnrollRejectReason_td"> '. $result['PreEnrollRejectReason'] .' </td>'; 
													echo '<td class="DateFlowTransReceived_td"> '. $result['DateFlowTransReceived'] .' </td>'; 

													if( $result['FlowStartDate'] ){
														echo '<td class="FlowStartDate_td">'.date('M d, Y',strtotime($result['FlowStartDate'])).'</td>';
													} 
													else {
													  echo '<td class="FlowStartDate_td"></td>';
													}
													
													echo '<td class="DropReason_td"> '. $result['DropReason'] .' </td>'; 
													 
													if( $result['UtilityRejectionDate'] ){
													 	echo '<td class="UtilityRejectionDate_td">'.date('M d, Y',strtotime($result['UtilityRejectionDate'])).'</td>';
													} 
													else {
													  	echo '<td class="UtilityRejectionDate_td"></td>'; 
													}

													echo '<td class="UtilityRejectReason_td"> '. $result['UtilityRejectReason'] .' </td>';

												}
											}
											else{
												echo '
														<tr>
								                        	<td colspan="8">
								                        		<font face="Arial, Helvetica, sans-serif" color="#FF0000" size="+1">No data in column</font>
								                        	</td>
								                    	</tr>
													';
											}

		        						?>
		        					</tbody>
		        				</table>

		        				<?php if( $reccnt1 > 0 ){ ?>
		        				<div class="dataTables_paginate paging_simple_numbers" id="example_paginate">
		                            <ul class="pagination">
		                            	<?php 
		                            		if($page != 1){ 
		                            	?>
		                                	<li class="paginate_button previous" id="example_previous">
		                                		<?php
	                                				$_REQUEST['start'] = $_REQUEST['start'] - 1;
	                                				$r = [];
	                                				foreach ($_REQUEST as $key => $value) {
	                                					$r[] = $key.'='.$value;
	                                				}
	                                				$next_p = $PHP_SELF . '?' .implode('&', $r);
	                                			?>

		                                		<a href="<?php echo $next_p; ?>" aria-controls="example" data-dt-idx="0" tabindex="0">
		                                			Previous
		                                		</a>
		                                	</li>
		                                <?php 
		                            		} 
		                            	?>
		                                	<li class="paginate_button active">
		                                		<a href="#" aria-controls="example" data-dt-idx="1" tabindex="0">
		                                			<?php echo $page; ?>
		                                		</a>
		                                	</li>

	                                	<?php 
	                                		if($reccnt1 > 1){ 
	                                	?>

	                                		<li class="paginate_button next " id="example_next">
	                                			<?php
	                                				$_REQUEST['start'] = $_REQUEST['start'] + 1;
	                                				$r = [];
	                                				foreach ($_REQUEST as $key => $value) {
	                                					$r[] = $key.'='.$value;
	                                				}
	                                				$next_p = $PHP_SELF . '?' .implode('&', $r);
	                                			?>
	                                			<a href="<?php echo $next_p;?>" aria-controls="example" data-dt-idx="2" tabindex="0">
	                                				Next
	                                			</a>
	                                		</li>
	                                	<?php 
	                                		} 
	                                	?>
	                            	</ul>
								</div>
								<?php } ?>
		        			</div>

		        		</div>
		        	</div>


		        	<!-- Organizational Commissions Table Starts -->

		        	<?php
                        // include 'org_commission_tbl.php'
		        	?>

		        	<!-- Organizational Commissions Table Ends -->	
  				</div>
  				<!-- /page content -->

		        <!-- footer content -->
		        <footer>
		          	<?php
		          		include("footer.php");
		          	?>
		        </footer>
		        <!-- /footer content -->

  			</div>
  		</div>

  		<!-- jQuery -->
	    <script src="../vendors/jquery/dist/jquery.min.js"></script>

	    <!-- Bootstrap -->
	    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>

	    <!-- Custom Theme Scripts -->
    	<script src="js/custom.js"></script>
    	<script type="text/javascript">
    		var vfg = "<?php echo base64_encode($rawsql_for_js); ?>";
    	</script>
    	<script type="text/javascript">

			$(document).ready(function(){
				$("#check_all").click(function(){				

					if($(this).is(":checked")){
				    
				        $(".checkall").attr("checked","checked");
				    
				    }else{
				    
				      	$(".checkall").removeAttr("checked");
				    
				    }
					
				});
			});

			function checkall(objForm){

				len = objForm.elements.length;

				var i=0;

				for( i=0 ; i<len ; i++){

					if (objForm.elements[i].type=='checkbox') 

					objForm.elements[i].checked=objForm.check_all.checked;

				}

			}

			function del_prompt(frmobj,comb){
				if(comb=='Delete'){
					if(confirm ("Are you sure you want to delete record(s)")){

						frmobj.action = "member-del.php";
						frmobj.what.value="Delete";
						frmobj.submit();
					}
					else{ 
						return false;
					}
				}
				else if(comb=='Deactivate'){
					frmobj.action = "member-del.php";
					frmobj.what.value="Deactivate";
					frmobj.submit();
				}
				else if(comb=='Activate'){
					frmobj.action = "member-del.php";
					frmobj.what.value="Activate";
					frmobj.submit();
				}
			}

			function viewdetails(uid){
				$.ajax({
					url:"user-view-details.php",
					data:{"uid":uid},
					beforeSend:function(){
						$("#popcontent").html('<div align="center"><img src="images/ajaxloader.gif"></div>');

						$('#myModal').modal({
							backdrop: 'static',
							keyboard: false
						});
					},
					success:function(data){
						$("#popcontent").html(data);
					}
				});
			}

			$(document).ready(function(){

				/* Scroll down to the table if search keywords exists in URI */

				if( (location.search.indexOf('search') != -1) || (location.search.indexOf('order-by') != -1) ){
					$('html,body').animate({
			        	scrollTop: $("#commision_tbl").offset().top},
			        'slow');
				}

				/* Scroll down to the table if search keywords exists in URI */

				var comiisionarray=[];

				$('.CommissionRunDetailID').each(function(){

					if($(this).text()!=''){
						comiisionarray.push($(this).text());
					}
				});

				if(comiisionarray.length==0){

					$('#cmrdID').hide();
					$('#cmrdIDhidesearch').addClass('hidesearch');
				}

				var CommissionRunIDarray=[];

				$('.CommissionRunID').each(function(){
					if($(this).text()!=''){
						CommissionRunIDarray.push($(this).text());
					}
				});

				if(CommissionRunIDarray.length==0){
					$('#cmrID').hide();	
					$('#cmrIDhidesearch').addClass('hidesearch');
				}
					
				var CommissionRunDatearray=[];
				$('.CommissionRunDate').each(function(){

					if($(this).text()!=''){
						CommissionRunDatearray.push($(this).text());
					}
				});
				if(CommissionRunDatearray.length==0){
					$('#cmrDate').hide();	
					$('#cmrDatehidesearch').addClass('hidesearch');
				}
			});

			/* Search on click of custom input group class in table header's search input fields STARTS*/
			$(document).on('click','.custom-input-group-class',function(e){
				e.preventDefault();

				// Searched Value
				var searched_val = $(this).siblings('input[type="text"]').val();

				if( searched_val != '' ){

					// refresh page with searched value
					var loc = location.origin +location.pathname;

					var searched_key = $(this).siblings('input[type="text"]').attr('placeholder').replace('Search ','');

					var temp_col_class = $(this).siblings('input[type="text"]').attr('placeholder').replace('Search ','') +'_td';

					var flag = false;

					$('td.'+temp_col_class).each(function(v,f){

						if( $.trim($(f).text()) != '' ){
							flag = true;
							return false;
						}

					});

					if( flag ){
						location.href = modifyUrl([searched_key,searched_val],'search');
					}
					else{
						alert('No data in column');
					}

				}
				else{
					alert('Please enter value to search');
				}
			});

			/* Search on click of custom input group class in table header's search input fields ENDS*/


			/* Sorting On arrow click as per the columns in table's header STARTS */

			$(document).on('click','.custom-sort-btn',function(e){
				e.preventDefault();

				var temp_col_class = $(this).data('order-by-key') +'_td';

				var flag = false;

				$('td.'+temp_col_class).each(function(v,f){

					if( $.trim($(f).text()) != '' ){
						flag = true;
						return false;
					}

				});

				if( flag ){
					var col = $(this).data('order-by-key');
					var order = $(this).data('order-by-val');

					// refresh page with searched value
					location.href = modifyUrl([col,order], 'order-by');
				}
				else{
					alert('No data in column');
				}

			});

			/* Sorting On arrow click as per the columns in table's header ENDS */

			/* Function to modify the URL as per new parameters */

			function modifyUrl( values, param ){

				var loc = location.origin +location.pathname;

				if( location.search == '' ){
					loc = loc + '?'+param+'='+ values[0]+'_'+values[1];
				}
				else{
					if( location.search.indexOf(param) != -1 ){
						var search_query_arr = location.search.replace('?','').split('&');

						$(search_query_arr).each(function(i,v){

							if( v.indexOf(param) != -1 ){

								var f = v.split('=');

								loc = loc + location.search.replace(f[1],values[0]+'_'+values[1]);

							}

						});
					}
					else{
						loc = loc + location.search + '&'+param+'='+ values[0]+'_'+values[1];
					}
				}

				return loc;
			}

			/* Function to modify the URL as per new parameters */


			/* Javascript for organization commission */

			/* Search on click of custom input group class in table header's search input fields STARTS*/
			$(document).on('click','.org-custom-input-group-class',function(e){
				e.preventDefault();

				// Searched Value
				var searched_val = $(this).siblings('input[type="text"]').val();

				if( searched_val != '' ){

					// refresh page with searched value
					var loc = location.origin +location.pathname;

					var searched_key = $(this).siblings('input[type="text"]').attr('placeholder').replace('Search ','');

					// location.href = modifyUrl([searched_key,searched_val],'org-search');

				}
			});

			/* Search on click of custom input group class in table header's search input fields ENDS*/


			/* Sorting On arrow click as per the columns in table's header STARTS */

			$(document).on('click','.org-custom-sort-btn',function(e){
				e.preventDefault();

				var col = $(this).data('order-by-key');
				var order = $(this).data('order-by-val');

				// refresh page with searched value
				// location.href = modifyUrl([col,order], 'org-order-by');
			});

			/* Sorting On arrow click as per the columns in table's header ENDS */


			/* Javscript to export data */

			function setses(t){
				$.ajax({
					url : 'exports.php',
					data : {
						'page' : 'commission',
						'image': vfg,
						'type' : t
					},
					method   : 'POST',
					success  : function(res){

						if( res == 'success' ){
							$('body').append($('<a>',{'href':'exports.php', 'target' : '_blank','id':'external_link_for_export'}));

							$(document).find('#external_link_for_export').get(0).click();
						}
						else if( res == 'fail' ){
							location.href = 'index.php';
						}
						else{
							setses(t);
						}
					}
				});
			}


			$(document).on('click','#csv_commission,#excel_commission,#pdf_commission',function(e){
				setses($(this).data('type'));
			});

			$(document).on('click','#print_commission',function(){

				var divToPrint = document.getElementById($(this).data('source'));
			    newWin= window.open("");
			    newWin.document.write(divToPrint.outerHTML);
			    newWin.print();
			    newWin.close();

			});

			/* Javascript to export data */
		</script>
  	</body>

</html>