<tr>
    <th rowspan="1" colspan="1" style="width: 155px;" aria-sort="ascending" aria-label="CommissionRunDetailID">
        <span class="htitle">CommissionRunDetailID</span>
          <a href="javascript:void(0)" class="custom-org-sort-btn" data-order-by-key = "CommissionRunDetailID" data-order-by-val = "desc">
            <span class="sort-btn btn-asc">▲</span>
        </a>
        <a href="javascript:void(0)" class="custom-org-sort-btn" data-order-by-key = "CommissionRunDetailID" data-order-by-val = "asc">
            <span class="sort-btn btn-desc ">▼</span>
        </a>
        <div class="input-group">
            <input type="text" placeholder="Search CommissionRunDetailID">
            <span class="input-group-addon org-custom-input-group-class">
                <i class="fa fa-search"></i>
            </span>
        </div>
      
    </th>
    <th rowspan="1" colspan="1" style="width: 155px;" aria-sort="ascending" aria-label="ID: activate to sort column descending">
        <span class="htitle">ID</span>
          <a href="javascript:void(0)" class="custom-org-sort-btn" data-order-by-key = "CommissionRunID" data-order-by-val = "desc">
            <span class="sort-btn btn-asc">▲</span>
        </a>
        <a href="javascript:void(0)" class="custom-org-sort-btn" data-order-by-key = "CommissionRunID" data-order-by-val = "asc">
            <span class="sort-btn btn-desc ">▼</span>
        </a>
        <div class="input-group">
            <input type="text" placeholder="Search CommissionRunID">
            <span class="input-group-addon org-custom-input-group-class">
                <i class="fa fa-search"></i>
            </span>
        </div>
      
    </th>
    <th rowspan="1" colspan="1" style="width: 141px;" aria-label="First Name">
        <span class="htitle">First Name</span>
           <a href="javascript:void(0)" class="custom-org-sort-btn" data-order-by-key = "CustomerFirstName" data-order-by-val = "desc">
            <span class="sort-btn btn-asc">▲</span>
        </a>
        <a href="javascript:void(0)" class="custom-org-sort-btn" data-order-by-key = "CustomerFirstName" data-order-by-val = "asc">
            <span class="sort-btn btn-desc ">▼</span>
        </a>
        <div class="input-group">
            <input type="text" placeholder="Search CustomerFirstName">
            <span class="input-group-addon org-custom-input-group-class">
                <i class="fa fa-search"></i>
            </span>
        </div>
     
    </th>
    <th rowspan="1" colspan="1" style="width: 141px;" aria-label="Last Name">Last Name
        <span class="htitle">Last Name</span>
          <a href="javascript:void(0)" class="custom-org-sort-btn" data-order-by-key = "CustomerLastName" data-order-by-val = "desc">
            <span class="sort-btn btn-asc">▲</span>
        </a>
        <a href="javascript:void(0)" class="custom-org-sort-btn" data-order-by-key = "CustomerLastName" data-order-by-val = "asc">
            <span class="sort-btn btn-desc ">▼</span>
        </a>
        <div class="input-group">
            <input type="text" placeholder="Search CustomerLastName">
            <span class="input-group-addon org-custom-input-group-class">
                <i class="fa fa-search"></i>
            </span>
        </div>
      
    </th>
    <th rowspan="1" colspan="1" style="width: 141px;" aria-label="Business Name">
        <span class="htitle">Business Name</span>
         <a href="javascript:void(0)" class="custom-org-sort-btn" data-order-by-key = "CustomerCompanyName" data-order-by-val = "desc">
            <span class="sort-btn btn-asc">▲</span>
        </a>
        <a href="javascript:void(0)" class="custom-org-sort-btn" data-order-by-key = "CustomerCompanyName" data-order-by-val = "asc">
            <span class="sort-btn btn-desc ">▼</span>
        </a>
        <div class="input-group">
            <input type="text" placeholder="Search CustomerCompanyName">
            <span class="input-group-addon org-custom-input-group-class">
                <i class="fa fa-search"></i>
            </span>
        </div>
       
    </th>
    <th rowspan="1" colspan="1" style="width: 150px;" aria-label="Contract ID: activate to sort column ascending">
        <span class="htitle">Contract ID</span>
          <a href="javascript:void(0)" class="custom-org-sort-btn" data-order-by-key = "ContractID" data-order-by-val = "desc">
            <span class="sort-btn btn-asc">▲</span>
        </a>
        <a href="javascript:void(0)" class="custom-org-sort-btn" data-order-by-key = "ContractID" data-order-by-val = "asc">
            <span class="sort-btn btn-desc ">▼</span>
        </a>
        <div class="input-group">
            <input type="text" placeholder="Search ContractID">
            <span class="input-group-addon org-custom-input-group-class">
                <i class="fa fa-search"></i>
            </span>
        </div>
      
    </th>
    <th rowspan="1" colspan="1" style="width: 150px;" aria-label="ContractNumber">
        <span class="htitle">ContractNumber</span>
        <a href="javascript:void(0)" class="custom-org-sort-btn" data-order-by-key = "ContractNumber" data-order-by-val = "desc">
            <span class="sort-btn btn-asc">▲</span>
        </a>
        <a href="javascript:void(0)" class="custom-org-sort-btn" data-order-by-key = "ContractNumber" data-order-by-val = "asc">
            <span class="sort-btn btn-desc ">▼</span>
        </a>
        <div class="input-group">
            <input type="text" placeholder="Search ContractNumber">
            <span class="input-group-addon org-custom-input-group-class">
                <i class="fa fa-search"></i>
            </span>
        </div>
        
    </th>
    <th rowspan="1" colspan="1" style="width: 169px;" aria-label="Sale Date: activate to sort column ascending">
        <span class="htitle">Sale Date</span>
        <a href="javascript:void(0)" class="custom-org-sort-btn" data-order-by-key = "EntryDate" data-order-by-val = "desc">
            <span class="sort-btn btn-asc">▲</span>
        </a>
        <a href="javascript:void(0)" class="custom-org-sort-btn" data-order-by-key = "EntryDate" data-order-by-val = "asc">
            <span class="sort-btn btn-desc ">▼</span>
        </a>
        <div class="input-group">
            <input type="text" placeholder="Search EntryDate">
            <span class="input-group-addon org-custom-input-group-class">
                <i class="fa fa-search"></i>
            </span>
        </div>
        
    </th>
    <th rowspan="1" colspan="1" style="width: 141px;" aria-label="Record Type">
        <span class="htitle">Record Type</span>
         <a href="javascript:void(0)" class="custom-org-sort-btn" data-order-by-key = "CustomerTypeName" data-order-by-val = "desc">
            <span class="sort-btn btn-asc">▲</span>
        </a>
        <a href="javascript:void(0)" class="custom-org-sort-btn" data-order-by-key = "CustomerTypeName" data-order-by-val = "asc">
            <span class="sort-btn btn-desc ">▼</span>
        </a>
        <div class="input-group">
            <input type="text" placeholder="Search CustomerTypeName">
            <span class="input-group-addon org-custom-input-group-class">
                <i class="fa fa-search"></i>
            </span>
        </div>
       
    </th>
    <th rowspan="1" colspan="1" style="width: 141px;" aria-label="Utility Account Number">
        <span class="htitle">Utility Account Number</span>
           <a href="javascript:void(0)" class="custom-org-sort-btn" data-order-by-key = "UtilityAccountNumber" data-order-by-val = "desc">
            <span class="sort-btn btn-asc">▲</span>
        </a>
        <a href="javascript:void(0)" class="custom-org-sort-btn" data-order-by-key = "UtilityAccountNumber" data-order-by-val = "asc">
            <span class="sort-btn btn-desc ">▼</span>
        </a>
        <div class="input-group">
            <input type="text" placeholder="Search UtilityAccountNumber">
            <span class="input-group-addon org-custom-input-group-class">
                <i class="fa fa-search"></i>
            </span>
        </div>
     
    </th>
    <th rowspan="1" colspan="1" style="width: 141px;" aria-label="MeterNumber">
        <span class="htitle">MeterNumber</span>
         <a href="javascript:void(0)" class="custom-org-sort-btn" data-order-by-key = "MeterNumber" data-order-by-val = "desc">
            <span class="sort-btn btn-asc">▲</span>
        </a>
        <a href="javascript:void(0)" class="custom-org-sort-btn" data-order-by-key = "MeterNumber" data-order-by-val = "asc">
            <span class="sort-btn btn-desc ">▼</span>
        </a>
        <div class="input-group">
            <input type="text" placeholder="Search MeterNumber">
            <span class="input-group-addon org-custom-input-group-class">
                <i class="fa fa-search"></i>
            </span>
        </div>
       
    </th>
    <th rowspan="1" colspan="1" style="width: 141px;" aria-label="Utility Name">
        <span class="htitle">Utility Name</span>
         <a href="javascript:void(0)" class="custom-org-sort-btn" data-order-by-key = "UtilityName" data-order-by-val = "desc">
            <span class="sort-btn btn-asc">▲</span>
        </a>
        <a href="javascript:void(0)" class="custom-org-sort-btn" data-order-by-key = "UtilityName" data-order-by-val = "asc">
            <span class="sort-btn btn-desc ">▼</span>
        </a>
        <div class="input-group">
            <input type="text" placeholder="Search UtilityName">
            <span class="input-group-addon org-custom-input-group-class">
                <i class="fa fa-search"></i>
            </span>
        </div>
       
    </th>
    <th rowspan="1" colspan="1" style="width: 141px;" aria-label="StateCode">
        <span class="htitle">StateCode</span>
         <a href="javascript:void(0)" class="custom-org-sort-btn" data-order-by-key = "StateCode" data-order-by-val = "desc">
            <span class="sort-btn btn-asc">▲</span>
        </a>
        <a href="javascript:void(0)" class="custom-org-sort-btn" data-order-by-key = "StateCode" data-order-by-val = "asc">
            <span class="sort-btn btn-desc ">▼</span>
        </a>
        <div class="input-group">
            <input type="text" placeholder="Search StateCode">
            <span class="input-group-addon org-custom-input-group-class">
                <i class="fa fa-search"></i>
            </span>
        </div>
       
    </th>
    <th rowspan="1" colspan="1" style="width: 141px;" aria-label="Commodity">
        <span class="htitle">Commodity</span>
          <a href="javascript:void(0)" class="custom-org-sort-btn" data-order-by-key = "CommodityName" data-order-by-val = "desc">
            <span class="sort-btn btn-asc">▲</span>
        </a>
        <a href="javascript:void(0)" class="custom-org-sort-btn" data-order-by-key = "CommodityName" data-order-by-val = "asc">
            <span class="sort-btn btn-desc ">▼</span>
        </a>
        <div class="input-group">
            <input type="text" placeholder="Search CommodityName">
            <span class="input-group-addon org-custom-input-group-class">
                <i class="fa fa-search"></i>
            </span>
        </div>
      
    </th>
    <th rowspan="1" colspan="1" style="width: 141px;" aria-label="ServiceStartDate">
        <span class="htitle">ServiceStartDate</span>
         <a href="javascript:void(0)" class="custom-org-sort-btn" data-order-by-key = "ServiceStartDate" data-order-by-val = "desc">
            <span class="sort-btn btn-asc">▲</span>
        </a>
        <a href="javascript:void(0)" class="custom-org-sort-btn" data-order-by-key = "ServiceStartDate" data-order-by-val = "asc">
            <span class="sort-btn btn-desc ">▼</span>
        </a>
        <div class="input-group">
            <input type="text" placeholder="Search ServiceStartDate">
            <span class="input-group-addon org-custom-input-group-class">
                <i class="fa fa-search"></i>
            </span>
        </div>
       
    </th>
    <th rowspan="1" colspan="1" style="width: 141px;" aria-label="ServiceEndDate">
        <span class="htitle">ServiceEndDate</span>
          <a href="javascript:void(0)" class="custom-org-sort-btn" data-order-by-key = "ServiceEndDate" data-order-by-val = "desc">
            <span class="sort-btn btn-asc">▲</span>
        </a>
        <a href="javascript:void(0)" class="custom-org-sort-btn" data-order-by-key = "ServiceEndDate" data-order-by-val = "asc">
            <span class="sort-btn btn-desc ">▼</span>
        </a>
        <div class="input-group">
            <input type="text" placeholder="Search ServiceEndDate">
            <span class="input-group-addon org-custom-input-group-class">
                <i class="fa fa-search"></i>
            </span>
        </div>
      
    </th>
    <th rowspan="1" colspan="1" style="width: 141px;" aria-label="BilledEnergyUsage">
        <span class="htitle">BilledEnergyUsage</span>
          <a href="javascript:void(0)" class="custom-org-sort-btn" data-order-by-key = "BilledEnergyUsage" data-order-by-val = "desc">
            <span class="sort-btn btn-asc">▲</span>
        </a>
        <a href="javascript:void(0)" class="custom-org-sort-btn" data-order-by-key = "BilledEnergyUsage" data-order-by-val = "asc">
            <span class="sort-btn btn-desc ">▼</span>
        </a>
        <div class="input-group">
            <input type="text" placeholder="Search BilledEnergyUsage">
            <span class="input-group-addon org-custom-input-group-class">
                <i class="fa fa-search"></i>
            </span>
        </div>
      
    </th>
    <th rowspan="1" colspan="1" style="width: 141px;" aria-label="Payment Amount">
        <span class="htitle">Payment Amount</span>
          <a href="javascript:void(0)" class="custom-org-sort-btn" data-order-by-key = "CommissionRate" data-order-by-val = "desc">
            <span class="sort-btn btn-asc">▲</span>
        </a>
        <a href="javascript:void(0)" class="custom-org-sort-btn" data-order-by-key = "CommissionRate" data-order-by-val = "asc">
            <span class="sort-btn btn-desc ">▼</span>
        </a>
        <div class="input-group">
            <input type="text" placeholder="Search CommissionRate">
            <span class="input-group-addon org-custom-input-group-class">
                <i class="fa fa-search"></i>
            </span>
        </div>
      
    </th>
    <th rowspan="1" colspan="1" style="width: 141px;" aria-label="RateUOM">
        <span class="htitle">RateUOM</span>
         <a href="javascript:void(0)" class="custom-org-sort-btn" data-order-by-key = "RateUOM" data-order-by-val = "desc">
            <span class="sort-btn btn-asc">▲</span>
        </a>
        <a href="javascript:void(0)" class="custom-org-sort-btn" data-order-by-key = "RateUOM" data-order-by-val = "asc">
            <span class="sort-btn btn-desc ">▼</span>
        </a>
        <div class="input-group">
            <input type="text" placeholder="Search RateUOM">
            <span class="input-group-addon org-custom-input-group-class">
                <i class="fa fa-search"></i>
            </span>
        </div>
       
    </th>
    <th rowspan="1" colspan="1" style="width: 141px;" aria-label="CommissionAmount">
        <span class="htitle">CommissionAmount</span>
          <a href="javascript:void(0)" class="custom-org-sort-btn" data-order-by-key = "CommissionAmount" data-order-by-val = "desc">
            <span class="sort-btn btn-asc">▲</span>
        </a>
        <a href="javascript:void(0)" class="custom-org-sort-btn" data-order-by-key = "CommissionAmount" data-order-by-val = "asc">
            <span class="sort-btn btn-desc ">▼</span>
        </a>
        <div class="input-group">
            <input type="text" placeholder="Search CommissionAmount">
            <span class="input-group-addon org-custom-input-group-class">
                <i class="fa fa-search"></i>
            </span>
        </div>
      
    </th>
    <th rowspan="1" colspan="1" style="width: 141px;" aria-label="PaymentTypeName">
        <span class="htitle">PaymentTypeName</span>
         <a href="javascript:void(0)" class="custom-org-sort-btn" data-order-by-key = "PaymentTypeName" data-order-by-val = "desc">
            <span class="sort-btn btn-asc">▲</span>
        </a>
        <a href="javascript:void(0)" class="custom-org-sort-btn" data-order-by-key = "PaymentTypeName" data-order-by-val = "asc">
            <span class="sort-btn btn-desc ">▼</span>
        </a>
        <div class="input-group">
            <input type="text" placeholder="Search PaymentTypeName">
            <span class="input-group-addon org-custom-input-group-class">
                <i class="fa fa-search"></i>
            </span>
        </div>
       
    </th>
    <th rowspan="1" colspan="1" style="width: 141px;" aria-label="PaidBrokerID">
        <span class="htitle">PaidBrokerID</span>
          <a href="javascript:void(0)" class="custom-org-sort-btn" data-order-by-key = "PaidBrokerID" data-order-by-val = "desc">
            <span class="sort-btn btn-asc">▲</span>
        </a>
        <a href="javascript:void(0)" class="custom-org-sort-btn" data-order-by-key = "PaidBrokerID" data-order-by-val = "asc">
            <span class="sort-btn btn-desc ">▼</span>
        </a>
        <div class="input-group">
            <input type="text" placeholder="Search PaidBrokerID">
            <span class="input-group-addon org-custom-input-group-class">
                <i class="fa fa-search"></i>
            </span>
        </div>
      
    </th>
    <th rowspan="1" colspan="1" style="width: 141px;" aria-label="PaidSigningAgentCode">
        <span class="htitle">PaidSigningAgentCode</span>
         <a href="javascript:void(0)" class="custom-org-sort-btn" data-order-by-key = "PaidSigningAgentCode" data-order-by-val = "desc">
            <span class="sort-btn btn-asc">▲</span>
        </a>
        <a href="javascript:void(0)" class="custom-org-sort-btn" data-order-by-key = "PaidSigningAgentCode" data-order-by-val = "asc">
            <span class="sort-btn btn-desc ">▼</span>
        </a>
        <div class="input-group">
            <input type="text" placeholder="Search PaidSigningAgentCode">
            <span class="input-group-addon org-custom-input-group-class">
                <i class="fa fa-search"></i>
            </span>
        </div>
       
    </th>
    <th rowspan="1" colspan="1" style="width: 141px;" aria-label="PaidSigningAgentLevel">
        <span class="htitle">PaidSigningAgentLevel</span>
         <a href="javascript:void(0)" class="custom-org-sort-btn" data-order-by-key = "PaidSigningAgentLevel" data-order-by-val = "desc">
            <span class="sort-btn btn-asc">▲</span>
        </a>
        <a href="javascript:void(0)" class="custom-org-sort-btn" data-order-by-key = "PaidSigningAgentLevel" data-order-by-val = "asc">
            <span class="sort-btn btn-desc ">▼</span>
        </a>
        <div class="input-group">
            <input type="text" placeholder="Search PaidSigningAgentLevel">
            <span class="input-group-addon org-custom-input-group-class">
                <i class="fa fa-search"></i>
            </span>
        </div>
       
    </th>
    <th rowspan="1" colspan="1" style="width: 141px;" aria-label="PaidBrokerFirstName">
        <span class="htitle">PaidBrokerFirstName</span>
          <a href="javascript:void(0)" class="custom-org-sort-btn" data-order-by-key = "PaidBrokerFirstName" data-order-by-val = "desc">
            <span class="sort-btn btn-asc">▲</span>
        </a>
        <a href="javascript:void(0)" class="custom-org-sort-btn" data-order-by-key = "PaidBrokerFirstName" data-order-by-val = "asc">
            <span class="sort-btn btn-desc ">▼</span>
        </a>
        <div class="input-group">
            <input type="text" placeholder="Search PaidBrokerFirstName">
            <span class="input-group-addon org-custom-input-group-class">
                <i class="fa fa-search"></i>
            </span>
        </div>
      
    </th>
    <th rowspan="1" colspan="1" style="width: 141px;" aria-label="PaidBrokerLastName">
        <span class="htitle">PaidBrokerLastName</span>
         <a href="javascript:void(0)" class="custom-org-sort-btn" data-order-by-key = "PaidBrokerLastName" data-order-by-val = "desc">
            <span class="sort-btn btn-asc">▲</span>
        </a>
        <a href="javascript:void(0)" class="custom-org-sort-btn" data-order-by-key = "PaidBrokerLastName" data-order-by-val = "asc">
            <span class="sort-btn btn-desc ">▼</span>
        </a>
        <div class="input-group">
            <input type="text" placeholder="Search PaidBrokerLastName">
            <span class="input-group-addon org-custom-input-group-class">
                <i class="fa fa-search"></i>
            </span>
        </div>
       
    </th>
    <th rowspan="1" colspan="1" style="width: 141px;" aria-label="PaidBrokerCompanyName">
        <span class="htitle">PaidBrokerCompanyName</span>
         <a href="javascript:void(0)" class="custom-org-sort-btn" data-order-by-key = "PaidBrokerCompanyName" data-order-by-val = "desc">
            <span class="sort-btn btn-asc">▲</span>
        </a>
        <a href="javascript:void(0)" class="custom-org-sort-btn" data-order-by-key = "PaidBrokerCompanyName" data-order-by-val = "asc">
            <span class="sort-btn btn-desc ">▼</span>
        </a>
        <div class="input-group">
            <input type="text" placeholder="Search PaidBrokerCompanyName">
            <span class="input-group-addon org-custom-input-group-class">
                <i class="fa fa-search"></i>
            </span>
        </div>
       
    </th>
    <th rowspan="1" colspan="1" style="width: 141px;" aria-label="BrokerID">
        <span class="htitle">BrokerID</span>
         <a href="javascript:void(0)" class="custom-org-sort-btn" data-order-by-key = "Level1BrokerID" data-order-by-val = "desc">
            <span class="sort-btn btn-asc">▲</span>
        </a>
        <a href="javascript:void(0)" class="custom-org-sort-btn" data-order-by-key = "Level1BrokerID" data-order-by-val = "asc">
            <span class="sort-btn btn-desc ">▼</span>
        </a>
        <div class="input-group">
            <input type="text" placeholder="Search Level1BrokerID">
            <span class="input-group-addon org-custom-input-group-class">
                <i class="fa fa-search"></i>
            </span>
        </div>
       
    </th>
    <th rowspan="1" colspan="1" style="width: 141px;" aria-label="Level1BrokerFirstName">
        <span class="htitle">Level1BrokerFirstName</span>
         <a href="javascript:void(0)" class="custom-org-sort-btn" data-order-by-key = "Level1BrokerFirstName" data-order-by-val = "desc">
            <span class="sort-btn btn-asc">▲</span>
        </a>
        <a href="javascript:void(0)" class="custom-org-sort-btn" data-order-by-key = "Level1BrokerFirstName" data-order-by-val = "asc">
            <span class="sort-btn btn-desc ">▼</span>
        </a>
        <div class="input-group">
            <input type="text" placeholder="Search Level1BrokerFirstName">
            <span class="input-group-addon org-custom-input-group-class">
                <i class="fa fa-search"></i>
            </span>
        </div>
       
    </th>
    <th rowspan="1" colspan="1" style="width: 141px;" aria-label="Level1BrokerLastName">
        <span class="htitle">Level1BrokerLastName</span>
          <a href="javascript:void(0)" class="custom-org-sort-btn" data-order-by-key = "Level1BrokerLastName" data-order-by-val = "desc">
            <span class="sort-btn btn-asc">▲</span>
        </a>
        <a href="javascript:void(0)" class="custom-org-sort-btn" data-order-by-key = "Level1BrokerLastName" data-order-by-val = "asc">
            <span class="sort-btn btn-desc ">▼</span>
        </a>
        <div class="input-group">
            <input type="text" placeholder="Search Level1BrokerLastName">
            <span class="input-group-addon org-custom-input-group-class">
                <i class="fa fa-search"></i>
            </span>
        </div>
      
    </th>
    <th rowspan="1" colspan="1" style="width: 141px;" aria-label="BrokerName">
        <span class="htitle">BrokerName</span>
          <a href="javascript:void(0)" class="custom-org-sort-btn" data-order-by-key = "Level1BrokerCompanyName" data-order-by-val = "desc">
            <span class="sort-btn btn-asc">▲</span>
        </a>
        <a href="javascript:void(0)" class="custom-org-sort-btn" data-order-by-key = "Level1BrokerCompanyName" data-order-by-val = "asc">
            <span class="sort-btn btn-desc ">▼</span>
        </a>
        <div class="input-group">
            <input type="text" placeholder="Search Level1BrokerCompanyName">
            <span class="input-group-addon org-custom-input-group-class">
                <i class="fa fa-search"></i>
            </span>
        </div>
      
    </th> 
    <th rowspan="1" colspan="1" style="width: 141px;" aria-label="Sales Agent ID">
        <span class="htitle">Sales Agent ID</span>
        <a href="javascript:void(0)" class="custom-org-sort-btn" data-order-by-key = "Level1SigningAgentCode" data-order-by-val = "desc">
            <span class="sort-btn btn-asc">▲</span>
        </a>
        <a href="javascript:void(0)" class="custom-org-sort-btn" data-order-by-key = "Level1SigningAgentCode" data-order-by-val = "asc">
            <span class="sort-btn btn-desc ">▼</span>
        </a>
        <div class="input-group">
            <input type="text" placeholder="Search Level1SigningAgentCode">
            <span class="input-group-addon org-custom-input-group-class">
                <i class="fa fa-search"></i>
            </span>
        </div>
        
    </th>  
    <th rowspan="1" colspan="1" style="width: 141px;" aria-label="Service Address 1">
        <span class="htitle">Service Address 1</span>
          <a href="javascript:void(0)" class="custom-org-sort-btn" data-order-by-key = "SerivceAddress1" data-order-by-val = "desc">
            <span class="sort-btn btn-asc">▲</span>
        </a>
        <a href="javascript:void(0)" class="custom-org-sort-btn" data-order-by-key = "SerivceAddress1" data-order-by-val = "asc">
            <span class="sort-btn btn-desc ">▼</span>
        </a>
        <div class="input-group">
            <input type="text" placeholder="Search SerivceAddress1">
            <span class="input-group-addon org-custom-input-group-class">
                <i class="fa fa-search"></i>
            </span>
        </div>
      
    </th>
    <th rowspan="1" colspan="1" style="width: 141px;" aria-label="Service Address 2">
        <span class="htitle">Service Address 2</span>
         <a href="javascript:void(0)" class="custom-org-sort-btn" data-order-by-key = "ServiceAddress2" data-order-by-val = "desc">
            <span class="sort-btn btn-asc">▲</span>
        </a>
        <a href="javascript:void(0)" class="custom-org-sort-btn" data-order-by-key = "ServiceAddress2" data-order-by-val = "asc">
            <span class="sort-btn btn-desc ">▼</span>
        </a>
        <div class="input-group">
            <input type="text" placeholder="Search ServiceAddress2">
            <span class="input-group-addon org-custom-input-group-class">
                <i class="fa fa-search"></i>
            </span>
        </div>
       
    </th>
    <th rowspan="1" colspan="1" style="width: 141px;" aria-label="Service City">
        <span class="htitle">Service City</span>
          <a href="javascript:void(0)" class="custom-org-sort-btn" data-order-by-key = "SerivceCity" data-order-by-val = "desc">
            <span class="sort-btn btn-asc">▲</span>
        </a>
        <a href="javascript:void(0)" class="custom-org-sort-btn" data-order-by-key = "SerivceCity" data-order-by-val = "asc">
            <span class="sort-btn btn-desc ">▼</span>
        </a>
        <div class="input-group">
            <input type="text" placeholder="Search SerivceCity">
            <span class="input-group-addon org-custom-input-group-class">
                <i class="fa fa-search"></i>
            </span>
        </div>
      
    </th>
    <th rowspan="1" colspan="1" style="width: 141px;" aria-label="ServiceCounty">
        <span class="htitle">ServiceCounty</span>
          <a href="javascript:void(0)" class="custom-org-sort-btn" data-order-by-key = "ServiceCounty" data-order-by-val = "desc">
            <span class="sort-btn btn-asc">▲</span>
        </a>
        <a href="javascript:void(0)" class="custom-org-sort-btn" data-order-by-key = "ServiceCounty" data-order-by-val = "asc">
            <span class="sort-btn btn-desc ">▼</span>
        </a>
        <div class="input-group">
            <input type="text" placeholder="Search ServiceCounty">
            <span class="input-group-addon org-custom-input-group-class">
                <i class="fa fa-search"></i>
            </span>
        </div>
      
    </th>
    <th rowspan="1" colspan="1" style="width: 141px;" aria-label="Service  ST">
        <span class="htitle">Service ST</span>
        <a href="javascript:void(0)" class="custom-org-sort-btn" data-order-by-key = "ServiceStateCode" data-order-by-val = "desc">
            <span class="sort-btn btn-asc">▲</span>
        </a>
        <a href="javascript:void(0)" class="custom-org-sort-btn" data-order-by-key = "ServiceStateCode" data-order-by-val = "asc">
            <span class="sort-btn btn-desc ">▼</span>
        </a>
        <div class="input-group">
            <input type="text" placeholder="Search ServiceStateCode">
            <span class="input-group-addon org-custom-input-group-class">
                <i class="fa fa-search"></i>
            </span>
        </div>
        
    </th>
    <th rowspan="1" colspan="1" style="width: 141px;" aria-label="Service Zip">
        <span class="htitle">Service Zip</span>
         <a href="javascript:void(0)" class="custom-org-sort-btn" data-order-by-key = "ServiceZipCode" data-order-by-val = "desc">
            <span class="sort-btn btn-asc">▲</span>
        </a>
        <a href="javascript:void(0)" class="custom-org-sort-btn" data-order-by-key = "ServiceZipCode" data-order-by-val = "asc">
            <span class="sort-btn btn-desc ">▼</span>
        </a>
        <div class="input-group">
            <input type="text" placeholder="Search ServiceZipCode">
            <span class="input-group-addon org-custom-input-group-class">
                <i class="fa fa-search"></i>
            </span>
        </div>
       
    </th>
    <th rowspan="1" colspan="1" style="width: 141px;" aria-label="ServiceCountry">
        <span class="htitle">ServiceCountry</span>
         <a href="javascript:void(0)" class="custom-org-sort-btn" data-order-by-key = "ServiceCountry" data-order-by-val = "desc">
            <span class="sort-btn btn-asc">▲</span>
        </a>
        <a href="javascript:void(0)" class="custom-org-sort-btn" data-order-by-key = "ServiceCountry" data-order-by-val = "asc">
            <span class="sort-btn btn-desc ">▼</span>
        </a>
        <div class="input-group">
            <input type="text" placeholder="Search ServiceCountry">
            <span class="input-group-addon org-custom-input-group-class">
                <i class="fa fa-search"></i>
            </span>
        </div>
       
    </th>
    <th rowspan="1" colspan="1" style="width: 141px;" aria-label="Customer Phone">
        <span class="htitle">Customer Phone</span>
          <a href="javascript:void(0)" class="custom-org-sort-btn" data-order-by-key = "HomePhone" data-order-by-val = "desc">
            <span class="sort-btn btn-asc">▲</span>
        </a>
        <a href="javascript:void(0)" class="custom-org-sort-btn" data-order-by-key = "HomePhone" data-order-by-val = "asc">
            <span class="sort-btn btn-desc ">▼</span>
        </a>
        <div class="input-group">
            <input type="text" placeholder="Search HomePhone">
            <span class="input-group-addon org-custom-input-group-class">
                <i class="fa fa-search"></i>
            </span>
        </div>
      
    </th>
    <th rowspan="1" colspan="1" style="width: 141px;" aria-label="WorkPhone">
        <span class="htitle">WorkPhone</span>
          <a href="javascript:void(0)" class="custom-org-sort-btn" data-order-by-key = "WorkPhone" data-order-by-val = "desc">
            <span class="sort-btn btn-asc">▲</span>
        </a>
        <a href="javascript:void(0)" class="custom-org-sort-btn" data-order-by-key = "WorkPhone" data-order-by-val = "asc">
            <span class="sort-btn btn-desc ">▼</span>
        </a>
        <div class="input-group">
            <input type="text" placeholder="Search WorkPhone">
            <span class="input-group-addon org-custom-input-group-class">
                <i class="fa fa-search"></i>
            </span>
        </div>
      
    </th>
    <th rowspan="1" colspan="1" style="width: 141px;" aria-label="CellularPhone">
        <span class="htitle">CellularPhone</span>
          <a href="javascript:void(0)" class="custom-org-sort-btn" data-order-by-key = "CellularPhone" data-order-by-val = "desc">
            <span class="sort-btn btn-asc">▲</span>
        </a>
        <a href="javascript:void(0)" class="custom-org-sort-btn" data-order-by-key = "CellularPhone" data-order-by-val = "asc">
            <span class="sort-btn btn-desc ">▼</span>
        </a>
        <div class="input-group">
            <input type="text" placeholder="Search CellularPhone">
            <span class="input-group-addon org-custom-input-group-class">
                <i class="fa fa-search"></i>
            </span>
        </div>
      
    </th>
    <th rowspan="1" colspan="1" style="width: 141px;" aria-label="AltPhone">
        <span class="htitle">AltPhone</span>
          <a href="javascript:void(0)" class="custom-org-sort-btn" data-order-by-key = "AltPhone" data-order-by-val = "desc">
            <span class="sort-btn btn-asc">▲</span>
        </a>
        <a href="javascript:void(0)" class="custom-org-sort-btn" data-order-by-key = "AltPhone" data-order-by-val = "asc">
            <span class="sort-btn btn-desc ">▼</span>
        </a>
        <div class="input-group">
            <input type="text" placeholder="Search AltPhone">
            <span class="input-group-addon org-custom-input-group-class">
                <i class="fa fa-search"></i>
            </span>
        </div>
      
    </th>
    <th rowspan="1" colspan="1" style="width: 141px;" aria-label="Customer Email">
        <span class="htitle">Customer Email</span>
         <a href="javascript:void(0)" class="custom-org-sort-btn" data-order-by-key = "Email" data-order-by-val = "desc">
            <span class="sort-btn btn-asc">▲</span>
        </a>
        <a href="javascript:void(0)" class="custom-org-sort-btn" data-order-by-key = "Email" data-order-by-val = "asc">
            <span class="sort-btn btn-desc ">▼</span>
        </a>
        <div class="input-group">
            <input type="text" placeholder="Search Email">
            <span class="input-group-addon org-custom-input-group-class">
                <i class="fa fa-search"></i>
            </span>
        </div>
       
    </th>
    <th rowspan="1" colspan="1" style="width: 141px;" aria-label="AltEmail">
        <span class="htitle">AltEmail</span>
          <a href="javascript:void(0)" class="custom-org-sort-btn" data-order-by-key = "AltEmail" data-order-by-val = "desc">
            <span class="sort-btn btn-asc">▲</span>
        </a>
        <a href="javascript:void(0)" class="custom-org-sort-btn" data-order-by-key = "AltEmail" data-order-by-val = "asc">
            <span class="sort-btn btn-desc ">▼</span>
        </a>
        <div class="input-group">
            <input type="text" placeholder="Search AltEmail">
            <span class="input-group-addon org-custom-input-group-class">
                <i class="fa fa-search"></i>
            </span>
        </div>
      
    </th>
    <th rowspan="1" colspan="1" style="width: 141px;" aria-label="Product Code">
        <span class="htitle">Product Code</span>
          <a href="javascript:void(0)" class="custom-org-sort-btn" data-order-by-key = "ProductCode" data-order-by-val = "desc">
            <span class="sort-btn btn-asc">▲</span>
        </a>
        <a href="javascript:void(0)" class="custom-org-sort-btn" data-order-by-key = "ProductCode" data-order-by-val = "asc">
            <span class="sort-btn btn-desc ">▼</span>
        </a>
        <div class="input-group">
            <input type="text" placeholder="Search ProductCode">
            <span class="input-group-addon org-custom-input-group-class">
                <i class="fa fa-search"></i>
            </span>
        </div>
      
    </th>
    <th rowspan="1" colspan="1" style="width: 141px;" aria-label="RateCode">
        <span class="htitle">RateCode</span>
          <a href="javascript:void(0)" class="custom-org-sort-btn" data-order-by-key = "RateCode" data-order-by-val = "desc">
            <span class="sort-btn btn-asc">▲</span>
        </a>
        <a href="javascript:void(0)" class="custom-org-sort-btn" data-order-by-key = "RateCode" data-order-by-val = "asc">
            <span class="sort-btn btn-desc ">▼</span>
        </a>
        <div class="input-group">
            <input type="text" placeholder="Search RateCode">
            <span class="input-group-addon org-custom-input-group-class">
                <i class="fa fa-search"></i>
            </span>
        </div>
      
    </th>
    <th rowspan="1" colspan="1" style="width: 141px;" aria-label="ContractTerm">
        <span class="htitle">ContractTerm</span>
          <a href="javascript:void(0)" class="custom-org-sort-btn" data-order-by-key = "ContractTerm" data-order-by-val = "desc">
            <span class="sort-btn btn-asc">▲</span>
        </a>
        <a href="javascript:void(0)" class="custom-org-sort-btn" data-order-by-key = "ContractTerm" data-order-by-val = "asc">
            <span class="sort-btn btn-desc ">▼</span>
        </a>
        <div class="input-group">
            <input type="text" placeholder="Search ContractTerm">
            <span class="input-group-addon org-custom-input-group-class">
                <i class="fa fa-search"></i>
            </span>
        </div>
      
    </th>
    <th rowspan="1" colspan="1" style="width: 141px;" aria-label="ProductDescription">
        <span class="htitle">ProductDescription</span>
         <a href="javascript:void(0)" class="custom-org-sort-btn" data-order-by-key = "ProductDescription" data-order-by-val = "desc">
            <span class="sort-btn btn-asc">▲</span>
        </a>
        <a href="javascript:void(0)" class="custom-org-sort-btn" data-order-by-key = "ProductDescription" data-order-by-val = "asc">
            <span class="sort-btn btn-desc ">▼</span>
        </a>
        <div class="input-group">
            <input type="text" placeholder="Search ProductDescription">
            <span class="input-group-addon org-custom-input-group-class">
                <i class="fa fa-search"></i>
            </span>
        </div>
       
    </th>
    <th rowspan="1" colspan="1" style="width: 141px;" aria-label="Status">
        <span class="htitle">Status</span>
          <a href="javascript:void(0)" class="custom-org-sort-btn" data-order-by-key = "BillingSerivceStatus" data-order-by-val = "desc">
            <span class="sort-btn btn-asc">▲</span>
        </a>
        <a href="javascript:void(0)" class="custom-org-sort-btn" data-order-by-key = "BillingSerivceStatus" data-order-by-val = "asc">
            <span class="sort-btn btn-desc ">▼</span>
        </a>
        <div class="input-group">
            <input type="text" placeholder="Search BillingSerivceStatus">
            <span class="input-group-addon org-custom-input-group-class">
                <i class="fa fa-search"></i>
            </span>
        </div>
      
    </th>
    <th rowspan="1" colspan="1" style="width: 141px;" aria-label="Billing Service Descriptions">
        <span class="htitle">Billing Service Descriptions</span>
          <a href="javascript:void(0)" class="custom-org-sort-btn" data-order-by-key = "BillingServiceDescription" data-order-by-val = "desc">
            <span class="sort-btn btn-asc">▲</span>
        </a>
        <a href="javascript:void(0)" class="custom-org-sort-btn" data-order-by-key = "BillingServiceDescription" data-order-by-val = "asc">
            <span class="sort-btn btn-desc ">▼</span>
        </a>
        <div class="input-group">
            <input type="text" placeholder="Search BillingServiceDescription">
            <span class="input-group-addon org-custom-input-group-class">
                <i class="fa fa-search"></i>
            </span>
        </div>
      
    </th>
    <th rowspan="1" colspan="1" style="width: 141px;" aria-label="Contract Status">
        <span class="htitle">Contract Status</span>
         <a href="javascript:void(0)" class="custom-org-sort-btn" data-order-by-key = "ContractStatus" data-order-by-val = "desc">
            <span class="sort-btn btn-asc">▲</span>
        </a>
        <a href="javascript:void(0)" class="custom-org-sort-btn" data-order-by-key = "ContractStatus" data-order-by-val = "asc">
            <span class="sort-btn btn-desc ">▼</span>
        </a>
        <div class="input-group">
            <input type="text" placeholder="Search ContractStatus">
            <span class="input-group-addon org-custom-input-group-class">
                <i class="fa fa-search"></i>
            </span>
        </div>
       
    </th>
    <th rowspan="1" colspan="1" style="width: 141px;" aria-label="Confirmed Service Start Date">
        <span class="htitle">Confirmed Service Start Date</span>
          <a href="javascript:void(0)" class="custom-org-sort-btn" data-order-by-key = "AccountConfirmedStartDate" data-order-by-val = "desc">
            <span class="sort-btn btn-asc">▲</span>
        </a>
        <a href="javascript:void(0)" class="custom-org-sort-btn" data-order-by-key = "AccountConfirmedStartDate" data-order-by-val = "asc">
            <span class="sort-btn btn-desc ">▼</span>
        </a>
        <div class="input-group">
            <input type="text" placeholder="Search AccountConfirmedStartDate">
            <span class="input-group-addon org-custom-input-group-class">
                <i class="fa fa-search"></i>
            </span>
        </div>
      
    </th>       
    <th rowspan="1" colspan="1" style="width: 141px;" aria-label="FlowEndDate">
        <span class="htitle">FlowEndDate</span>
         <a href="javascript:void(0)" class="custom-org-sort-btn" data-order-by-key = "FlowEndDate" data-order-by-val = "desc">
            <span class="sort-btn btn-asc">▲</span>
        </a>
        <a href="javascript:void(0)" class="custom-org-sort-btn" data-order-by-key = "FlowEndDate" data-order-by-val = "asc">
            <span class="sort-btn btn-desc ">▼</span>
        </a>
        <div class="input-group">
            <input type="text" placeholder="Search FlowEndDate">
            <span class="input-group-addon org-custom-input-group-class">
                <i class="fa fa-search"></i>
            </span>
        </div>
       
    </th>    
    <th rowspan="1" colspan="1" style="width: 141px;" aria-label="Contract End Date">
        <span class="htitle">Contract End Date</span>
         <a href="javascript:void(0)" class="custom-org-sort-btn" data-order-by-key = "ContractEndDate" data-order-by-val = "desc">
            <span class="sort-btn btn-asc">▲</span>
        </a>
        <a href="javascript:void(0)" class="custom-org-sort-btn" data-order-by-key = "ContractEndDate" data-order-by-val = "asc">
            <span class="sort-btn btn-desc ">▼</span>
        </a>
        <div class="input-group">
            <input type="text" placeholder="Search ContractEndDate">
            <span class="input-group-addon org-custom-input-group-class">
                <i class="fa fa-search"></i>
            </span>
        </div>
       
    </th>                
    <th rowspan="1" colspan="1" style="width: 141px;" aria-label="Utility ID">
        <span class="htitle">Utility ID</span>
         <a href="javascript:void(0)" class="custom-org-sort-btn" data-order-by-key = "UtilityID" data-order-by-val = "desc">
            <span class="sort-btn btn-asc">▲</span>
        </a>
        <a href="javascript:void(0)" class="custom-org-sort-btn" data-order-by-key = "UtilityID" data-order-by-val = "asc">
            <span class="sort-btn btn-desc ">▼</span>
        </a>
        <div class="input-group">
            <input type="text" placeholder="Search UtilityID">
            <span class="input-group-addon org-custom-input-group-class">
                <i class="fa fa-search"></i>
            </span>
        </div>
       
    </th> 

    <th rowspan="1" colspan="1" style="width: 141px;" aria-label="UtilityShortName">
        <span class="htitle">UtilityShortName</span>
         <a href="javascript:void(0)" class="custom-org-sort-btn" data-order-by-key = "UtilityShortName" data-order-by-val = "desc">
            <span class="sort-btn btn-asc">▲</span>
        </a>
        <a href="javascript:void(0)" class="custom-org-sort-btn" data-order-by-key = "UtilityShortName" data-order-by-val = "asc">
            <span class="sort-btn btn-desc ">▼</span>
        </a>
        <div class="input-group">
            <input type="text" placeholder="Search UtilityShortName">
            <span class="input-group-addon org-custom-input-group-class">
                <i class="fa fa-search"></i>
            </span>
        </div>
       
    </th> 
    <th rowspan="1" colspan="1" style="width: 141px;" aria-label="PreEnrollmentReject Reasons">
        <span class="htitle">PreEnrollmentReject Reasons</span>
          <a href="javascript:void(0)" class="custom-org-sort-btn" data-order-by-key = "PreEnrollRejectReason" data-order-by-val = "desc">
            <span class="sort-btn btn-asc">▲</span>
        </a>
        <a href="javascript:void(0)" class="custom-org-sort-btn" data-order-by-key = "PreEnrollRejectReason" data-order-by-val = "asc">
            <span class="sort-btn btn-desc ">▼</span>
        </a>
        <div class="input-group">
            <input type="text" placeholder="Search PreEnrollRejectReason">
            <span class="input-group-addon org-custom-input-group-class">
                <i class="fa fa-search"></i>
            </span>
        </div>
      
    </th>
    <th rowspan="1" colspan="1" style="width: 141px;" aria-label="DateFlowTransReceived">
        <span class="htitle">DateFlowTransReceived</span>
          <a href="javascript:void(0)" class="custom-org-sort-btn" data-order-by-key = "DateFlowTransReceived" data-order-by-val = "desc">
            <span class="sort-btn btn-asc">▲</span>
        </a>
        <a href="javascript:void(0)" class="custom-org-sort-btn" data-order-by-key = "DateFlowTransReceived" data-order-by-val = "asc">
            <span class="sort-btn btn-desc ">▼</span>
        </a>
        <div class="input-group">
            <input type="text" placeholder="Search DateFlowTransReceived">
            <span class="input-group-addon org-custom-input-group-class">
                <i class="fa fa-search"></i>
            </span>
        </div>
      
    </th>
    <th rowspan="1" colspan="1" style="width: 141px;" aria-label="FlowStartDate">
        <span class="htitle">FlowStartDate</span>
         <a href="javascript:void(0)" class="custom-org-sort-btn" data-order-by-key = "FlowStartDate" data-order-by-val = "desc">
            <span class="sort-btn btn-asc">▲</span>
        </a>
        <a href="javascript:void(0)" class="custom-org-sort-btn" data-order-by-key = "FlowStartDate" data-order-by-val = "asc">
            <span class="sort-btn btn-desc ">▼</span>
        </a>
        <div class="input-group">
            <input type="text" placeholder="Search FlowStartDate">
            <span class="input-group-addon org-custom-input-group-class">
                <i class="fa fa-search"></i>
            </span>
        </div>
       
    </th>
    <th rowspan="1" colspan="1" style="width: 141px;" aria-label="UtilityDropReason">
        <span class="htitle">UtilityDropReason</span>
        <a href="javascript:void(0)" class="custom-org-sort-btn" data-order-by-key = "DropReason" data-order-by-val = "desc">
            <span class="sort-btn btn-asc">▲</span>
        </a>
        <a href="javascript:void(0)" class="custom-org-sort-btn" data-order-by-key = "DropReason" data-order-by-val = "asc">
            <span class="sort-btn btn-desc ">▼</span>
        </a>
        <div class="input-group">
            <input type="text" placeholder="Search DropReason">
            <span class="input-group-addon org-custom-input-group-class">
                <i class="fa fa-search"></i>
            </span>
        </div>
        
    </th>
    <th rowspan="1" colspan="1" style="width: 141px;" aria-label="RejectionDateReceived">
        <span class="htitle">RejectionDateReceived</span>
          <a href="javascript:void(0)" class="custom-org-sort-btn" data-order-by-key = "UtilityRejectionDate" data-order-by-val = "desc">
            <span class="sort-btn btn-asc">▲</span>
        </a>
        <a href="javascript:void(0)" class="custom-org-sort-btn" data-order-by-key = "UtilityRejectionDate" data-order-by-val = "asc">
            <span class="sort-btn btn-desc ">▼</span>
        </a>
        <div class="input-group">
            <input type="text" placeholder="Search UtilityRejectionDate">
            <span class="input-group-addon org-custom-input-group-class">
                <i class="fa fa-search"></i>
            </span>
        </div>
      
    </th>
    <th rowspan="1" colspan="1" style="width: 141px;" aria-label="UtilityRejectReason">
        <span class="htitle">UtilityRejectReason</span>
        <a href="javascript:void(0)" class="custom-org-sort-btn" data-order-by-key = "UtilityRejectReason" data-order-by-val = "desc">
            <span class="sort-btn btn-asc">▲</span>
        </a>
        <a href="javascript:void(0)" class="custom-org-sort-btn" data-order-by-key = "UtilityRejectReason" data-order-by-val = "asc">
            <span class="sort-btn btn-desc ">▼</span>
        </a>
        <div class="input-group">
            <input type="text" placeholder="Search UtilityRejectReason">
            <span class="input-group-addon org-custom-input-group-class">
                <i class="fa fa-search"></i>
            </span>
        </div>
        
    </th>

</tr>