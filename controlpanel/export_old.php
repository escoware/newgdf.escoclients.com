<?php

	include("../include/config.php");

	if($_SESSION['life_user_id'] == ''){
		header("location:index.php");
		exit();
	}
	/**
	 *
	 * This file is used to export the data selected by user
	 * File format will be .csv, excel(.xlsx,.xls), .pdf 
	 *
	 * @param File Type $type
	 * @param Query $image
	 * @param page Name $page
	 *
	 * @return Redirect 
	 * @author Satish Deshmukh
	 *
	 */

	// Check if method is post, used to visit this page
	if( $_SERVER['REQUEST_METHOD'] == 'POST' ){

		// Valid pages array
		$valid_pages = ['accounts','paynopay','commission'];

		// Valid extensions array
		$valid_ext = ['pdf','excel','csv'];

		// Check if Referer is set
		if( isset($_SERVER['HTTP_REFERER']) && $_SERVER['HTTP_REFERER'] ){

			// Generete URI for checking valid page
			$valid_uri_str  = $_SERVER['REQUEST_SCHEME'] ? $_SERVER['REQUEST_SCHEME'].'://' : 'http://';
			$valid_uri_str .= $_SERVER['SERVER_NAME'] .str_ireplace('export.php', '{$$}', $_SERVER['REQUEST_URI']);

			// Check if all param are present and valid
			if( (isset($_REQUEST['type']) && $_REQUEST['type']) && (isset($_REQUEST['image']) && $_REQUEST['image']) && (isset($_REQUEST['page']) && $_REQUEST['page']) ){

				if( in_array($_REQUEST['type'], $valid_ext) === false ){
					sendBack($_SERVER['HTTP_REFERER']);
				}

				if( in_array($_REQUEST['page'], $valid_pages) === false ){
					sendBack($_SERVER['HTTP_REFERER']);
				}

			    // decode query
				$query = base64_decode($_REQUEST['image']);

				switch ($_REQUEST['type']) {
					case 'pdf':
							downloadPdf();
						break;
					
					case 'excel':
							downloadExcel();
						break;

					case 'csv':
							downloadCsv($query, $_REQUEST['type'], $_REQUEST['page']);
						break;

					default:
						header('Location: '.$_SERVER['HTTP_REFERER']);
						break;
				}

			}
		}
		else{
			sendBack();	
		}
	}
	else{
		sendBack();
	}

	function getAccountsCsv($result){

		// create line with field names
		$csv_file = '';

		$csv_file .= 'Account ID,Contract ID,Enrollment Date,Broker ID,Customer Type,First Name,Last Name,Company Name,Service Address,Service Address2,City,State,Zip Code,Phone,Email,Product Code,Rate,Term';

		// newline (seems to work both on Linux & Windows servers)
		$csv_file.= '
';

		while ($res = mssql_fetch_assoc($result)) {
			
			// create line with field values

			$csv_file .= '"'.$res['AccountID'] .'",'; 
            $csv_file .= '"'.$res['ContractID'] .'",';
          	$csv_file .= '"'.date('M d, Y h:i:s A',strtotime($res['EnrollmentDate'])) .'",';
          	$csv_file .= '"'.$res['BrokerID'] .'",';
		    $csv_file .= '"'.$res['CustomerTypeName'] .'",';
		    $csv_file .= '"'.$res['FirstName'] .'",';
			$csv_file .= '"'.$res['LastName'] .'",';
			$csv_file .= '"'.$res['CompanyName'] .'",';
			$csv_file .= '"'.$res['Address1'] .'",';
			$csv_file .= '"'.$res['Address2'] .'",';
			$csv_file .= '"'.$res['City'] .'",';
			$csv_file .= '"'.$res['StateCode'] .'",';
			$csv_file .= '"'.$res['PostalCode'] .'",';

			if( preg_match( '/^\+\d(\d{3})(\d{3})(\d{4})$/', str_ireplace('-', '', trim($res['HomePhone'])),  $matches ) ){
			    $csv_file .= '"'.$matches[1] . '-' .$matches[2] . '-' . $matches[3] .'",';
			}
			elseif( preg_match( '/^(\d{3})(\d{3})(\d{4})$/', str_ireplace('-', '', trim($res['HomePhone'])),  $matches ) ){
				$csv_file .= '"'.$matches[1] . '-' .$matches[2] . '-' . $matches[3] .'",';
			}
			else{
				$csv_file .= '"'.$res['HomePhone'] .'",';
			}

			$csv_file .= '"'.$res['Email'] .'",';
			$csv_file .= '"'.$res['ProductCode'] .'",';
			$csv_file .= '"'.number_format($res['RateValue'],3) .'",';
			$csv_file .= '"'.$res['Term'] .'"';
			
			$csv_file .= '
';
		}

		header('Content-Description: File Transfer');
		header('Content-type: text/x-csv');
		header('Content-Disposition: attachment; filename="'.$_REQUEST["page"].'.csv"');
	    header('Content-Length: '.strlen($csv_file));
	    header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
	    header('Expires: 0');
	    header('Pragma: public');
	    echo $csv_file;
	    exit;

	}

	function getPayNoPayCsv($result){
		// create line with field names
		$csv_file = '';

		$csv_file .= 'CommissionRunDetailID,CommissionRunID,CommissionRunDate,ContractID,ContractNumber,LeadID,EntryDate,PaidBrokerID,PaidBrokerCompanyName,CustomerTypeName,CustomerFirstName,CustomerLastName,CustomerCompanyName,SerivceAddress1,ServiceAddress2,SerivceCity,ServiceStateCode,ServiceZipCode,ProductName,ContractRateValue,ContractTerm,PlanDescription,CommodityName,UtilityAccountNumber,MeterNumber,BillingServiceStatus,BillingServiceDescription,ContractStatus,BillingCycleStartDate,BillingCycleEndDate,ContractEndDate,AccountConfirmedStartDate,UtilityID,UtilityName,UtilityShortName,PreEnrollRejectReason,PaidSigningAgentCode,TPVLink,Servicestatusbillingvalue,CommissionRate,RateUOM,forecastusage,CommissionAmount,PaymentTypeName,DateFlowTransReceived,FlowStartDate,FlowEndDate,DropReason,UtilityRejectionDate,UtilityRejectReason,CallCenterDropCode,ForecastAnnualUsage,ForecastAnnualUsageCoverage,ForecastAnnualUsageUOM,DwellingType,TrueUpIndicator,DualFuelIndicator';

		// newline (seems to work both on Linux & Windows servers)
		$csv_file.= '
';

		while ($res = mssql_fetch_assoc($result)) {
			
			// create line with field values
			$csv_file .= '"'.$res['CommissionRunDetailID'] .'",';
			$csv_file .= '"'.$res['CommissionRunID'] .'",';
			$csv_file .= '"'.$res['CommissionRunDate'] .'",';
			$csv_file .= '"'.$res['ContractID'] .'",';
			$csv_file .= '"'.$res['ContractNumber'] .'",';
			$csv_file .= '"'.$res['LeadID'] .'",';

			if( $res['EntryDate'] ) {
				$EntryDate = date('M d, Y',strtotime($res['EntryDate'])); 
			} 
			else{
				$EntryDate = $res['EntryDate'];
			}

			$csv_file .= '"'.$EntryDate .'",';
			$csv_file .= '"'.$res['PaidBrokerID'] .'",';
			$csv_file .= '"'.$res['PaidBrokerCompanyName'] .'",';
			$csv_file .= '"'.$res['CustomerTypeName'] .'",';
			$csv_file .= '"'.$res['CustomerFirstName'] .'",';
			$csv_file .= '"'.$res['CustomerLastName'] .'",';
			$csv_file .= '"'.$res['CustomerCompanyName'] .'",';
			$csv_file .= '"'.$res['SerivceAddress1'] .'",';
			$csv_file .= '"'.$res['ServiceAddress2'] .'",';
			$csv_file .= '"'.$res['SerivceCity'] .'",';
			$csv_file .= '"'.$res['ServiceStateCode'] .'",';
			$csv_file .= '"'.$res['ServiceZipCode'] .'",';
			$csv_file .= '"'.$res['ProductName'] .'",';
			$csv_file .= '"'.number_format($res['ContractRateValue'],3) .'",';
			$csv_file .= '"'.$res['ContractTerm'] .'",';
			$csv_file .= '"'.$res['PlanDescription'] .'",';
			$csv_file .= '"'.$res['CommodityName'] .'",';
			$csv_file .= '"'.$res['UtilityAccountNumber'] .'",';
			$csv_file .= '"'.$res['MeterNumber'] .'",';
			$csv_file .= '"'.$res['BillingServiceStatus'] .'",';
			$csv_file .= '"'.$res['BillingServiceDescription'] .'",';
			$csv_file .= '"'.$res['ContractStatus'] .'",';
			$csv_file .= '"'.$res['BillingCycleStartDate'] .'",';

			if( $res['ContractEndDate'] ) {
				$ContractEndDate=date('M d, Y',strtotime($res['ContractEndDate']));
			} 
			else{
				$ContractEndDate=$res['ContractEndDate'];
			}
		
			$csv_file .= '"'.$ContractEndDate .'",';
			if($res['AccountConfirmedStartDate']!=''){
				$AccountConfirmedStartDate = date('m/d/Y',strtotime($res['AccountConfirmedStartDate']));
			}
			else{
				$AccountConfirmedStartDate = $res['AccountConfirmedStartDate'];
			}

			$csv_file .= '"'.$ContractEndDate .'",';
			$csv_file .= '"'.$AccountConfirmedStartDate .'",';
			$csv_file .= '"'.$res['UtilityID'] .'",';
			$csv_file .= '"'.$res['UtilityName'] .'",';
			$csv_file .= '"'.$res['UtilityShortName'] .'",';
			$csv_file .= '"'.$res['PreEnrollRejectReason'] .'",';
			$csv_file .= '"'.$res['PaidSigningAgentCode'] .'",';
			$csv_file .= '"'.$res['TPVLink'] .'",';
			$csv_file .= '"'.$res['servicestatusbillingvalue'] .'",';
			$csv_file .= '"'.money_format('%(#10.2n', $res['CommissionRate']) .'",';
			$csv_file .= '"'.$res['RateUOM'] .'",';
			$csv_file .= '"'.$res['forecastusage'] .'",';
			$csv_file .= '"'.money_format('%(#10.2n', $res['CommissionAmount']) .'",';
			$csv_file .= '"'.$res['PaymentTypeName'] .'",';

			if( $res['DateFlowTransReceived'] ) {
				$DateFlowTransReceived=date('M d, Y',strtotime($res['DateFlowTransReceived']));
			} 
			else{
				$DateFlowTransReceived=$res['DateFlowTransReceived'];
			} 
		
			$csv_file .= '"'.$DateFlowTransReceived .'",';
	
			if( $res['FlowStartDate'] ) {
				$FlowStartDate=date('M d, Y',strtotime($res['FlowStartDate']));
			} 
			else {
				$FlowStartDate=$res['FlowStartDate'];
			} 
	
			$csv_file .= '"'.$FlowStartDate .'",';

			if($res['FlowEndDate']) {
				$FlowEndDate=date('M d, Y',strtotime($res['FlowEndDate'])); 
			} 
			else{
				$FlowEndDate=$res['FlowEndDate'];
			}
		 	if($res['DropDateReceived']) {
				$DropDateReceived=date('M d, Y',strtotime($res['DropDateReceived'])); 
			} 
			else {
				$DropDateReceived=$res['DropDateReceived'];
			}
		 	if($res['UtilityRejectionDate']) {
				$UtilityRejectionDate=date('M d, Y',strtotime($res['UtilityRejectionDate']));
			} 
			else {
				$UtilityRejectionDate=$res['UtilityRejectionDate'];
			}
	
			$csv_file .= '"'.$FlowEndDate .'",';
			$csv_file .= '"'.$DropDateReceived .'",';
			$csv_file .= '"'.$res['DropReason'] .'",';
			$csv_file .= '"'.$UtilityRejectionDate .'",';
			$csv_file .= '"'.$res['UtilityRejectReason'] .'",';
			$csv_file .= '"'.$res['CallCenterDropCode'] .'",';
			$csv_file .= '"'.$res['ForecastAnnualUsage'] .'",';
			$csv_file .= '"'.$res['ForecastAnnualUsageCoverage'] .'",';
			$csv_file .= '"'.$res['ForecastAnnualUsageUOM'] .'",';
			$csv_file .= '"'.$res['DwellingType'] .'",';
			$csv_file .= '"'.$res['TrueUpIndicator'] .'",';
			$csv_file .= '"'.$res['DualFuelIndicator'] .'",';

			$csv_file .= '
';
		}

		header('Content-Description: File Transfer');
		header('Content-type: text/x-csv');
		header('Content-Disposition: attachment; filename="'.$_REQUEST["page"].'.csv"');
	    header('Content-Length: '.strlen($csv_file));
	    header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
	    header('Expires: 0');
	    header('Pragma: public');
	    echo $csv_file;
	    exit;
	}

	function getCommissionCsv($result){
		// create line with field names
		$csv_file = '';

		$csv_file .= 'Account ID,Contract ID,Enrollment Date,Broker ID,Customer Type,First Name,Last Name,Company Name,Service Address,Service Address2,City,State,Zip Code,Phone,Email,Product Code,Rate,Term';

		// newline (seems to work both on Linux & Windows servers)
		$csv_file.= '
';

		while ($res = mssql_fetch_assoc($result)) {
			
			// create line with field values

			$csv_file .= '"'.$res['AccountID'] .'",'; 
            $csv_file .= '"'.$res['ContractID'] .'",';
          	$csv_file .= '"'.date('M d, Y h:i:s A',strtotime($res['EnrollmentDate'])) .'",';
          	$csv_file .= '"'.$res['BrokerID'] .'",';
		    $csv_file .= '"'.$res['CustomerTypeName'] .'",';
		    $csv_file .= '"'.$res['FirstName'] .'",';
			$csv_file .= '"'.$res['LastName'] .'",';
			$csv_file .= '"'.$res['CompanyName'] .'",';
			$csv_file .= '"'.$res['Address1'] .'",';
			$csv_file .= '"'.$res['Address2'] .'",';
			$csv_file .= '"'.$res['City'] .'",';
			$csv_file .= '"'.$res['StateCode'] .'",';
			$csv_file .= '"'.$res['PostalCode'] .'",';

			if( preg_match( '/^\+\d(\d{3})(\d{3})(\d{4})$/', str_ireplace('-', '', trim($res['HomePhone'])),  $matches ) ){
			    $csv_file .= '"'.$matches[1] . '-' .$matches[2] . '-' . $matches[3] .'",';
			}
			elseif( preg_match( '/^(\d{3})(\d{3})(\d{4})$/', str_ireplace('-', '', trim($res['HomePhone'])),  $matches ) ){
				$csv_file .= '"'.$matches[1] . '-' .$matches[2] . '-' . $matches[3] .'",';
			}
			else{
				$csv_file .= '"'.$res['HomePhone'] .'",';
			}

			$csv_file .= '"'.$res['Email'] .'",';
			$csv_file .= '"'.$res['ProductCode'] .'",';
			$csv_file .= '"'.number_format($res['RateValue'],3) .'",';
			$csv_file .= '"'.$res['Term'] .'"';
			
			$csv_file .= '
';
		}

		header('Content-Description: File Transfer');
		header('Content-type: text/x-csv');
		header('Content-Disposition: attachment; filename="'.$_REQUEST["page"].'.csv"');
	    header('Content-Length: '.strlen($csv_file));
	    header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
	    header('Expires: 0');
	    header('Pragma: public');
	    echo $csv_file;
	    exit;
	}

	function downloadPdf(){

	}

	function downloadCsv($q, $t, $p){
		checkUserFolder();die('asd');
		if( checkUserFolder() ){

		}

		$fp = fopen('file.csv', 'w');

	}

	function checkUserFolder(){

		$user_folder = $_SERVER['CONTEXT_DOCUMENT_ROOT'].'/staging/lifeenergy/controlpanel/exports/145';

		if( !is_dir($user_folder) ){

			echo "<pre>"; var_dump(mkdir($user_folder));
			die;
		}

		return true;
	}

	function downloadCsv_old($q, $t, $p){

		$result = mssql_query($q);

		$csv_export = '';

		for($i = 0; $i < mssql_num_fields($result); $i++) {
			$csv_export.= mssql_fetch_field($result, $i)->name.',';
		}

		$csv_export.= '
';

		while($row = mssql_fetch_assoc($result)) {

		  	for($i = 0; $i < mssql_num_fields($result); $i++) {
		    	$csv_export.= '"'.$row[mssql_fetch_field($result, $i)->name].'",';
		  	}	
		  	$csv_export.= '
';	
		}

		if( $p == 'accounts' ){
			getAccountsCsv($result);
		}
		elseif( $p == 'paynopay' ){
			getPayNoPayCsv($result);
		}
		elseif( $p == 'commission' ){
			getCommissionCsv($result);
		}
		else{
			sendBack();
		}

	}

	function downloadExcel(){

	}

	function sendBack($param = ''){

		if($param != '' ){

			header('Location: '.$param);
		}
		else{
			header('Location: index.php');
		}

	}
?>