<div class="nav_menu">
  <nav class="" role="navigation">
    <nav class="navbar">
      <div class="container-fluid">
       <div class="navbar nav_title" style="border: 0;">
        <a href="dashboard.php" class="site_title"> <span><?php echo SITE_TITLE; ?></span></a>
      </div>
      <ul class="nav navbar-nav navbar-right">
        <li class="">
          <a href="javascript:void(0)" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false" id="admin_clr">
            <img src="images/user.png"  alt=""><?php  echo $_SESSION['sess_admin_username'];?>
            <span class="usr_name">
              <?php echo $_SESSION['life_user_fname'].' '. $_SESSION['life_user_lname'] .' ('.$_SESSION["life_user_name"].')'; ?>
            </span>
           
          </a>
          <ul class="dropdown-menu dropdown-usermenu pull-right exit_btn">               


            <li><a href="logout.php" class="lg_out"> Logout</a>
            </li>
          </ul>
        </li>

      </ul>


    </div>
  </nav>
  <div class="navbar-header">
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>                        
    </button>

  </div>
  <div class="collapse navbar-collapse" id="myNavbar">
   <ul class="nav navbar-nav" id="top_navigation">

    <li class="dropdown"><a  href="dashboard.php" <?php if(basename($_SERVER['PHP_SELF']) =="dashboard.php") { ?>class="active"<?php } ?>>Dashboard </a>

    </li>
    <li class="dropdown"><a href="accounts.php">Accounts </a>

    </li>
    <li class="dropdown"><a href="commission.php">Commissions</a>

    </li>


    <li class="dropdown"><a href="paynopay.php" <?php if(basename($_SERVER['PHP_SELF']) =="paynopay.php") { ?>class="active"<?php } ?>>My pay/Nopay Accounts</a>

    </li>

    <li class="dropdown">
        <a href="exports.php" <?php ( basename($_SERVER['PHP_SELF']) == 'exports.php' ) ? 'class="active"' : '' ?>>
            Exports
        </a>
    </li>
  </ul>
</div>
</nav>

</div>
