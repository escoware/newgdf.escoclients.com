<?php
	require_once 'include/accounts/accounts_query.php';
	
	if( $total_data_table_rows > 0 ){

		echo '<tbody>';

		$i=0;

		while( $result = mssql_fetch_assoc($data_resource) ){

			$i++;

			if( $i % 2 == 0 ){
				$bgcolor = "#fff";
			}
			else{
				$bgcolor = "";
			}
			?>

			<tr bgcolor="<?php echo $bgcolor;?>">

				<td class="AccountID_td"><?php echo $result['AccountID']; ?></td>
				<td class="ContractID_td"><?php echo $result['ContractID'];?></td>
				<td class="EnrollmentDate_td"><?php echo date('m/d/Y',strtotime($result['EnrollmentDate']))?></td>
				<td class="StatusName_td"><?php echo $result['StatusName']; //echo $result['BrokerID'];?></td>
				<td class="SigningAgentCode_td"><?php echo $result['SigningAgentCode']; ?></td>
				<td class="CustomerTypeName_td"><?php echo $result['CustomerTypeName'];?></td>
				<td class="FirstName_td"><?php echo $result['FirstName'];?></td>
				<td class="LastName_td"><?php echo $result['LastName'];?></td>
				<td class="CompanyName_td"><?php echo $result['CompanyName'];?></td>
				<td class="Address1_td"><?php echo $result['Address1'];?></td>
				<td class="Address2_td"><?php echo $result['Address2'];?></td>
				<td class="City_td"><?php echo $result['City'];?></td>
				<td class="StateCode_td"><?php echo $result['StateCode'];?></td>
				<td class="PostalCode_td"><?php echo $result['PostalCode'];?></td>
				<td class="HomePhone_td">
					<?php 
					if( preg_match( '/^\+\d(\d{3})(\d{3})(\d{4})$/', str_ireplace('-', '', trim($result['HomePhone'])),  $matches ) ){
						echo $matches[1] . '-' .$matches[2] . '-' . $matches[3];
					}
					elseif( preg_match( '/^(\d{3})(\d{3})(\d{4})$/', str_ireplace('-', '', trim($result['HomePhone'])),  $matches ) ){
						echo $matches[1] . '-' .$matches[2] . '-' . $matches[3];
					}
					else{
						echo $result['HomePhone'];
					}
					?>
				</td>
				<td class="Email_td"><?php echo $result['Email'];?></td>
				<td class="ProductCode_td"><?php echo $result['ProductCode'];?></td>
				<td class="RateValue_td"><?php echo number_format($result['RateValue'],3);?></td>
				<td class="Term_td"><?php echo $result['Term'];?></td>
				<td class="MarkupInfo_td"><?php echo $result['MarkupInfo']; ?></td>
			</tr>
			<?php
		}

		echo '</tbody>';

	}
	else{
		echo '<tbody>
			<tr>
				<td colspan="8">
					<font face="Arial, Helvetica, sans-serif" color="#FF0000" size="+1">No data in column</font>
				</td>
			</tr>
		</tbody>';
	}
?>