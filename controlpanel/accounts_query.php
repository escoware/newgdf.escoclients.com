<?php  
	
	if( $_REQUEST['order-by'] ){

		$order_key = explode('_', $_REQUEST['order-by']);
		$order_col = $order_key[0];
		$order_val = $order_key[1];
	}
	else{
		$order_col = 'AccountID';
		$order_val = 'asc';
	}

	$page = $_REQUEST['page'] ? (int)$_REQUEST['page'] : 1;

	$minlimit = ( $page > 1 ) ? ( ( ( $page - 1 ) * $num_of_rows_shown_in_table ) + 1 ) : 1;

	$maxlimit = $minlimit + $num_of_rows_shown_in_table;

	$query_for_data = "SELECT * FROM dbo.BP2_DistinctAccountDetails_1 WHERE BrokerID = {$_SESSION['life_user_id']}";

	if( $_REQUEST['search'] ){
		$search_key = explode('_', $_REQUEST['search']);
		$search_col = $search_key[0];
		$search_val = $search_key[1];

		$query_for_data .= " AND {$search_col} LIKE '%{$search_val}%'";
	}

	$query_for_data .= " ORDER BY {$order_col} {$order_val}";

	$rawsql_for_js = $query_for_data;


	$query_for_data .= " OFFSET {$minlimit} ROWS FETCH NEXT {$num_of_rows_shown_in_table} ROWS ONLY";

	$data_resource = mssql_query($query_for_data);

	// use raw query for js to find the total number of records
	$total_records_resource = mssql_query($rawsql_for_js);

	$total_data_table_rows = mssql_num_rows($data_resource);
	$total_rows = mssql_num_rows($total_records_resource);
?>