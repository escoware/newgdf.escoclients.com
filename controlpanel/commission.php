<?php
	include("../include/config.php");

	if($_SESSION['life_user_id']==''){
		header("location:index.php");
		exit();
	}
	setlocale(LC_MONETARY, 'en_US');
	$num_of_rows_shown_in_table = $_REQUEST['rows'] ? $_REQUEST['rows'] : 100;
	require_once 'include/commissions/query.php';
?>

<!DOCTYPE html>
<html lang="en">

  	<head>
    	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

	    <!-- Meta, title, CSS, favicons, etc. -->
	    <meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">

	    <title><?php echo SITE_TITLE; ?></title>

	    <!-- Bootstrap -->
	    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

	    <!-- Font Awesome -->
	    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">

	    <!-- iCheck -->
	    <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet"> 
    	<link href="css/custom.css" rel="stylesheet">
    	<link href="css/buttons.dataTables.min.css" rel="stylesheet" type="text/css">

    	<style type="text/css">
    		.custom-input-group-class{
    			background-color: #fff;
    			cursor: pointer; 
    		}
    		input[type="text"]{
    			color: #000 !important;
    			padding: 5px !important;
    		}
    		thead{
    			background: #fff;
    		}
    	</style>
  	</head>

  	<body class="nav-md">
  		<div class="container body">
  			<div class="main_container">
  				<div class="col-md-3 left_col">
		          	<?php 
		          		// Include Left Menu
		          		include("left-menu.php");
		          	?>
		        </div>

		        <!-- top navigation -->
		        <div class="top_nav">
		          	<?php 
		          		// Include Top Menu
		          		include("top-menu.php"); 
		          	?>
		        </div>

		        <!-- page content -->
		        <div class="right_col" role="main">

		        	<!--  First box div  -->
		        	<div class="col-md-12 col-lg-12 cnt_area">
		        		<div class="x_panel">
		              		<div class="x_title">
		                		<h4 class="m-b-20">
		                			Commissions
		                		</h4>
		                	</div>

		                	<div class="x_content">
		                		<table id="datatable-responsive" class="table dt-responsive nowrap pay_on comsn_tbl" cellspacing="0" width="100%">
		                			<thead>
		                				<tr>
		                					<th class="tbl_hdng">
		                						Commission Cycle
		                					</th>

		                					<th class="tbl_hdng">
		                						Commission Total
		                					</th>

		                					<th class="tbl_hdng">
		                						Run Date
		                					</th>
		                				</tr>
		                			</thead>

		                			<?php

		                				$commission_raw_query = "SELECT CommissionRunID, CAST(ROUND(CommissionRunBrokers.TotalCommission, 2) AS NUMERIC(35, 2)) AS TotalCommission, CONVERT(VARCHAR(10), RunDate, 111) AS RunDate FROM CommissionRunBrokers INNER JOIN CommissionRuns ON CommissionRunBrokers.CommissionRunID = CommissionRuns.ID WHERE BrokerId = '".$_SESSION['life_user_id']."' AND CommissionRuns.Active = 1 AND CommissionRunBrokers.Active = 1 order by CommissionRunID desc";

		                				$query = mssql_query($commission_raw_query);

		                				echo '<tbody>';
		                				
		                				while($result=mssql_fetch_assoc($query)){
		                					
		                					echo '<tr>
				                					<th>
				                						<a href="javascript:void(0)">
				                							'.$result['CommissionRunID'].'
				                						</a>	
				                					</th>
				                					<th class="com_total">
				                						'.money_format('%(.2n',$result['TotalCommission']).'
				                					</th>
				                					<th class="com_total">
				                						'.date('m/d/Y',strtotime($result['RunDate'])).'
				                					</th>
				                				</tr>';
		                				}

		                				echo '</tbody>';
		                			?>
		                		</table>
		                	</div>
		                </div>
		        	</div>

		        	<div class="col-md-12 col-lg-12">
		        		<div class="x_panel">
		        			
		        			<div class="x_title">
		        				<h4>
		        					Personal Commissions
		        				</h4>
		        			</div>

		        			<div class="x_content">
		        				<div class="col-md-12 col-lg-12 dt-buttons">
				              		<a class="dt-button buttons-copy buttons-html5 hide" tabindex="0" aria-controls="example" id="copy_commission" href="javascript:void(0)">
				                    	<span>Copy</span>
				                    </a>
				                    <a class="dt-button buttons-csv buttons-html5" tabindex="0" aria-controls="example" id="csv_commission" data-type="csv" href="javascript:void(0)">
				                    	<span>CSV</span>
				                    </a>
				                    <a class="dt-button buttons-excel buttons-html5" tabindex="0" aria-controls="example" id="excel_commission" data-type="xlsx" href="javascript:void(0)">
				                    	<span>Excel</span>
				                    </a>
				                    <a class="dt-button buttons-pdf buttons-html5 hide" tabindex="0" aria-controls="example" id="pdf_commission" data-type="pdf" href="javascript:void(0)">
				                    	<span>PDF</span>
				                    </a>
				                    <a class="dt-button buttons-print" tabindex="0" aria-controls="example" id="print_commission" data-type="print" data-source="datatable-responsives" href="javascript:void(0)">
				                    	<span>Print</span>
				                    </a>
				                    <form target="_blank" id="export-form" class="hide"></form>
								</div>

								<div class="col-md-2 col-sm-6 col-xs-12">
	  								Show&nbsp;&nbsp;
	  								<select class="form-control" id="num_of_rows_shown" style="width: 80px; display: inline;">
										<option value="100" <?php echo ($_REQUEST['rows'] == 100) ? 'selected="selected"' : ''; ?>>100</option>
	  									<option value="200" <?php echo ($_REQUEST['rows'] == 200) ? 'selected="selected"' : ''; ?>>200</option>
	  									<option value="500" <?php echo ($_REQUEST['rows'] == 500) ? 'selected="selected"' : ''; ?>>500</option>
	  									<option value="1000" <?php echo ($_REQUEST['rows'] == 1000) ? 'selected="selected"' : ''; ?>>1000</option>
	  								</select>
	  								&nbsp;&nbsp;entries
	  							</div>

		        				<?php
		        					require_once 'include/exit_search_sort_view.php';
		        				?>

		        				<table id="datatable-responsives" class="table table-hover table-striped nowrap" cellspacing="0" width="100%" id="commision_tbl">
		        					<thead>
		        						<?php 
		        							include 'include/commissions/commission_thead.php';
		        						?>
		        					</thead>

		        					<?php
										require_once 'include/commissions/data.php';
									?>

		        					<tbody>
		        						<?php
		        							require_once 'include/commissions/data.php';
		        						?>
		        					</tbody>
		        				</table>

		        				<!-- pagination -->
								<?php
									require_once 'include/commissions/pagination.php';
								?>
		        			</div>

		        		</div>
		        	</div>


		        	<!-- Organizational Commissions Table Starts -->

		        	<?php
                        // include 'org_commission_tbl.php'
		        	?>

		        	<!-- Organizational Commissions Table Ends -->	
  				</div>
  				<!-- /page content -->

		        <!-- footer content -->
		        <footer>
		          	<?php
		          		include("footer.php");
		          	?>
		        </footer>
		        <!-- /footer content -->

  			</div>
  		</div>  	

  		<!-- jQuery -->
	    <script src="../vendors/jquery/dist/jquery.min.js"></script>

	    <!-- Bootstrap -->
	    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>

	    <!-- Custom Theme Scripts -->
    	<script src="js/custom.js"></script>
    	<script type="text/javascript">
    		var vfg = "<?php echo base64_encode($rawsql_for_js); ?>";
    	</script>

    	<script type="text/javascript" src="include/commissions/commission.js"></script>

    	<script type="text/javascript" src="js/fixedheader.js"></script>

    	<script type="text/javascript">
    		function pageTop(){
				return $(".nav_menu").height();
			}

    		$(document).ready(function(){
    			$('table#datatable-responsive').floatThead({
					top : pageTop,
					position: 'fixed'
				});
    		});

    		$(window).bind('load', function(){
    			$(document).find('.floatThead-container table thead').each(function(e,r){

    				var width = $(r).width();

    				$(document).find('#datatable-responsive tbody').each(function(t,y){
    					if( e == t ){
    						$(y).css('width',width+'px');
    					}
    				});

    			});
    		});

    	</script>
  	</body>

</html>