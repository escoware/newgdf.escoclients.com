<?php 
include("../include/config.php");
if($_SESSION['life_user_id']==''){
	header("location:index.php");
	exit();

}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<!-- Meta, title, CSS, favicons, etc. -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?php echo SITE_TITLE; ?></title>
	<!-- Bootstrap -->
	<link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
	<!-- Font Awesome -->
	<link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
	<link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,300italic'>

	<!-- Custom Theme Style -->
	<link href="css/custom.css" rel="stylesheet">
</head>
<body class="nav-md">
	<div class="container body">
		<div class="main_container">
        <?php /*?><div class="col-md-3 left_col">
          <?php include("left-menu.php"); ?>
      </div><?php */?>
      <!-- top navigation -->
      <div class="top_nav">
      	<?php include("top-menu.php"); ?>
      </div>

      <?php
      if($_REQUEST['utype']=='parent'){

      	function utf8ize($d) {
      		if (is_array($d)) {
      			foreach ($d as $k => $v) {
      				$d[$k] = utf8ize($v);
      			}
      		} else if (is_string ($d)) {
      			return utf8_encode($d);
      		}
      		return $d;
      	}
      	function callgraphpie($datatype){
      		if($datatype=='pie') {
      			$query12 = mssql_query("EXEC dbo.brm_sp_DownlineByBillingStatus ".$_SESSION['life_user_id']." ");		
      			while($resultdt=mssql_fetch_assoc($query12)){
		  	//$cdata12['BrokerID']=$result12['BrokerID'];
      				$aad[]=$_SESSION['life_user_id'];
      				$aad[]=$resultdt['BrokerID'];
      			}

      			$myarraydata=array_unique($aad);

      			$dt=	"IN(";
      			$i=1;
      			foreach ($myarraydata as $key=>$val){
      				$dt.="'".$val."'";
      				if($i <= count($myarraydata)-1 ){
      					$dt.=",";
      				}
      				$i++;
      			}
      			$dt.=" )";
      			$data=mssql_query("SELECT StateCode, COUNT(StateCode) AS 'Count'
      				FROM dbo.BP2_DistinctAccountDetails_1
      				WHERE BrokerID ".$dt."
      				GROUP BY StateCode");

      			while($result= mssql_fetch_assoc($data)){

      				$cdata['label']=$result['StateCode'];
      				$cdata['value']=$result['Count'];






      				$aa[]= $cdata; 
      			}

      			$jsondata=$aa;;


      			echo json_encode(utf8ize($jsondata));


      		}
      	}
      	function callgraparea($datatype){
      		if($datatype=='area') {

      			$data=mssql_query("SELECT StateCode, COUNT(StateCode) AS 'Count'
      				FROM dbo.BP2_DistinctAccountDetails_1
      				WHERE BrokerID = '".$_SESSION['life_user_id']."'
      				GROUP BY StateCode");

      			while($result= mssql_fetch_assoc($data)){

      				$cdata['label']=$result['StateCode'];
      				$cdata['value']=$result['Count'];






      				$aa[]= $cdata; 
      			}

      			$jsondata=$aa;;


      			echo json_encode(utf8ize($jsondata));


      		}
      	}
      	function callgraph($datatype){

      		if($datatype=='Donut'){

      			$query12 = mssql_query("EXEC dbo.brm_sp_DownlineByBillingStatus ".$_SESSION['life_user_id']." ");		
      			while($resultdt=mssql_fetch_assoc($query12)){
		  			//$cdata12['BrokerID']=$result12['BrokerID'];
      				$aad[]=$_SESSION['life_user_id'];
      				$aad[]=$resultdt['BrokerID'];
      			}

      			$myarraydata=array_unique($aad);

      			$dt=	"IN(";
      			$i=1;
      			foreach ($myarraydata as $key=>$val){
      				$dt.="'".$val."'";
      				if($i <= count($myarraydata)-1 ){
      					$dt.=",";
      				}
      				$i++;
      			}
      			$dt.=" )";

      			$data= mssql_query("SELECT UtilityName, COUNT(UtilityName) AS 'Count'
      				FROM dbo.BP2_DistinctAccountDetails_1
      				WHERE BrokerID ".$dt."
      				GROUP BY UtilityName");


      			while($result=mssql_fetch_assoc($data)){

      				$cdata['label']=$result['UtilityName'];
      				$cdata['value']=$result['Count'];

      				$aa[]= $cdata; 
      			}

      			$jsondata=$aa;

      			echo json_encode(utf8ize($jsondata));

      		} else if($datatype='column'){
      			$query12 = mssql_query("EXEC dbo.brm_sp_DownlineByBillingStatus ".$_SESSION['life_user_id']." ");		
      			while($resultdt=mssql_fetch_assoc($query12)){
		  		//$cdata12['BrokerID']=$result12['BrokerID'];
      				$aad[]=$_SESSION['life_user_id'];
      				$aad[]=$resultdt['BrokerID'];
      			}

      			$myarraydata=array_unique($aad);

      			$dt=	"IN(";
      			$i=1;
      			foreach ($myarraydata as $key=>$val){
      				$dt.="'".$val."'";
      				if($i <= count($myarraydata)-1 ){
      					$dt.=",";
      				}
      				$i++;
      			}
      			$dt.=" )";

      			$data=mssql_query("SELECT CustomerTypeName,count(StatusName) as statuscount,StatusName FROM dbo.BP2_DistinctAccountDetails_1  WHERE BrokerID ".$dt." group by CustomerTypeName,StatusName order by CustomerTypeName");


      			while($result= mssql_fetch_assoc($data)){

      				$cdata['y']=$result['StatusName'];
      				$cdata['a']=$result['statuscount'];

      				$aa[]= $cdata; 
      			}

      			$jsondata=$aa;;


      			echo json_encode(utf8ize($jsondata));

      		} else if($datatype='pie'){
      			$query12 = mssql_query("EXEC dbo.brm_sp_DownlineByBillingStatus ".$_SESSION['life_user_id']." ");		
      			while($resultdt=mssql_fetch_assoc($query12)){
		  		//$cdata12['BrokerID']=$result12['BrokerID'];
      				$aad[]=$_SESSION['life_user_id'];
      				$aad[]=$resultdt['BrokerID'];
      			}

      			$myarraydata=array_unique($aad);

      			$dt=	"IN(";
      			$i=1;
      			foreach ($myarraydata as $key=>$val){
      				$dt.="'".$val."'";
      				if($i <= count($myarraydata)-1 ){
      					$dt.=",";
      				}
      				$i++;
      			}
      			$dt.=" )";


      			$data=mssql_query("SELECT UtilityName, COUNT(UtilityName) AS 'Count'
      				FROM dbo.BP2_DistinctAccountDetails_1
      				WHERE BrokerID ".$dt."
      				GROUP BY UtilityName");

      			while($result= mssql_fetch_assoc($data)){

      				$cdata['label']=$result['UtilityName'];
      				$cdata['value']=$result['Count'];






      				$aa[]= $cdata; 
      			}

      			$jsondata=$aa;;


      			echo json_encode(utf8ize($jsondata));



      		}
      	}



      	function callgraphresidential(){


      		$query12 = mssql_query("EXEC dbo.brm_sp_DownlineByBillingStatus ".$_SESSION['life_user_id']." ");		
      		while($resultdt=mssql_fetch_assoc($query12)){
		  	//$cdata12['BrokerID']=$result12['BrokerID'];
      			$aad[]=$_SESSION['life_user_id'];
      			$aad[]=$resultdt['BrokerID'];
      		}

      		$myarraydata=array_unique($aad);

      		$dt=	"IN(";
      		$i=1;
      		foreach ($myarraydata as $key=>$val){
      			$dt.="'".$val."'";
      			if($i <= count($myarraydata)-1 ){
      				$dt.=",";
      			}
      			$i++;
      		}
      		$dt.=" )";

      		$data=mssql_query("SELECT CustomerTypeName,count(StatusName) as statuscount,StatusName FROM dbo.BP2_DistinctAccountDetails_1  WHERE BrokerID ".$dt." and CustomerTypeName = 'Residential' group by CustomerTypeName,StatusName  order by CustomerTypeName");


      		while($result= mssql_fetch_assoc($data)){




      			$cdata['y']=$result['StatusName'];
      			$cdata['a']=$result['statuscount'];









      			$aa[]= $cdata; 
      		}

      		$jsondata=$aa;


      		echo json_encode(utf8ize($jsondata));




      	} 
      	function callgrapcommercial(){



      		$query12 = mssql_query("EXEC dbo.brm_sp_DownlineByBillingStatus ".$_SESSION['life_user_id']." ");		
      		while($resultdt=mssql_fetch_assoc($query12)){
		  	//$cdata12['BrokerID']=$result12['BrokerID'];
      			$aad[]=$_SESSION['life_user_id'];
      			$aad[]=$resultdt['BrokerID'];
      		}

      		$myarraydata=array_unique($aad);

      		$dt=	"IN(";
      		$i=1;
      		foreach ($myarraydata as $key=>$val){
      			$dt.="'".$val."'";
      			if($i <= count($myarraydata)-1 ){
      				$dt.=",";
      			}
      			$i++;
      		}
      		$dt.=" )";

      		$data=mssql_query("SELECT CustomerTypeName,count(StatusName) as statuscount,StatusName FROM dbo.BP2_DistinctAccountDetails_1  WHERE BrokerID ".$dt." and CustomerTypeName = 'Commercial' group by CustomerTypeName,StatusName  order by CustomerTypeName");


      		while($result= mssql_fetch_assoc($data)){




      			$cdata['y']=$result['StatusName'];
      			$cdata['a']=$result['statuscount'];









      			$aa[]= $cdata; 
      		}

      		$jsondata=$aa;


      		echo json_encode(utf8ize($jsondata));




      	} 









      	function calllabel($labeldata) {
      		if($labeldata=='column'){
      			$query12 = mssql_query("EXEC dbo.brm_sp_DownlineByBillingStatus ".$_SESSION['life_user_id']." ");		
      			while($resultdt=mssql_fetch_assoc($query12)){
		  	//$cdata12['BrokerID']=$result12['BrokerID'];
      				$aad[]=$_SESSION['life_user_id'];
      				$aad[]=$resultdt['BrokerID'];
      			}

      			$myarraydata=array_unique($aad);

      			$dt=	"IN(";
      			$i=1;
      			foreach ($myarraydata as $key=>$val){
      				$dt.="'".$val."'";
      				if($i <= count($myarraydata)-1 ){
      					$dt.=",";
      				}
      				$i++;
      			}
      			$dt.=" )";

      			$data=mssql_query("SELECT CustomerTypeName FROM dbo.BP2_DistinctAccountDetails_1 WHERE BrokerID ".$dt."");
      			$i=1;

      			while ($result= mssql_fetch_assoc($data)){
      				if($i==1){
      					echo $result['CustomerTypeName'];
      				}
      				$i++; 



      			}

      		}




      	}



      	function stackCommercial(){	
      		$query12 = mssql_query("EXEC dbo.brm_sp_DownlineByBillingStatus ".$_SESSION['life_user_id']." ");		
      		while($resultdt=mssql_fetch_assoc($query12)){
		  	//$cdata12['BrokerID']=$result12['BrokerID'];
      			$aad[]=$_SESSION['life_user_id'];
      			$aad[]=$resultdt['BrokerID'];
      		}

      		$myarraydata=array_unique($aad);

      		$dt=	"IN(";
      		$i=1;
      		foreach ($myarraydata as $key=>$val){
      			$dt.="'".$val."'";
      			if($i <= count($myarraydata)-1 ){
      				$dt.=",";
      			}
      			$i++;
      		}
      		$dt.=" )";

      		$stackdata=mssql_query("SELECT CustomerTypeName,count(StatusName) as statuscount,StatusName,BrokerID FROM dbo.BP2_DistinctAccountDetails_1  WHERE BrokerID ".$dt." and CustomerTypeName = 'Commercial' group by CustomerTypeName,StatusName,BrokerID  order by CustomerTypeName");	

      		while($resultstatck=mssql_fetch_assoc($stackdata)){
      			$arr[$resultstatck['StatusName']][$resultstatck['BrokerID']] =$resultstatck['statuscount'] ;

      		}
      		$barr=$arr;
      		ksort($barr) ;

      		$stackdata2=mssql_query("SELECT CustomerTypeName,count(StatusName) as statuscount,StatusName,BrokerID FROM dbo.BP2_DistinctAccountDetails_1  WHERE BrokerID ".$dt." and CustomerTypeName = 'Commercial' group by CustomerTypeName,StatusName,BrokerID  order by CustomerTypeName");
      		$arr='';	
      		while($resultstatck2=mssql_fetch_assoc($stackdata2)){
      			$arr[$resultstatck2['BrokerID']][$resultstatck2['StatusName']] =$resultstatck2['statuscount'] ;
      		}

      		$narr='';
      		$i=0;
      		foreach($arr as $key=>$val){
      			$narr[$i]['name']=$key;
      			$j=0;
      			foreach($barr as $k=>$v){
      				$thisvalue=$barr[$k][$key];
      				if($thisvalue==''){
      					$thisvalue=0;
      				}
      				$narr[$i]['data'][$j]=$thisvalue;
      				$j++;}
      				$i++;}
      				echo json_encode($narr);

      			}



      			function stackres(){
      				$query12 = mssql_query("EXEC dbo.brm_sp_DownlineByBillingStatus ".$_SESSION['life_user_id']." ");		
      				while($resultdt=mssql_fetch_assoc($query12)){
		  	//$cdata12['BrokerID']=$result12['BrokerID'];
      					$aad[]=$_SESSION['life_user_id'];
      					$aad[]=$resultdt['BrokerID'];
      				}

      				$myarraydata=array_unique($aad);

      				$dt=	"IN(";
      				$i=1;
      				foreach ($myarraydata as $key=>$val){
      					$dt.="'".$val."'";
      					if($i <= count($myarraydata)-1 ){
      						$dt.=",";
      					}
      					$i++;
      				}
      				$dt.=" )";

      				$stackdata=mssql_query("SELECT CustomerTypeName,count(StatusName) as statuscount,StatusName,BrokerID FROM dbo.BP2_DistinctAccountDetails_1  WHERE BrokerID ".$dt." and CustomerTypeName = 'Residential' group by CustomerTypeName,StatusName,BrokerID  order by CustomerTypeName");	


      				while($resultstatck=mssql_fetch_assoc($stackdata)){
      					$arr[$resultstatck['StatusName']][$resultstatck['BrokerID']] =$resultstatck['statuscount'] ;

      				}
      				$barr=$arr;
      				ksort($barr) ;

      				$stackdata2=mssql_query("SELECT CustomerTypeName,count(StatusName) as statuscount,StatusName,BrokerID FROM dbo.BP2_DistinctAccountDetails_1  WHERE BrokerID ".$dt." and CustomerTypeName = 'Residential' group by CustomerTypeName,StatusName,BrokerID  order by CustomerTypeName");
      				$arr='';	
      				while($resultstatck2=mssql_fetch_assoc($stackdata2)){
      					$arr[$resultstatck2['BrokerID']][$resultstatck2['StatusName']] =$resultstatck2['statuscount'] ;
      				}

      				$narr='';
      				$i=0;
      				foreach($arr as $key=>$val){
      					$narr[$i]['name']=$key;
      					$j=0;
      					foreach($barr as $k=>$v){
      						$thisvalue=$barr[$k][$key];
      						if($thisvalue==''){
      							$thisvalue=0;
      						}
      						$narr[$i]['data'][$j]=$thisvalue;
      						$j++;}
      						$i++;}
      						echo json_encode($narr);

      					}





      					function callstackcategorycommercial(){
      						$query12 = mssql_query("EXEC dbo.brm_sp_DownlineByBillingStatus ".$_SESSION['life_user_id']." ");		
      						while($resultdt=mssql_fetch_assoc($query12)){
		  	//$cdata12['BrokerID']=$result12['BrokerID'];
      							$aad[]=$_SESSION['life_user_id'];
      							$aad[]=$resultdt['BrokerID'];
      						}

      						$myarraydata=array_unique($aad);

      						$dt=	"IN(";
      						$i=1;
      						foreach ($myarraydata as $key=>$val){
      							$dt.="'".$val."'";
      							if($i <= count($myarraydata)-1 ){
      								$dt.=",";
      							}
      							$i++;
      						}
      						$dt.=" )";

      						$stackdata=mssql_query("SELECT CustomerTypeName,count(StatusName) as statuscount,StatusName FROM dbo.BP2_DistinctAccountDetails_1  WHERE BrokerID ".$dt." and CustomerTypeName = 'Commercial' group by CustomerTypeName,StatusName  order by CustomerTypeName");


      						while($resultstatck3=mssql_fetch_assoc($stackdata)){
      							$crr[$resultstatck3['StatusName']] =$resultstatck3['statuscount'] ;
      						}

      						$st=	"[";
      						$i=1;
      						foreach ($crr as $key=>$val){
      							$st.="'".$key."'";
      							if($i <= count($crr)-1 ){
      								$st.=",";
      							}
      							$i++;
      						}
      						$st.=" ]";
      						echo $st;
      					}
      					function callstackcategoryres(){
      						$query12 = mssql_query("EXEC dbo.brm_sp_DownlineByBillingStatus ".$_SESSION['life_user_id']." ");		
      						while($resultdt=mssql_fetch_assoc($query12)){
		  	//$cdata12['BrokerID']=$result12['BrokerID'];
      							$aad[]=$_SESSION['life_user_id'];
      							$aad[]=$resultdt['BrokerID'];
      						}

      						$myarraydata=array_unique($aad);

      						$dt=	"IN(";
      						$i=1;
      						foreach ($myarraydata as $key=>$val){
      							$dt.="'".$val."'";
      							if($i <= count($myarraydata)-1 ){
      								$dt.=",";
      							}
      							$i++;
      						}
      						$dt.=" )";

      						$stackdatares=mssql_query("SELECT CustomerTypeName,count(StatusName) as statuscount,StatusName FROM dbo.BP2_DistinctAccountDetails_1  WHERE BrokerID ".$dt." and CustomerTypeName = 'Residential' group by CustomerTypeName,StatusName  order by CustomerTypeName");

      						while($resultstatckres3=mssql_fetch_assoc($stackdatares)){
      							$crrres[$resultstatckres3['StatusName']] =$resultstatckres3['statuscount'] ;
      						}

      						$stres=	"[";
      						$i=1;
      						foreach ($crrres as $key=>$val){
      							$stres.="'".$key."'";
      							if($i <= count($crrres)-1 ){
      								$stres.=",";
      							}
      							$i++;
      						}
      						$stres.=" ]";
      						echo $stres;
      					}





      					?>

      					<div class="col-md-12 account_bt">

      						<div class="row right_btns"> 
      							<!-- Newly commented -->
      							<!--<div class=" advnc search"  style="float: right;position: relative;z-index: 999;margin: 10px 10px 5px 0px;text-transform: uppercase;margin-right:5px;"><a class="btn-sm btn btn-primary"   href="dashboard.php">VIEW MY ACCOUNTS ONLY</a></div>
      							<div class=" advnc search"  style="float:right;position: relative;z-index: 999;margin: 10px 10px 5px 0px;text-transform: uppercase;"><a class="btn-sm btn btn-primary" href="dashboard.php?utype=downline">VIEW MY DOWNLINE ACCOUNTS</a></div>
      							<div class=" advnc search" style="float:right;position: relative;z-index: 999;margin: 10px 10px 5px 0px;text-transform: uppercase;"  ><a class="btn-sm btn btn-primary" style=" background-color: #76838f; border: 1px solid #76838f; "  href="dashboard.php?utype=parent">VIEW ALL ACCOUNTS</a></div>-->
      						</div>







         <!--<div class="row right_btns"><button type="button" class="btn btn-primary blue">View All Accounts</button>
          <button type="button" class="btn btn-primary blue">View My Downline Accounts</button>
          <button type="button" class="btn btn-primary blue active">View My Accounts Only</button></div>
          
      -->         



  </div>


  <div class="col-md-12 dash_brd">
  	<div class="panel">
  		<div class="panel-body">
  			<div class="row row-lg">


  				<div class="col-md-6">
  					<!-- Example Line -->
  					<div class="example-wrap margin-md-0">
  						<h4 class="example-title">Number Of Accounts By Service State</h4>
  						<!-- <p>Use function: <code>Morris.Line(options)</code> to generate chart.</p> -->
  						<div class="example">
  							<div id="exampleMorrisLine"></div>
  						</div>
  					</div>
  					<!-- End Example Line -->
  				</div> 
  				<div class="col-md-6">
  					<!-- Example Donut -->
  					<div class="example-wrap">
  						<h4 class="example-title">Number of Accounts By Utility</h4>
  						<!--<p>Create a Donut chart using <code>Morris.Donut(options)</code>.</p> -->
  						<div class="example">
  							<div id="exampleMorrisDonut"></div>
  						</div>
  					</div>
  					<!-- End Example Donut -->
  				</div>
  			</div>




      <!-- <div class="col-md-6">
            
              <div class="example-wrap">
                <h4 class="example-title">ACCOUNTS BY TYPE AND STATUS</h4>
               <p>Create an area chart using: <code>Morris.Area(options)</code>.</p>
                <div class="example">
                  <div id="exampleMorrisArea"></div>
                </div>
              </div>
             
          </div>  -->
      </div>
  </div>
</div>
<!-- End Panel -->

<!-- Panel -->
     <!-- <div class="panel">
        <div class="panel-body">
        <div class="row row-lg">
          
          
<div class="col-md-6">
           
              <div class="example-wrap margin-md-0">
                <h4 class="example-title">Commercial Accounts By Status</h4>
            
                <div class="example">
                  <div id="exampleMorrisBarcomm"></div>
                </div>
              </div>
             
            </div>
<div class="col-md-6">
             
              <div class="example-wrap margin-md-0">
                <h4 class="example-title">Residential Accounts By Status</h4>
           
                <div class="example">
                  <div id="exampleMorrisBarres"></div>
                </div>
              </div>
            
            </div>
            
          </div> 
          
        </div>
    </div> -->

    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>


    <div class="bg_clr"></div>



    <div class="col-md-12 dash_brd no_mgn">
    	<div class="panel">
    		<div class="panel-body">
    			<div class="row row-lg">


    				<div class="col-md-6">

    					<div class="example-wrap margin-md-0">
    						<h4 class="example-title">RESIDENTIAL ACCOUNTS BY STATUS</h4>

    						<div class="example">
    							<div id="exampleC3StackedBar"></div>
    						</div>
    					</div>

    				</div>

    				<div class="col-md-6">

    					<div class="example-wrap margin-md-0">
    						<h4 class="example-title">Commercial Accounts By Status</h4>

    						<div class="example">
    							<div id="exampleC3StackedBar2"></div>
    						</div>
    					</div>

    				</div>

    			</div> 

    		</div>
    	</div> 
    </div>
    <script src="global/vendor/jquery/jquery.min.js"></script>
    <script src="global/vendor/bootstrap/bootstrap.min.js"></script>
    <script src="global/vendor/animsition/animsition.min.js"></script>
    <script src="global/vendor/asscroll/jquery-asScroll.min.js"></script>
    <script src="global/vendor/mousewheel/jquery.mousewheel.min.js"></script>
    <script src="global/vendor/asscrollable/jquery.asScrollable.all.min.js"></script>
    <script src="global/vendor/ashoverscroll/jquery-asHoverScroll.min.js"></script>

    <!-- Plugins -->
    <script src="global/vendor/switchery/switchery.min.js"></script>
    <script src="global/vendor/intro-js/intro.min.js"></script>
    <script src="global/vendor/screenfull/screenfull.min.js"></script>
    <script src="global/vendor/slidepanel/jquery-slidePanel.min.js"></script>

    <!-- Plugins For This Page -->
    <script src="global/js/raphael-min.js"></script>
    <script src="global/js/morris.min.js"></script>

    <!-- Scripts -->
    <script src="global/js/core.min.js"></script>
    <script src="topbar/assets/js/site.min.js"></script>

    <script src="topbar/assets/js/sections/menu.min.js"></script>
    <script src="topbar/assets/js/sections/menubar.min.js"></script>
    <script src="topbar/assets/js/sections/sidebar.min.js"></script>

    <script src="global/js/configs/config-colors.min.js"></script>
    <script src="topbar/assets/js/configs/config-tour.min.js"></script>
    <script src="global/js/components/asscrollable.min.js"></script>
    <script src="global/js/components/animsition.min.js"></script>
    <script src="global/js/components/slidepanel.min.js"></script>
    <script src="global/js/components/switchery.min.js"></script>
    <script type="text/javascript">
    	Highcharts.setOptions({
    		lang: {
    			thousandsSep: ','
    		}
    	});
    	Highcharts.chart('exampleC3StackedBar2', {
    		chart: {
    			type: 'column'
    		},
    		title: {
    			text: ''
    		},
    		xAxis: {
    			categories: <?php callstackcategorycommercial(); ?>
    		},
    		yAxis: {
    			min: 0,
    			title: {
    				text: ''
    			},
    			stackLabels: {
    				enabled: false,
    				style: {
    					fontWeight: 'bold',
    					color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
    				}
    			}
    		},
    		legend: {
    			align: 'right',
    			x: -30,
    			verticalAlign: 'top',
    			y: 25,
    			floating: true,
    			backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
    			borderColor: '#CCC',
    			borderWidth: 1,
    			shadow: false
    		},
    		tooltip: {
    			headerFormat: '<b>{point.x}</b><br/>',
    			pointFormat: '{series.name}: {point.y:,.1f}<br/>Total: {point.stackTotal:,.1f}'

    		},
    		plotOptions: {
    			column: {
    				stacking: 'normal',
    				dataLabels: {
    					enabled: true,
    					color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'
    				}
    			}
    		},
    		series:<?php stackCommercial(); ?>
    	});
    </script>
    <script type="text/javascript">
    	Highcharts.setOptions({
    		lang: {
    			thousandsSep: ','
    		}
    	});
    	Highcharts.chart('exampleC3StackedBar', {
    		chart: {
    			type: 'column'
    		},
    		title: {
    			text: ''
    		},
    		xAxis: {
    			categories: <?php callstackcategoryres(); ?>
    		},
    		yAxis: {
    			min: 0,
    			title: {
    				text: ''
    			},
    			stackLabels: {
    				enabled: false,
    				style: {
    					fontWeight: 'bold',
    					color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
    				}
    			}
    		},
    		legend: {
    			align: 'right',
    			x: -30,
    			verticalAlign: 'top',
    			y: 25,
    			floating: true,
    			backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
    			borderColor: '#CCC',
    			borderWidth: 1,
    			shadow: false
    		},
    		tooltip: {
    			headerFormat: '<b>{point.x}</b><br/>',
    			pointFormat: '{series.name}: {point.y:,.1f}<br/>Total: {point.stackTotal:,.1f}',

    		},
    		plotOptions: {
    			column: {
    				stacking: 'normal',
    				dataLabels: {
    					enabled: true,
    					color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'
    				}
    			}
    		},
    		series:<?php stackres(); ?>
    	});
    </script>
    <script type="text/javascript">

    	!function(document,window,$)
    	{"use strict";var Site=window.Site;
    	$(document).ready(function($){Site.run()}),




/*function(){Morris.Area({element:"exampleMorrisArea",data:[{y:"AB",a:2070},{y:"Ca",a:2170}],xkey:"y",ykeys:["a"],labels:["Series A"],behaveLikeLine:!0,resize:!0,pointSize:3,smooth:!0,gridTextColor:"#474e54",gridLineColor:"#eef0f2",goalLineColors:"#e3e6ea",gridTextFamily:$.configs.get("site","fontFamily"),gridTextWeight:"300",numLines:7,gridtextSize:14,lineWidth:1,fillOpacity:.1,lineColors:[$.colors("primary",600),$.colors("green",600)]})}(),
function(){Morris.Bar({element:"exampleMorrisBarres",data:<?php callgraphresidential()?>,xkey:"y",ykeys:["a"],labels:["Residential"],barGap:6,xLabelMargin: 10,barSizeRatio:.35,smooth:!0,gridTextColor:"#474e54",gridLineColor:"#eef0f2",goalLineColors:"#e3e6ea",gridTextFamily:$.configs.get("site","fontFamily"),gridTextWeight:"300",numLines:6,gridtextSize:14,resize:!0,barColors:[$.colors("red",500),$.colors("grey",400)]})}(),
function(){Morris.Bar({element:"exampleMorrisBarcomm",data:<?php callgrapcommercial()?>,xkey:"y",ykeys:["a"],labels:["Commercial"],barGap:6,xLabelMargin: 10,barSizeRatio:.35,smooth:!0,gridTextColor:"#474e54",gridLineColor:"#eef0f2",goalLineColors:"#e3e6ea",gridTextFamily:$.configs.get("site","fontFamily"),gridTextWeight:"300",numLines:6,gridtextSize:14,resize:!0,barColors:[$.colors("red",500),$.colors("grey",400)]})}(),*/
function(){Morris.Donut({element:"exampleMorrisDonut",data:<?php callgraph('Donut')?>,resize:!0,colors:[$.colors("red",500),$.colors("primary",500),$.colors("grey",400),$.colors("green",500),$.colors("yellow",500),$.colors("pink",500),$.colors("purple",500)]})}(), 
function(){Morris.Donut({element:"exampleMorrisLine",data:<?php callgraphpie('pie')?>,resize:!0,colors:[$.colors("red",500),$.colors("primary",500),$.colors("grey",400)]})}()}



(document,window,jQuery);
</script>

<script type="text/javascript">
	$(document).ready(function(){
		if($('#exampleMorrisDonut').html()==''){

			$('#exampleMorrisDonut').html('<p>No data available</p>');

		}
		if($('#exampleMorrisBar').html()==''){

			$('#exampleMorrisBar').html('<p>No data available</p>');

		}
		if($('#exampleMorrisLine').html()==''){

			$('#exampleMorrisLine').html('<p>No data available</p>');

		}

	});
</script>


<?php
} else if($_REQUEST['utype']=='downline') {

	function utf8ize($d) {
		if (is_array($d)) {
			foreach ($d as $k => $v) {
				$d[$k] = utf8ize($v);
			}
		} else if (is_string ($d)) {
			return utf8_encode($d);
		}
		return $d;
	}
	function callgraphpie($datatype){
		if($datatype=='pie') {
			$query12 = mssql_query("EXEC dbo.brm_sp_DownlineByBillingStatus ".$_SESSION['life_user_id']." ");		
			while($resultdt=mssql_fetch_assoc($query12)){
		  	//$cdata12['BrokerID']=$result12['BrokerID'];
				$aad[]=$resultdt['BrokerID'];
			}

			$myarraydata=array_unique($aad);

			$dt=	"IN(";
			$i=1;
			foreach ($myarraydata as $key=>$val){
				$dt.="'".$val."'";
				if($i <= count($myarraydata)-1 ){
					$dt.=",";
				}
				$i++;
			}
			$dt.=" )";
			$data=mssql_query("SELECT StateCode, COUNT(StateCode) AS 'Count'
				FROM dbo.BP2_DistinctAccountDetails_1
				WHERE BrokerID ".$dt."
				GROUP BY StateCode");

			while($result= mssql_fetch_assoc($data)){

				$cdata['label']=$result['StateCode'];
				$cdata['value']=$result['Count'];






				$aa[]= $cdata; 
			}

			$jsondata=$aa;;


			echo json_encode(utf8ize($jsondata));


		}
	}
	function callgraparea($datatype){
		if($datatype=='area') {

			$data=mssql_query("SELECT StateCode, COUNT(StateCode) AS 'Count'
				FROM dbo.BP2_DistinctAccountDetails_1
				WHERE BrokerID = '".$_SESSION['life_user_id']."'
				GROUP BY StateCode");

			while($result= mssql_fetch_assoc($data)){

				$cdata['label']=$result['StateCode'];
				$cdata['value']=$result['Count'];






				$aa[]= $cdata; 
			}

			$jsondata=$aa;;


			echo json_encode(utf8ize($jsondata));


		}
	}
	function callgraph($datatype){

		if($datatype=='Donut'){

			$query12 = mssql_query("EXEC dbo.brm_sp_DownlineByBillingStatus ".$_SESSION['life_user_id']." ");		
			while($resultdt=mssql_fetch_assoc($query12)){
		  	//$cdata12['BrokerID']=$result12['BrokerID'];
				$aad[]=$resultdt['BrokerID'];
			}

			$myarraydata=array_unique($aad);

			$dt=	"IN(";
			$i=1;
			foreach ($myarraydata as $key=>$val){
				$dt.="'".$val."'";
				if($i <= count($myarraydata)-1 ){
					$dt.=",";
				}
				$i++;
			}
			$dt.=" )";




			$data= mssql_query("SELECT UtilityName, COUNT(UtilityName) AS 'Count'
				FROM dbo.BP2_DistinctAccountDetails_1
				WHERE BrokerID ".$dt."
				GROUP BY UtilityName");


			while($result=mssql_fetch_assoc($data)){

				$cdata['label']=$result['UtilityName'];
				$cdata['value']=$result['Count'];






				$aa[]= $cdata; 
			}

			$jsondata=$aa;;


			echo json_encode(utf8ize($jsondata));




		} else if($datatype='column'){
			$query12 = mssql_query("EXEC dbo.brm_sp_DownlineByBillingStatus ".$_SESSION['life_user_id']." ");		
			while($resultdt=mssql_fetch_assoc($query12)){
		  	//$cdata12['BrokerID']=$result12['BrokerID'];
				$aad[]=$resultdt['BrokerID'];
			}

			$myarraydata=array_unique($aad);

			$dt=	"IN(";
			$i=1;
			foreach ($myarraydata as $key=>$val){
				$dt.="'".$val."'";
				if($i <= count($myarraydata)-1 ){
					$dt.=",";
				}
				$i++;
			}
			$dt.=" )";

			$data=mssql_query("SELECT CustomerTypeName,count(StatusName) as statuscount,StatusName FROM dbo.BP2_DistinctAccountDetails_1  WHERE BrokerID ".$dt." group by CustomerTypeName,StatusName order by CustomerTypeName");


			while($result= mssql_fetch_assoc($data)){




				$cdata['y']=$result['StatusName'];
				$cdata['a']=$result['statuscount'];









				$aa[]= $cdata; 
			}

			$jsondata=$aa;;


			echo json_encode(utf8ize($jsondata));



		} else if($datatype='pie'){
			$query12 = mssql_query("EXEC dbo.brm_sp_DownlineByBillingStatus ".$_SESSION['life_user_id']." ");		
			while($resultdt=mssql_fetch_assoc($query12)){
		  	//$cdata12['BrokerID']=$result12['BrokerID'];
				$aad[]=$resultdt['BrokerID'];
			}

			$myarraydata=array_unique($aad);

			$dt=	"IN(";
			$i=1;
			foreach ($myarraydata as $key=>$val){
				$dt.="'".$val."'";
				if($i <= count($myarraydata)-1 ){
					$dt.=",";
				}
				$i++;
			}
			$dt.=" )";


			$data=mssql_query("SELECT UtilityName, COUNT(UtilityName) AS 'Count'
				FROM dbo.BP2_DistinctAccountDetails_1
				WHERE BrokerID ".$dt."
				GROUP BY UtilityName");

			while($result= mssql_fetch_assoc($data)){

				$cdata['label']=$result['UtilityName'];
				$cdata['value']=$result['Count'];






				$aa[]= $cdata; 
			}

			$jsondata=$aa;;


			echo json_encode(utf8ize($jsondata));



		}




	}


	function callgraphresidential(){


		$query12 = mssql_query("EXEC dbo.brm_sp_DownlineByBillingStatus ".$_SESSION['life_user_id']." ");		
		while($resultdt=mssql_fetch_assoc($query12)){
		  	//$cdata12['BrokerID']=$result12['BrokerID'];
			$aad[]=$resultdt['BrokerID'];
		}

		$myarraydata=array_unique($aad);

		$dt=	"IN(";
		$i=1;
		foreach ($myarraydata as $key=>$val){
			$dt.="'".$val."'";
			if($i <= count($myarraydata)-1 ){
				$dt.=",";
			}
			$i++;
		}
		$dt.=" )";

		$data=mssql_query("SELECT CustomerTypeName,count(StatusName) as statuscount,StatusName FROM dbo.BP2_DistinctAccountDetails_1  WHERE BrokerID ".$dt."  and CustomerTypeName = 'Residential' group by CustomerTypeName,StatusName  order by CustomerTypeName");


		while($result= mssql_fetch_assoc($data)){




			$cdata['y']=$result['StatusName'];
			$cdata['a']=$result['statuscount'];









			$aa[]= $cdata; 
		}

		$jsondata=$aa;


		echo json_encode(utf8ize($jsondata));




	} 
	function callgrapcommercial(){



		$query12 = mssql_query("EXEC dbo.brm_sp_DownlineByBillingStatus ".$_SESSION['life_user_id']." ");		
		while($resultdt=mssql_fetch_assoc($query12)){
		  	//$cdata12['BrokerID']=$result12['BrokerID'];
			$aad[]=$resultdt['BrokerID'];
		}

		$myarraydata=array_unique($aad);

		$dt=	"IN(";
		$i=1;
		foreach ($myarraydata as $key=>$val){
			$dt.="'".$val."'";
			if($i <= count($myarraydata)-1 ){
				$dt.=",";
			}
			$i++;
		}
		$dt.=" )";

		$data=mssql_query("SELECT CustomerTypeName,count(StatusName) as statuscount,StatusName FROM dbo.BP2_DistinctAccountDetails_1  WHERE BrokerID ".$dt." and CustomerTypeName = 'Commercial' group by CustomerTypeName,StatusName  order by CustomerTypeName");


		while($result= mssql_fetch_assoc($data)){




			$cdata['y']=$result['StatusName'];
			$cdata['a']=$result['statuscount'];









			$aa[]= $cdata; 
		}

		$jsondata=$aa;


		echo json_encode(utf8ize($jsondata));




	} 




	function calllabel($labeldata) {
		if($labeldata=='column'){
			$query12 = mssql_query("EXEC dbo.brm_sp_DownlineByBillingStatus ".$_SESSION['life_user_id']." ");		
			while($resultdt=mssql_fetch_assoc($query12)){
		  	//$cdata12['BrokerID']=$result12['BrokerID'];
				$aad[]=$resultdt['BrokerID'];
			}

			$myarraydata=array_unique($aad);

			$dt=	"IN(";
			$i=1;
			foreach ($myarraydata as $key=>$val){
				$dt.="'".$val."'";
				if($i <= count($myarraydata)-1 ){
					$dt.=",";
				}
				$i++;
			}
			$dt.=" )";

			$data=mssql_query("SELECT CustomerTypeName FROM dbo.BP2_DistinctAccountDetails_1 WHERE BrokerID ".$dt."");
			$i=1;

			while ($result= mssql_fetch_assoc($data)){
				if($i==1){
					echo $result['CustomerTypeName'];
				}
				$i++; 



			}

		}




	}

	function stackCommercial(){	
		$query12 = mssql_query("EXEC dbo.brm_sp_DownlineByBillingStatus ".$_SESSION['life_user_id']." ");		
		while($resultdt=mssql_fetch_assoc($query12)){
		  	//$cdata12['BrokerID']=$result12['BrokerID'];

			$aad[]=$resultdt['BrokerID'];
		}

		$myarraydata=array_unique($aad);

		$dt=	"IN(";
		$i=1;
		foreach ($myarraydata as $key=>$val){
			$dt.="'".$val."'";
			if($i <= count($myarraydata)-1 ){
				$dt.=",";
			}
			$i++;
		}
		$dt.=" )";

		$stackdata=mssql_query("SELECT CustomerTypeName,count(StatusName) as statuscount,StatusName,BrokerID FROM dbo.BP2_DistinctAccountDetails_1  WHERE BrokerID ".$dt." and CustomerTypeName = 'Commercial' group by CustomerTypeName,StatusName,BrokerID  order by CustomerTypeName");	

		while($resultstatck=mssql_fetch_assoc($stackdata)){
			$arr[$resultstatck['StatusName']][$resultstatck['BrokerID']] =$resultstatck['statuscount'] ;

		}
		$barr=$arr;
		ksort($barr) ;

		$stackdata2=mssql_query("SELECT CustomerTypeName,count(StatusName) as statuscount,StatusName,BrokerID FROM dbo.BP2_DistinctAccountDetails_1  WHERE BrokerID ".$dt." and CustomerTypeName = 'Commercial' group by CustomerTypeName,StatusName,BrokerID  order by CustomerTypeName");
		$arr='';	
		while($resultstatck2=mssql_fetch_assoc($stackdata2)){
			$arr[$resultstatck2['BrokerID']][$resultstatck2['StatusName']] =$resultstatck2['statuscount'] ;
		}

		$narr='';
		$i=0;
		foreach($arr as $key=>$val){
			$narr[$i]['name']=$key;
			$j=0;
			foreach($barr as $k=>$v){
				$thisvalue=$barr[$k][$key];
				if($thisvalue==''){
					$thisvalue=0;
				}
				$narr[$i]['data'][$j]=$thisvalue;
				$j++;}
				$i++;}
				echo json_encode($narr);

			}



			function stackres(){
				$query12 = mssql_query("EXEC dbo.brm_sp_DownlineByBillingStatus ".$_SESSION['life_user_id']." ");		
				while($resultdt=mssql_fetch_assoc($query12)){
		  	//$cdata12['BrokerID']=$result12['BrokerID'];
//$aad[]=$_SESSION['life_user_id'];
					$aad[]=$resultdt['BrokerID'];
				}

				$myarraydata=array_unique($aad);

				$dt=	"IN(";
				$i=1;
				foreach ($myarraydata as $key=>$val){
					$dt.="'".$val."'";
					if($i <= count($myarraydata)-1 ){
						$dt.=",";
					}
					$i++;
				}
				$dt.=" )";

				$stackdata=mssql_query("SELECT CustomerTypeName,count(StatusName) as statuscount,StatusName,BrokerID FROM dbo.BP2_DistinctAccountDetails_1  WHERE BrokerID ".$dt." and CustomerTypeName = 'Residential' group by CustomerTypeName,StatusName,BrokerID  order by CustomerTypeName");	


				while($resultstatck=mssql_fetch_assoc($stackdata)){
					$arr[$resultstatck['StatusName']][$resultstatck['BrokerID']] =$resultstatck['statuscount'] ;

				}
				$barr=$arr;
				ksort($barr) ;

				$stackdata2=mssql_query("SELECT CustomerTypeName,count(StatusName) as statuscount,StatusName,BrokerID FROM dbo.BP2_DistinctAccountDetails_1  WHERE BrokerID ".$dt." and CustomerTypeName = 'Residential' group by CustomerTypeName,StatusName,BrokerID  order by CustomerTypeName");
				$arr='';	
				while($resultstatck2=mssql_fetch_assoc($stackdata2)){
					$arr[$resultstatck2['BrokerID']][$resultstatck2['StatusName']] =$resultstatck2['statuscount'] ;
				}

				$narr='';
				$i=0;
				foreach($arr as $key=>$val){
					$narr[$i]['name']=$key;
					$j=0;
					foreach($barr as $k=>$v){
						$thisvalue=$barr[$k][$key];
						if($thisvalue==''){
							$thisvalue=0;
						}
						$narr[$i]['data'][$j]=$thisvalue;
						$j++;}
						$i++;}
						echo json_encode($narr);

					}






					function callstackcategorycommercial(){

						$stackdata=mssql_query("EXEC Brm_sp_GetBrokersDownline_AccountSummary ".$_SESSION['life_user_id'].",'Commercial'");	
						while($resultstatck3=mssql_fetch_assoc($stackdata)){
							$crr[$resultstatck3['StatusName']] =$resultstatck3['Count'] ;
						}

						$st=	"[";
						$i=1;
						foreach ($crr as $key=>$val){
							$st.="'".$key."'";
							if($i <= count($crr)-1 ){
								$st.=",";
							}
							$i++;
						}
						$st.=" ]";
						echo $st;
					}
					function callstackcategoryres(){

						$stackdatares=mssql_query("EXEC Brm_sp_GetBrokersDownline_AccountSummary ".$_SESSION['life_user_id'].",'Residential'");	
						while($resultstatckres3=mssql_fetch_assoc($stackdatares)){
							$crrres[$resultstatckres3['StatusName']] =$resultstatckres3['Count'] ;
						}

						$stres=	"[";
						$i=1;
						foreach ($crrres as $key=>$val){
							$stres.="'".$key."'";
							if($i <= count($crrres)-1 ){
								$stres.=",";
							}
							$i++;
						}
						$stres.=" ]";
						echo $stres;
					}


					?>


					<div class="col-md-12 account_bt">

						<div class="row right_btns"> 
							<!-- Newly Commented -->
							<!--<div class=" advnc search"  style="float: right;position: relative;z-index: 999;margin: 10px 10px 10px 0px;text-transform: uppercase;margin-right:5px;"><a class="btn-sm btn btn-primary"   href="dashboard.php">VIEW MY ACCOUNTS ONLY</a></div>
							<div class=" advnc search"  style="float:right;position: relative;z-index: 999;margin: 10px 10px 10px 0px;text-transform: uppercase;"><a class="btn-sm btn btn-primary" style=" background-color: #76838f; border: 1px solid #76838f; "  href="dashboard.php?utype=downline">VIEW MY DOWNLINE ACCOUNTS</a></div>
							<div class=" advnc search" style="float:right;position: relative;z-index: 999;margin: 10px 10px 10px 0px;text-transform: uppercase;"  ><a class="btn-sm btn btn-primary"  href="dashboard.php?utype=parent">VIEW ALL ACCOUNTS</a></div>-->
						</div>







         <!--<div class="row right_btns"><button type="button" class="btn btn-primary blue">View All Accounts</button>
          <button type="button" class="btn btn-primary blue">View My Downline Accounts</button>
          <button type="button" class="btn btn-primary blue active">View My Accounts Only</button></div>
          
      -->         



  </div>

  <div class="col-md-12 dash_brd">
  	<div class="panel-body">
  		<div class="row row-lg">


  			<div class="col-md-6">
  				<!-- Example Line -->
  				<div class="example-wrap margin-md-0">
  					<h4 class="example-title">Number Of Accounts By Service State</h4>
  					<!-- <p>Use function: <code>Morris.Line(options)</code> to generate chart.</p> -->
  					<div class="example">
  						<div id="exampleMorrisLine"></div>
  					</div>
  				</div>
  				<!-- End Example Line -->
  			</div> 
  			<div class="col-md-6">
  				<!-- Example Donut -->
  				<div class="example-wrap">
  					<h4 class="example-title">Number of Accounts By Utility</h4>
  					<!--<p>Create a Donut chart using <code>Morris.Donut(options)</code>.</p> -->
  					<div class="example">
  						<div id="exampleMorrisDonut"></div>
  					</div>
  				</div>
  				<!-- End Example Donut -->
  			</div>




      <!-- <div class="col-md-6">
            
              <div class="example-wrap">
                <h4 class="example-title">ACCOUNTS BY TYPE AND STATUS</h4>
               <p>Create an area chart using: <code>Morris.Area(options)</code>.</p>
                <div class="example">
                  <div id="exampleMorrisArea"></div>
                </div>
              </div>
             
          </div>  -->
      </div>
  </div>
</div>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>

<div class="bg_clr"></div>

<div class="col-md-12 dash_brd no_mgn">
	<div class="panel-body">
		<div class="row row-lg">


			<div class="col-md-6">

				<div class="example-wrap margin-md-0">
					<h4 class="example-title">RESIDENTIAL ACCOUNTS BY STATUS</h4>

					<div class="example">
						<div id="exampleC3StackedBar"></div>
					</div>
				</div>

			</div>

			<div class="col-md-6">

				<div class="example-wrap margin-md-0">
					<h4 class="example-title">Commercial Accounts By Status</h4>

					<div class="example">
						<div id="exampleC3StackedBar2"></div>
					</div>
				</div>

			</div>

		</div> 

	</div>
</div> 




<!-- Core  -->
<script src="global/vendor/jquery/jquery.min.js"></script>
<script src="global/vendor/bootstrap/bootstrap.min.js"></script>
<script src="global/vendor/animsition/animsition.min.js"></script>
<script src="global/vendor/asscroll/jquery-asScroll.min.js"></script>
<script src="global/vendor/mousewheel/jquery.mousewheel.min.js"></script>
<script src="global/vendor/asscrollable/jquery.asScrollable.all.min.js"></script>
<script src="global/vendor/ashoverscroll/jquery-asHoverScroll.min.js"></script>

<!-- Plugins -->
<script src="global/vendor/switchery/switchery.min.js"></script>
<script src="global/vendor/intro-js/intro.min.js"></script>
<script src="global/vendor/screenfull/screenfull.min.js"></script>
<script src="global/vendor/slidepanel/jquery-slidePanel.min.js"></script>
<script src="http://getbootstrapadmin.com/remark/global/vendor/d3/d3.min.js"></script>
<script src="http://getbootstrapadmin.com/remark/global/vendor/c3/c3.min.js"></script>

<!-- Plugins For This Page -->
<script src="global/js/raphael-min.js"></script>
<script src="global/js/morris.min.js"></script>

<!-- Scripts -->
<script src="global/js/core.min.js"></script>
<script src="topbar/assets/js/site.min.js"></script>

<script src="topbar/assets/js/sections/menu.min.js"></script>
<script src="topbar/assets/js/sections/menubar.min.js"></script>
<script src="topbar/assets/js/sections/sidebar.min.js"></script>

<script src="global/js/configs/config-colors.min.js"></script>
<script src="topbar/assets/js/configs/config-tour.min.js"></script>
<script src="global/js/components/asscrollable.min.js"></script>
<script src="global/js/components/animsition.min.js"></script>
<script src="global/js/components/slidepanel.min.js"></script>
<script src="global/js/components/switchery.min.js"></script>
<script type="text/javascript">
	Highcharts.setOptions({
		lang: {
			thousandsSep: ','
		}
	});
	Highcharts.chart('exampleC3StackedBar2', {
		chart: {
			type: 'column'
		},
		title: {
			text: ''
		},
		xAxis: {
			categories: <?php callstackcategorycommercial(); ?>
		},
		yAxis: {
			min: 0,
			title: {
				text: ''
			},
			stackLabels: {
				enabled: false,
				style: {
					fontWeight: 'bold',
					color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
				}
			}
		},
		legend: {
			align: 'right',
			x: -30,
			verticalAlign: 'top',
			y: 25,
			floating: true,
			backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
			borderColor: '#CCC',
			borderWidth: 1,
			shadow: false
		},
		tooltip: {
			headerFormat: '<b>{point.x}</b><br/>',

			pointFormat: '{series.name}: {point.y:,.1f}<br/>Total: {point.stackTotal:,.1f}'
		},
		plotOptions: {
			column: {
				stacking: 'normal',
				dataLabels: {
					enabled: true,
					color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'
				}
			}
		},
		series:<?php stackCommercial(); ?>
	});
</script>
<script type="text/javascript">
	Highcharts.setOptions({
		lang: {
			thousandsSep: ','
		}
	});
	Highcharts.setOptions({
		lang: {
			thousandsSep: ','
		}
	});
	Highcharts.chart('exampleC3StackedBar', {
		chart: {
			type: 'column'
		},
		title: {
			text: ''
		},
		xAxis: {
			categories: <?php callstackcategoryres(); ?>
		},
		yAxis: {
			min: 0,
			title: {
				text: ''
			},
			stackLabels: {
				enabled: false,
				style: {
					fontWeight: 'bold',
					color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
				}
			}
		},
		legend: {
			align: 'right',
			x: -30,
			verticalAlign: 'top',
			y: 25,
			floating: true,
			backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
			borderColor: '#CCC',
			borderWidth: 1,
			shadow: false
		},
		tooltip: {
			headerFormat: '<b>{point.x}</b><br/>',
			pointFormat: '{series.name}: {point.y:,.1f}<br/>Total: {point.stackTotal:,.1f}'
		},
		plotOptions: {
			column: {
				stacking: 'normal',
				dataLabels: {
					enabled: true,
					color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'
				}
			}
		},
		series:<?php stackres(); ?>
	});
</script>

<script type="text/javascript">

	!function(document,window,$)
	{"use strict";var Site=window.Site;
	$(document).ready(function($){Site.run()}),




	/*function(){Morris.Area({element:"exampleMorrisArea",data:[{y:"AB",a:2070},{y:"Ca",a:2170}],xkey:"y",ykeys:["a"],labels:["Series A"],behaveLikeLine:!0,resize:!0,pointSize:3,smooth:!0,gridTextColor:"#474e54",gridLineColor:"#eef0f2",goalLineColors:"#e3e6ea",gridTextFamily:$.configs.get("site","fontFamily"),gridTextWeight:"300",numLines:7,gridtextSize:14,lineWidth:1,fillOpacity:.1,lineColors:[$.colors("primary",600),$.colors("green",600)]})}(),*/



	function(){Morris.Donut({element:"exampleMorrisDonut",data:<?php callgraph('Donut')?>,resize:!0,colors:[$.colors("red",500),$.colors("primary",500),$.colors("grey",400),$.colors("green",500),$.colors("yellow",500),$.colors("pink",500),$.colors("purple",500)]})}(), 
	function(){Morris.Donut({element:"exampleMorrisLine",data:<?php callgraphpie('pie')?>,resize:!0,colors:[$.colors("red",500),$.colors("primary",500),$.colors("grey",400)]})}()}



	(document,window,jQuery);
</script>

<script type="text/javascript">
	$(document).ready(function(){
		if($('#exampleMorrisDonut').html()==''){

			$('#exampleMorrisDonut').html('<p>No data available</p>');

		}
		if($('#exampleMorrisBar').html()==''){

			$('#exampleMorrisBar').html('<p>No data available</p>');

		}
		if($('#exampleMorrisLine').html()==''){

			$('#exampleMorrisLine').html('<p>No data available</p>');

		}

	});
</script>


<?php
} else {
	

	function utf8ize($d) {
		if (is_array($d)) {
			foreach ($d as $k => $v) {
				$d[$k] = utf8ize($v);
			}
		} else if (is_string ($d)) {
			return utf8_encode($d);
		}
		return $d;
	}
	function callgraphpie($datatype){
		if($datatype=='pie') {



			$data=mssql_query("SELECT StateCode, COUNT(StateCode) AS 'Count'
				FROM dbo.BP2_DistinctAccountDetails_1
				WHERE BrokerID = '".$_SESSION['life_user_id']."'
				GROUP BY StateCode");

			while($result= mssql_fetch_assoc($data)){

				$cdata['label']=$result['StateCode'];
				$cdata['value']=$result['Count'];






				$aa[]= $cdata; 
			}

			$jsondata=$aa;;


			echo json_encode(utf8ize($jsondata));


		}
	}
	function callgraparea($datatype){
		if($datatype=='area') {

			$data=mssql_query("SELECT StateCode, COUNT(StateCode) AS 'Count'
				FROM dbo.BP2_DistinctAccountDetails_1
				WHERE BrokerID = '".$_SESSION['life_user_id']."'
				GROUP BY StateCode");

			while($result= mssql_fetch_assoc($data)){

				$cdata['label']=$result['StateCode'];
				$cdata['value']=$result['Count'];






				$aa[]= $cdata; 
			}

			$jsondata=$aa;;


			echo json_encode(utf8ize($jsondata));


		}
	}
	function callgraph($datatype){

		if($datatype=='Donut'){

			$data= mssql_query("SELECT UtilityName, COUNT(UtilityName) AS 'Count'
				FROM dbo.BP2_DistinctAccountDetails_1
				WHERE BrokerID = '".$_SESSION['life_user_id']."'
				GROUP BY UtilityName");

			while($result=mssql_fetch_assoc($data)){

				$cdata['label']=$result['UtilityName'];
				$cdata['value']=$result['Count'];






				$aa[]= $cdata; 
			}

			$jsondata=$aa;;


			echo json_encode(utf8ize($jsondata));




		} else if($datatype='column'){

			$data=mssql_query("SELECT CustomerTypeName,count(StatusName) as statuscount,StatusName FROM dbo.BP2_DistinctAccountDetails_1  WHERE BrokerID = '".$_SESSION['life_user_id']."'  group by CustomerTypeName,StatusName order by CustomerTypeName ");


			while($result= mssql_fetch_assoc($data)){




				$cdata['y']=$result['StatusName'];
				$cdata['a']=$result['statuscount'];









				$aa[]= $cdata; 
			}

			$jsondata=$aa;;


			echo json_encode(utf8ize($jsondata));



		} else if($datatype='pie'){



			$data=mssql_query("SELECT UtilityName, COUNT(UtilityName) AS 'Count'
				FROM dbo.BP2_DistinctAccountDetails_1
				WHERE BrokerID = '".$_SESSION['life_user_id']."'
				GROUP BY UtilityName");

			while($result= mssql_fetch_assoc($data)){

				$cdata['label']=$result['UtilityName'];
				$cdata['value']=$result['Count'];






				$aa[]= $cdata; 
			}

			$jsondata=$aa;;


			echo json_encode(utf8ize($jsondata));



		}




	}
	function callgraphresidential(){



		$data=mssql_query("SELECT CustomerTypeName,count(StatusName) as statuscount,StatusName FROM dbo.BP2_DistinctAccountDetails_1  WHERE BrokerID = '".$_SESSION['life_user_id']."' and CustomerTypeName = 'Residential'  group by CustomerTypeName,StatusName order by CustomerTypeName ");


		while($result= mssql_fetch_assoc($data)){




			$cdata['y']=$result['StatusName'];
			$cdata['a']=$result['statuscount'];









			$aa[]= $cdata; 
		}

		$jsondata=$aa;;


		echo json_encode(utf8ize($jsondata));


	} 
	function callgrapcommercial(){



		$data=mssql_query("SELECT CustomerTypeName,count(StatusName) as statuscount,StatusName FROM dbo.BP2_DistinctAccountDetails_1  WHERE BrokerID = '".$_SESSION['life_user_id']."' and CustomerTypeName = 'Commercial'  group by CustomerTypeName,StatusName order by CustomerTypeName ");


		while($result= mssql_fetch_assoc($data)){




			$cdata['y']=$result['StatusName'];
			$cdata['a']=$result['statuscount'];









			$aa[]= $cdata; 
		}

		$jsondata=$aa;;


		echo json_encode(utf8ize($jsondata));




	} 




	function calllabel($labeldata) {
 if($labeldata=='column'){http://www.aqusagtechnologies.com/demo/

 	$data=mssql_query("SELECT CustomerTypeName FROM dbo.BP2_DistinctAccountDetails_1 WHERE BrokerID = '".$_SESSION['life_user_id']."'
 		");
 	$i=1;

 	while ($result= mssql_fetch_assoc($data)){
 		if($i==1){
 			echo $result['CustomerTypeName'];
 		}
 		$i++; 



 	}

 }




}














?>
<div class="col-md-12 ">

	<div class="row right_btns"> 
		<!-- Newly COmmented -->
		<!-- <div class=" advnc search"  style="float: right;position: relative;z-index: 999;margin: 10px 10px 10px 0px;text-transform: uppercase;margin-right:5px;"><a class="btn-sm btn btn-primary"  style=" background-color: #76838f; border: 1px solid #76838f; "  href="dashboard.php">VIEW MY ACCOUNTS ONLY</a></div>
		<div class=" advnc search"  style="float:right;position: relative;z-index: 999;margin: 10px 10px 10px 0px;text-transform: uppercase;"><a class="btn-sm btn btn-primary" href="dashboard.php?utype=downline">VIEW MY DOWNLINE ACCOUNTS</a></div>
		<div class=" advnc search" style="float:right;position: relative;z-index: 999;margin: 10px 10px 10px 0px;text-transform: uppercase;"  ><a class="btn-sm btn btn-primary" href="dashboard.php?utype=parent">VIEW ALL ACCOUNTS</a></div>-->
	</div>







         <!--<div class="row right_btns"><button type="button" class="btn btn-primary blue">View All Accounts</button>
          <button type="button" class="btn btn-primary blue">View My Downline Accounts</button>
          <button type="button" class="btn btn-primary blue active">View My Accounts Only</button></div>
          
      -->         



  </div>

  <div class="col-md-12 dash_brd">
  	<div class="panel-body">
  		<div class="row row-lg">


  			<div class="col-md-6">
  				<!-- Example Line -->
  				<div class="example-wrap margin-md-0">
  					<h4 class="example-title">Number Of Accounts By Service State</h4>
  					<!-- <p>Use function: <code>Morris.Line(options)</code> to generate chart.</p> -->
  					<div class="example">
  						<div id="exampleMorrisLine"></div>
  					</div>
  				</div>
  				<!-- End Example Line -->
  			</div> 
  			<div class="col-md-6">
  				<!-- Example Donut -->
  				<div class="example-wrap">
  					<h4 class="example-title">Number of Accounts By Utility</h4>
  					<!--<p>Create a Donut chart using <code>Morris.Donut(options)</code>.</p> -->
  					<div class="example">
  						<div id="exampleMorrisDonut"></div>
  					</div>
  				</div>
  				<!-- End Example Donut -->
  			</div>




      <!-- <div class="col-md-6">
            
              <div class="example-wrap">
                <h4 class="example-title">ACCOUNTS BY TYPE AND STATUS</h4>
               <p>Create an area chart using: <code>Morris.Area(options)</code>.</p>
                <div class="example">
                  <div id="exampleMorrisArea"></div>
                </div>
              </div>
             
          </div>  -->
      </div>
  </div>
</div>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>

<div class="bg_clr"></div>

<div class="col-md-12 dash_brd no_mgn">
	<div class="panel-body">
		<div class="row row-lg">


			<div class="col-md-6">
				<!-- Example Bar -->
				<div class="example-wrap margin-md-0">
					<h4 class="example-title">Commercial Accounts By Status</h4>
					<!-- <p>Create bar charts using <code>Morris.Bar(options)</code>.</p> -->
					<div class="example">
						<div id="exampleMorrisBarcomm"></div>
					</div>
				</div>
				<!-- End Example Bar -->
			</div>
			<div class="col-md-6">
				<!-- Example Bar -->
				<div class="example-wrap margin-md-0">
					<h4 class="example-title">Residential Accounts By Status</h4>
					<!-- <p>Create bar charts using <code>Morris.Bar(options)</code>.</p> -->
					<div class="example">
						<div id="exampleMorrisBarres"></div>
					</div>
				</div>
				<!-- End Example Bar -->
			</div>

		</div> 

	</div>
</div> 




<!-- Core  -->
<script src="global/vendor/jquery/jquery.min.js"></script>
<script src="global/vendor/bootstrap/bootstrap.min.js"></script>
<script src="global/vendor/animsition/animsition.min.js"></script>
<script src="global/vendor/asscroll/jquery-asScroll.min.js"></script>
<script src="global/vendor/mousewheel/jquery.mousewheel.min.js"></script>
<script src="global/vendor/asscrollable/jquery.asScrollable.all.min.js"></script>
<script src="global/vendor/ashoverscroll/jquery-asHoverScroll.min.js"></script>

<!-- Plugins -->
<script src="global/vendor/switchery/switchery.min.js"></script>
<script src="global/vendor/intro-js/intro.min.js"></script>
<script src="global/vendor/screenfull/screenfull.min.js"></script>
<script src="global/vendor/slidepanel/jquery-slidePanel.min.js"></script>

<!-- Plugins For This Page -->
<script src="global/js/raphael-min.js"></script>
<script src="global/js/morris.min.js"></script>

<!-- Scripts -->
<script src="global/js/core.min.js"></script>
<script src="topbar/assets/js/site.min.js"></script>

<script src="topbar/assets/js/sections/menu.min.js"></script>
<script src="topbar/assets/js/sections/menubar.min.js"></script>
<script src="topbar/assets/js/sections/sidebar.min.js"></script>

<script src="global/js/configs/config-colors.min.js"></script>
<script src="topbar/assets/js/configs/config-tour.min.js"></script>
<script src="global/js/components/asscrollable.min.js"></script>
<script src="global/js/components/animsition.min.js"></script>
<script src="global/js/components/slidepanel.min.js"></script>
<script src="global/js/components/switchery.min.js"></script>
<script type="text/javascript">

	!function(document,window,$)
	{"use strict";var Site=window.Site;
	$(document).ready(function($){Site.run()}),




	/*function(){Morris.Area({element:"exampleMorrisArea",data:[{y:"AB",a:2070},{y:"Ca",a:2170}],xkey:"y",ykeys:["a"],labels:["Series A"],behaveLikeLine:!0,resize:!0,pointSize:3,smooth:!0,gridTextColor:"#474e54",gridLineColor:"#eef0f2",goalLineColors:"#e3e6ea",gridTextFamily:$.configs.get("site","fontFamily"),gridTextWeight:"300",numLines:7,gridtextSize:14,lineWidth:1,fillOpacity:.1,lineColors:[$.colors("primary",600),$.colors("green",600)]})}(),*/
	function(){Morris.Bar({element:"exampleMorrisBarres",data:<?php callgraphresidential()?>,xkey:"y",ykeys:["a"],labels:["Residential"],barGap:6,xLabelMargin: 10,barSizeRatio:.35,smooth:!0,gridTextColor:"#474e54",gridLineColor:"#eef0f2",goalLineColors:"#e3e6ea",gridTextFamily:$.configs.get("site","fontFamily"),gridTextWeight:"300",numLines:6,gridtextSize:14,resize:!0,barColors:[$.colors("red",500),$.colors("grey",400)]})}(),
	function(){Morris.Bar({element:"exampleMorrisBarcomm",data:<?php callgrapcommercial()?>,xkey:"y",ykeys:["a"],labels:["Commercial"],barGap:6,xLabelMargin: 10,barSizeRatio:.35,smooth:!0,gridTextColor:"#474e54",gridLineColor:"#eef0f2",goalLineColors:"#e3e6ea",gridTextFamily:$.configs.get("site","fontFamily"),gridTextWeight:"300",numLines:6,gridtextSize:14,resize:!0,barColors:[$.colors("red",500),$.colors("grey",400)]})}(),
	function(){Morris.Donut({element:"exampleMorrisDonut",data:<?php callgraph('Donut')?>,resize:!0,colors:[$.colors("red",500),$.colors("primary",500),$.colors("grey",400),$.colors("green",500),$.colors("yellow",500),$.colors("pink",500),$.colors("purple",500)]})}(), 
	function(){Morris.Donut({element:"exampleMorrisLine",data:<?php callgraphpie('pie')?>,resize:!0,colors:[$.colors("red",500),$.colors("primary",500),$.colors("grey",400)]})}()}



	(document,window,jQuery);
</script>

<script type="text/javascript">
	$(document).ready(function(){
		if($('#exampleMorrisDonut').html()==''){

			$('#exampleMorrisDonut').html('<p>No data available</p>');

		}
		if($('#exampleMorrisBar').html()==''){

			$('#exampleMorrisBar').html('<p>No data available</p>');

		}
		if($('#exampleMorrisLine').html()==''){

			$('#exampleMorrisLine').html('<p>No data available</p>');

		}

	});
</script>


<?php } ?>    


<!-- /footer content -->
</div>
<footer>
	<?php  include("footer.php");?>
</footer>
</div>


</body>
</html>