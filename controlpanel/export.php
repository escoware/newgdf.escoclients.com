<?php
	
	error_reporting(-1);

	include("../include/config.php");

	if($_SESSION['life_user_id'] == ''){
		echo 'fail';
		exit();
	}

	/**
	 *
	 * This file is used to export the data selected by user
	 * File format will be .csv, excel(.xlsx,.xls), .pdf 
	 *
	 * @param File Type $type
	 * @param Query $image
	 * @param page Name $page
	 *
	 * @return Redirect 
	 * @author Satish Deshmukh
	 *
	 */

	// Check if method is post, used to visit this page
	if( $_SERVER['REQUEST_METHOD'] == 'POST' ){

		// Valid pages array
		$valid_pages = ['accounts','paynopay','commission'];

		// Valid extensions array
		$valid_ext = ['pdf','xlsx','csv'];

		// Check if all param are present and valid
		if( isset($_REQUEST['task']) && ($_REQUEST['task'] == 'start') ){
			
			// Check if session is set
			if( isset($_SESSION['export']) && is_array($_SESSION['export']) && !empty($_SESSION['export']) ){

				$session = $_SESSION['export'];

				if( in_array($session['type'], $valid_ext) === false ){
					echo 'failure';
					exit;
				}

				if( in_array($session['page'], $valid_pages) === false ){
					echo 'failure';
					exit;
				}

				// decode query
				$query = base64_decode($session['image']);

				switch ($session['type']) {
					case 'pdf':
							downloadPdf($query, $session['type'], $session['page']);
						break;
					
					case 'xlsx':
							echo downloadExcel($query, $session['type'], $session['page']);
						break;

					case 'csv':
							echo downloadCsv($query, $session['type'], $session['page']);
						break;

					default:
						// header('Location: '.$_SERVER['HTTP_REFERER']);
						break;
				}

			}
		}

	}

	function downloadCsv($q, $t, $p){
		$csvName = strtotime(date('Y-m-d H:i:s'))."_".$p."_export.csv";

		$base_root_folder = $_SERVER['CONTEXT_DOCUMENT_ROOT'].'/controlpanel/exports/'.$_SESSION['life_user_id'];

		if( checkUserFolder($base_root_folder) ){
			$user_folder_export_file = $base_root_folder.'/'.$csvName;

			$fp = fopen($user_folder_export_file, 'w');

			$resource = mssql_query($q);

			while ($export = mssql_fetch_assoc($resource)) {
			    if (!isset($headings))
			    {
			        $headings = array_keys($export);
			        fputcsv($fp, $headings, ',', '"');
			    }
			    fputcsv($fp, $export, ',', '"');
			}

			fclose($fp);

			unset($_SESSION['export']);

			return 'exports/'.$_SESSION['life_user_id'].'/'.$csvName;
		}
		else{
			return false;
		}

	}

	function downloadExcel($q, $t, $p){

		$csvName = strtotime(date('Y-m-d H:i:s'))."_".$p."_export.xlsx";

		$base_root_folder = $_SERVER['CONTEXT_DOCUMENT_ROOT'].'/controlpanel/exports/'.$_SESSION['life_user_id'];

		if( checkUserFolder($base_root_folder) ){
			$user_folder_export_file = $base_root_folder.'/'.$csvName;

			$resource = mssql_query($q);

			include_once("include/xlsxwriter.class.php");

			$writer = new XLSXWriter();

			while ($export = mssql_fetch_assoc($resource)) {

				if (!isset($headings)){

			        $headings = array_keys($export);
			        
			        $e = [];

			        foreach ($headings as $h) {
			        	$e[$h] = 'string';
			        }

		        	$writer->writeSheetHeader('Sheet1', $e);	
			    }
				
				$writer->writeSheetRow('Sheet1', array_values($export) );
				
			}

			$writer->writeToFile($base_root_folder.'/'.$csvName);

			unset($_SESSION['export']);

			return 'exports/'.$_SESSION['life_user_id'].'/'.$csvName;
		}
		else{
			return false;
		}
	}

	function downloadPdf($q, $t, $p){
		$csvName = strtotime(date('Y-m-d H:i:s'))."_".$p."_export.pdf";

		$base_root_folder = $_SERVER['CONTEXT_DOCUMENT_ROOT'].'/controlpanel/exports/'.$_SESSION['life_user_id'];
		$user_folder_export_file = $base_root_folder.'/'.$csvName;

		if( checkUserFolder($base_root_folder) ){

			$user_folder_export_file = $base_root_folder.'/'.$csvName;

			$resource = mssql_query($q);

			$html = '<table><thead><tr>';

			for($i = 0; $i < mssql_num_fields($resource); $i++) {
				$html .= '<th>'.mssql_fetch_field($resource, $i)->name.'</th>';
			}

			$html .= '</tr></thead><tbody>';

			$a = 0;

			while ( $export = mssql_fetch_assoc($resource)) {

				$html .= '<tr>';

				foreach ($export as $ex) {

					$html .= '<td>'.$ex.'</td>';

				}

				$html .= '</tr>';

				if( $a == 10 ){
					// break;
				}

				$a++;
			}

			$html .= '</tbody></table>';

			require_once 'include/vendor/autoload.php';
			// Create an instance of the class:
			$condif = [
				'mode' => 'utf-8',
				'format' => 'A4',
				'default_font_size' => 16,
				'default_font' => '',
				'margin_left' => 15,
				'margin_right' => 15,
				'margin_top' => 16,
				'margin_bottom' => 16,
				'margin_header' => 9,
				'margin_footer' => 9,
				'orientation' => 'P',
			];
			
			$mpdf = new Mpdf\Mpdf($condif);

			$mpdf->WriteHTML($html);

			//save the file put which location you need folder/filname
			$mpdf->Output($user_folder_export_file,'F');
		}
	}

	function checkUserFolder($root){

		if( !is_dir($root) ){

			if( mkdir($root) ){
				return true;
			}

			return false;
		}

		return true;
	}

	function get_include_contents($filename) {
	    if (is_file($filename)) {
	        ob_start();
	        include_once $filename;
	        return ob_get_clean();
	    }
	    return false;
	}
?>