<?php 
include("../include/config.php");
include("../include/simpleimage.php");
 validate_admin();
if($_REQUEST['submitForm']=='yes'){
	$title= mysql_real_escape_string($_POST['title']);
        
        if($_FILES['photo']['size']>0 && $_FILES['photo']['error']=='' ){
            $img=time().$_FILES['photo']['name'];
            move_uploaded_file($_FILES['photo']['tmp_name'],"../upload_images/newsletter/".$img);
            copy("../upload_images/newsletter/".$img,"../upload_images/newsletter/thumb/".$img);
            copy("../upload_images/newsletter/".$img,"../upload_images/newsletter/tiny/".$img);
            $Myimage=new SimpleImage();
            $Myimage->load("../upload_images/newsletter/thumb/".$img);
            $Myimage->resize(100,100);
            $Myimage->save("../upload_images/newsletter/thumb/".$img);
            $Myimage->load("../upload_images/newsletter/tiny/".$img);
            $Myimage->resize(200,200);
            $Myimage->save("../upload_images/newsletter/tiny/".$img);
            
           }
        
        
	if($_REQUEST['id']==''){
	  $obj->query("insert into $tbl_newsletter set title='$title',photo='$img',status=1 ");
	  $_SESSION['sess_msg']='Newsletter  added successfully';  
	  
       }else{ 	  
	   $sql=" update $tbl_newsletter set title='$title' ";	
           if($img){
             $imageArr=$obj->query("select photo from $tbl_newsletter where id='".$_REQUEST['id']."' ");  
             $resultImage=$obj->fetchNextObject($imageArr);
             @unlink("../upload_images/newsletter/".$resultImage->photo);
             @unlink("../upload_images/newsletter/thumb/".$resultImage->photo);
             @unlink("../upload_images/newsletter/tiny/".$resultImage->photo);
             $sql.=" , photo='$img' ";  
           }
	   $sql.=" where id='".$_REQUEST['id']."'";
	   $obj->query($sql);
	   $_SESSION['sess_msg']='Newsletter updated successfully';   
        }
   header("location:newsletter-list.php");
   exit();
	
}
if($_REQUEST['id']!=''){
$sql=$obj->query("select * from $tbl_newsletter where id='".$_REQUEST['id']."' ",$debug=-1);
$result=$obj->fetchNextObject($sql);	
}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo SITE_TITLE; ?></title>
    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    
    <!-- Custom Theme Style -->
    <link href="css/custom.css" rel="stylesheet">
  </head>
  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <?php  include("left-menu.php");?>
        </div>
        <!-- top navigation -->
        <div class="top_nav">
          <?php include("top-menu.php"); ?>
        </div>
        <!-- /top navigation -->
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                  <div class="col-md-9">
                      <h3>
                      Add Newsletter
                  </h3></div>
                  <div class="col-md-3">
                     <button type="button" class="btn btn-primary pull-right" onClick="location.href='newsletter-list.php'">View Newsletter</button>
                     </div>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                      <form class="form-horizontal form-label-left" enctype="multipart/form-data" method="post" novalidate >
                    <input type="hidden" name="submitForm" value="yes">
                      <p>Fields mark with (*) are required </p>
                      
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Title <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="title" class="form-control col-md-7 col-xs-12"  name="title" placeholder="Newsletter Title" required="required" type="text" value="<?php echo stripslashes($result->title); ?>"> 
                        </div>
                      </div>
                      
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Image <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input  class=" col-md-7 col-xs-12"  name="photo" type="file" > <br/>
                          <?php if(is_file("../upload_images/newsletter/thumb/".$result->photo)){?>
                               <img src="../upload_images/newsletter/thumb/<?php echo $result->photo; ?>"/>
                               <?php }?>
                        </div>
                      </div>
                     
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-md-offset-3">
                          <button type="reset" class="btn btn-primary">Cancel</button>
                          <button id="send" type="submit" class="btn btn-success">Submit</button>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
        <!-- footer content -->
        <footer>
         <?php  include("footer.php");?>
        </footer>
        <!-- /footer content -->
      </div>
    </div>
    <!-- jQuery -->
    <script src="../vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- validator -->
    <script src="../vendors/validator/validator.min.js"></script>
    <!-- Custom Theme Scripts -->
    <script src="js/custom.js"></script>
    <!-- validator -->
    <script>
      // initialize the validator function
      validator.message.date = 'not a real date';
      // validate a field on "blur" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
      $('form')
        .on('blur', 'input[required], input.optional, select.required', validator.checkField)
        .on('change', 'select.required', validator.checkField)
        .on('keypress', 'input[required][pattern]', validator.keypress);
      $('.multi.required').on('keyup blur', 'input', function() {
        validator.checkField.apply($(this).siblings().last()[0]);
      });
      $('form').submit(function(e) {
        e.preventDefault();
        var submit = true;
        // evaluate the form using generic validaing
        if (!validator.checkAll($(this))) {
          submit = false;
        }
        if (submit)
          this.submit();
        return false;
      });
    </script>
    <!-- /validator -->
  </body>
</html>