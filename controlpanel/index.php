<?php 
include("../include/config.php");
if($_SESSION['life_user_id']!=''){
header("location:dashboard.php");
exit();

}

if($_REQUEST['submitlogfrm']=='yes')
{
$log_email=$_REQUEST['log_email'];
$log_password=md5($_REQUEST['log_password']);

 	$query=mssql_query("SELECT
 *
FROM  dbo.Brokers Where Email='".$log_email."' and Password='".$log_password."'
");
   //echo mssql_get_last_message();
if(mssql_num_rows($query) > 0){

$record = mssql_fetch_assoc($query);
 	

	
	   $_SESSION['life_user_id']=$record['ID'];
				   $_SESSION['life_user_name']=$record['Email'];
           $_SESSION['life_user_fname']=$record['FirstName'];
 $_SESSION['life_user_lname']=$record['LastName'];
				
					$_SESSION['life_user_email']=$record['Username'];
				
	 if($_REQUEST['back']=='') {
	 header("location:dashboard.php");
	 } else { 
	  header("location:".$_REQUEST['back']);
	 }

 } else {
	 $_SESSION['ses_login_errmsg']='Invalid email id or password';

	  

 
 
 
 }
}



 
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo SITE_TITLE; ?></title>
    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
    <!-- Custom Theme Style -->
    <link href="css/custom.css" rel="stylesheet">
   
  </head>
  <body style="background-color: rgba(38,50,56,.6);">
    <div class="">
      <a class="hiddenanchor" id="toregister"></a>
      <a class="hiddenanchor" id="tologin"></a>
      <div id="wrapper">
        <div id="login" class=" form">
          <section class="login_content">
           
            <form method="post"  class="form-horizontal form-label-left" action="" novalidate>
            <input type="hidden" name="submitlogfrm" value="yes">
              <div class="brnd_logo"> <img class="brand-img" src="<?php echo LOGO_PATH?>" alt="Logo">
              <h2 class="brand-text"><?php echo SITE_TITLE; ?></h2></div>
              <p class="sign">Please sign into your account</p>
              <?php if($_SESSION[sess_msg]!=''){ ?>
              <div class="form-group error-msg"><?php echo $_SESSION[sess_msg];$_SESSION[sess_msg]=''; ?></div>
              <?php } ?>
              <div class="item form-group">
                <input type="text" name="log_email" id="username" class="form-control" placeholder="Username" required />
              </div>
              <div class="item form-group">
                <input type="password" name="log_password" id="password" class="form-control" placeholder="Password" required />
              </div>
              <div>
              
                <button  type="submit" class="btn btn-default lgn_submt">Sign in</button>
               <!-- <a class="reset_pass" href="#">Lost your password?</a> -->
              </div>
              <div class="clearfix"></div>
             
                
               
                <br />
                <div class="lgn_footer">
                  
                  <p>©<?php echo date('Y'); ?> All Rights Reserved.  </p>
                  <!--<div class="social">
          <a href="javascript:void(0)">
            <i class="fa fa-twitter" aria-hidden="true"></i>
          </a>
          <a href="javascript:void(0)">
            <i class="fa fa-facebook" aria-hidden="true"></i>
          </a>
          <a href="javascript:void(0)">
            <i class="fa fa-dribbble" aria-hidden="true"></i>
          </a>
        </div>
                </div>-->
             
            </form>
          </section>
        </div>
 <!-- jQuery -->
    <script src="../vendors/jquery/dist/jquery.min.js"></script>
    <!-- validator -->
    <script src="../vendors/validator/validator.min.js"></script>
 
    <!-- validator -->
    <script>
      // initialize the validator function
      validator.message.date = 'not a real date';
      // validate a field on "blur" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
      $('form')
        .on('blur', 'input[required], input.optional, select.required', validator.checkField)
        .on('change', 'select.required', validator.checkField)
        .on('keypress', 'input[required][pattern]', validator.keypress);
      $('.multi.required').on('keyup blur', 'input', function() {
        validator.checkField.apply($(this).siblings().last()[0]);
      });
      $('form').submit(function(e) {
        e.preventDefault();
        var submit = true;
        // evaluate the form using generic validaing
        if (!validator.checkAll($(this))) {
          submit = false;
        }
        if (submit)
          this.submit();
        return false;
      });
    </script>
    <!-- /validator -->
      </div>
    </div>
  </body>
</html>