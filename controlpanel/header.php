<div class="top_strip">
<div class="container">
<div class="row">
              
              <div class="col-md-6">
                
                <div class="top_number">
                  Oders On Call:  <a href="tel:844-711-HAIR">844-711-HAIR</a> or <a href="tel:844-711-4247">844-711-4247</a>
                </div>

              </div>

              <div class="col-md-6">
                <div class="social-header"> <a href="#"><i class="fa fa-facebook"></i></a> <a href="#"><i class="fa fa-twitter"></i></a> <a href="#"><i class="fa fa-google-plus"></i></a> <a href="#"><i class="fa fa-instagram"></i></a> <a href="#"><i class="fa fa-linkedin"></i></a> <a href="#"><i class="fa fa-pinterest"></i></a> </div>

              </div>

              </div>
</div>
</div>

            <header id="header" class="header-page">
              <div class="container">
              


                <div class="row">
                  <div class="col-sm-4 col-xs-12">
                    <div class="logo"> <a href="#" title=""> <img src="images/logo.png" alt="" /> </a> </div>
                  </div>
                  <div class="col-sm-8 col-xs-12">
                    <div class="advanced-search top-search">
                      <div class="search-cat"> <span class="cat-search box-cat-toggle"></span>
                        <div class="wrap-scrollbar">
                          <div  class="scrollbar">
                            <ul>
                              <li class="level-0">All Categories </li>
                              <li class="level-1"> Default Category </li>
                              <!--<li class="level-2"> Corssbody </li>
                              <li class="level-3"> New Arrivals </li>
                              <li class="level-3"> Tops & Blouses </li>
                              <li class="level-3"> Pants & Denim </li>
                              <li class="level-3"> Dresses & Skirts </li>
                              <li class="level-2"> Satchel </li>
                              <li class="level-3"> New Arrivals </li>
                              <li class="level-3"> Shirts </li>
                              <li class="level-3"> Tees, Knits and Polos </li>
                              <li class="level-3"> Pants & Denim </li>
                              <li class="level-3"> Blazers </li>
                              <li class="level-2"> Totes </li>
                              <li class="level-3"> Eyewear </li>
                              <li class="level-3"> Jewelry </li>
                              <li class="level-3"> Shoes </li>
                              <li class="level-3"> Bags & Luggage </li>-->
                            </ul>
                          </div>
                        </div>
                      </div>
                      <div class="search-form search-form2">
                        <form id="search_mini_form">
                        
                          <input size="30" id="search" type="text" name="q" class="input-text" value="Enter key search..."  />
                          <input type="submit" value=""/>
                        </form>
                      </div>
                    </div>
                    <script type="text/javascript">	
	jQuery( document ).ready(function($) {
		$('.scrollbar').slimScroll({
			width : '200px',
			height: '200px',
			alwaysVisible : true,
			wheelStep: 2,
			touchScrollStep: 50,
		});
		$("body").click(function() {
			$('.wrap-scrollbar').hide();
		});
		
		$(".advanced-search").click(function(e) {
			e.stopPropagation();
		});		
		
		$('.scrollbar li').click(function(){
			var x = $('input',$(this)).attr('value');		
			$('.cat-value').val(x);
			//$('.cat-search').text($(this).text());
		});
		$('.wrap-scrollbar').hide();		
		$('.wrap-scrollbar li,.cat-search').click(function(e){
			$('.wrap-scrollbar').slideToggle('fast');
		});		
		
	});
</script> </div>
                </div>
                <div class="top-nav">
                  <div class="row">
                    <div class="col-md-10 col-sm-6 col-xs-6">
                      <div class="magemenu-menu nav-exploded navbar vt-magemenu" >
                        <div class="box-right main-nav main-nav3">
                          <ul class="nav-exploded explodedmenu">
                          <li id="menu5" class="menu lblnew menu01"> <a href="#"> <span>Home</span> </a> </li>
                            <li id="menu4" class="menu parentMenu  actived-default"> <a href="#"> <span>Products</span> </a>
                              <div id="popup4" class="explodedmenu-menu-popup">
                                <div class="exploded-cms-block static-block">
                                  <div class="block-menu-01">
                                    <div class="item">
                                      <div class="inner">
                                        <h2 class="i0"><a href="#"><span>Hair Extensions</span></a></h2>
                                        <ul>
                                          <li class="first"><a href="#"><img src="images/ca-b1.png" alt="banner"/></a></li>
                                          <li><a href="#"><span>Invisi-Tab Remy </span></a></li>
                                          <li><a href="#"><span>Fusion</span></a></li>
                                          <li><a href="#"><span>I-Tip</span></a></li>
                                          <li><a href="#"><span>Clip-In</span></a></li>
                                          <li class=""><a href="h#"><span>Tape-in</span></a></li>
                                        </ul>
                                      </div>
                                    </div>
                                    <div class="item">
                                      <div class="inner">
                                        <h2 class="i1"><a href="h#"><span>Mannequins</span></a></h2>
                                        <ul>
                                          <li class="first"><a href="#"><img src="images/ca-b2.png" alt="banner"/></a></li>
                                          <li><a href="#"><span>Virgin Hair Emma</span></a></li>
                                          <li><a href="#"><span>Virgin Hair Emily</span></a></li>
                                          <li><a href="#"><span>Miniature Courtney</span></a></li>
                                          <li ><a href="#"><span>Nicki Child</span></a></li>
                                          <li ><a href="#"><span>Jade Afro Kinky</span></a></li>
                                        </ul>
                                      </div>
                                    </div>
                                    <div class="item">
                                      <div class="inner">
                                        <h2 class="i2"><a href="#"><span>Wigs</span></a></h2>
                                        <ul>
                                          <li class="first"><a href="#"><img src="images/ca-b3.png" alt="banner"/></a></li>
                                          <li><a href="#"><span>Laura</span></a></li>
                                          <li><a href="#"><span>Nancy</span></a></li>
                                          <li><a href="#"><span>Susan S</span></a></li>
                                          <li><a href="#"><span></span>Alice</a></li>
                                          <li><a href="#"><span></span>Cara</a></li>
                                        </ul>
                                      </div>
                                    </div>
                                    <div class="item">
                                      <div class="inner">
                                        <h2 class="i3"><a href="#"><span>Brushes</span></a></h2>
                                        <ul>
                                          <li class="first"><a href="#"><img src="images/ca-b4.png" alt="banner"/></a></li>
                                          <li><a href="#"><span>Boar/Nylon Round</span></a></li>
                                          <li><a href="#"><span>Beech Wood Handle</span></a></li>
                                          <li><a href="#"><span>Boar & Nylon Bristle</span></a></li>
                                          <li><a href="#"><span>Paddle Brush</span></a></li>
                                          <li><a href="#"><span>Vent Brush</span></a></li>
                                        </ul>
                                      </div>
                                    </div>
                                    <div class="item last">
                                      <div class="inner">
                                        <h2 class="i4"><a href="#"><span>Hair Tools</span></a></h2>
                                        <ul>
                                          <li class="first"><a href="#"><img src="images/ca-b5.png" alt="banner"/></a></li>
                                          <li><a href="#"><span>Luxe Air</span></a></li>
                                          <li><a href="#"><span>Digital Flat Iron </span></a></li>
                                          <li><a href="#"><span>Heat Brush Pro</span></a></li>
                                          <li><a href="#"><span>Shine 'n Silky Conditioner</span></a></li>
                                          <li><a href="#"><span>Deluxe Hydrating Shampoo</span></a></li>
                                        </ul>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </li>
                            
                            <li id="menu6" class="menu menu03 lblhot"> <a href="#"> <span>Brands</span> </a>
                              
                            </li>
                            <li id="menu7" class="menu menumain"> <a href="#"> <span>Information</span> </a>
                              
                            </li>
                            <li id="menu8" class="menu  menu03"> <a href="#"> <span>Before - After</span> </a> </li>
                           
                            <li id="menu32" class="menu"> <a href="#"> <span>Contact</span> </a> </li>
                          </ul>
                        </div>
                      </div>
                      <div id="mobile-nav" class="hide-des m1" >
                        <button type="button" class="btn-sidebar btn-mobile-menu"></button>
                        <div class="mobile-sidebar">
                          <button type="button" class="close-menu"><span>close menu</span></button>
                          <ul class="nav-menu clearfix">
                            <li > <a  href="#" title="Home"><span>Home</span></a> </li>
                            <li class="level0 nav-1 parent" onmouseover="toggleMenu(this,1)" onmouseout="toggleMenu(this,0)"> <a href="#"> <span>Corssbody</span> </a>
                              <ul class="level0">
                                <li class="level1 nav-1-1 first"> <a href="#"> <span>New Arrivals</span> </a> </li>
                                <li class="level1 nav-1-2"> <a href="#"> <span>Tops &amp; Blouses</span> </a> </li>
                                <li class="level1 nav-1-3"> <a href="#"> <span>Pants &amp; Denim</span> </a> </li>
                                <li class="level1 nav-1-4 last"> <a href="#"> <span>Dresses &amp; Skirts</span> </a> </li>
                              </ul>
                            </li>
                            <li class="level0 nav-2 parent" onmouseover="toggleMenu(this,1)" onmouseout="toggleMenu(this,0)"> <a href="#"> <span>Satchel</span> </a>
                              <ul class="level0">
                                <li class="level1 nav-2-1 first"> <a href="#"> <span>New Arrivals</span> </a> </li>
                                <li class="level1 nav-2-2"> <a href="#"> <span>Shirts</span> </a> </li>
                                <li class="level1 nav-2-3"> <a href="#"> <span>Tees, Knits and Polos</span> </a> </li>
                                <li class="level1 nav-2-4"> <a href="#"> <span>Pants &amp; Denim</span> </a> </li>
                                <li class="level1 nav-2-5 last"> <a href="#"> <span>Blazers</span> </a> </li>
                              </ul>
                            </li>
                            <li class="level0 nav-3 parent" onmouseover="toggleMenu(this,1)" onmouseout="toggleMenu(this,0)"> <a href="#"> <span>Totes</span> </a>
                              <ul class="level0">
                                <li class="level1 nav-3-1 first"> <a href="#"> <span>Eyewear</span> </a> </li>
                                <li class="level1 nav-3-2"> <a href="#"> <span>Jewelry</span> </a> </li>
                                <li class="level1 nav-3-3"> <a href="#"> <span>Shoes</span> </a> </li>
                                <li class="level1 nav-3-4 last"> <a href="#"> <span>Bags &amp; Luggage</span> </a> </li>
                              </ul>
                            </li>
                            <li class="level0 nav-4 parent" onmouseover="toggleMenu(this,1)" onmouseout="toggleMenu(this,0)"> <a href="#"> <span>Ladies Wallets</span> </a>
                              <ul class="level0">
                                <li class="level1 nav-4-1 first"> <a href="#"> <span>Books &amp; Music</span> </a> </li>
                                <li class="level1 nav-4-2"> <a href="#"> <span>Bed &amp; Bath</span> </a> </li>
                                <li class="level1 nav-4-3"> <a href="#"> <span>Electronics</span> </a> </li>
                                <li class="level1 nav-4-4 last"> <a href="#"> <span>Decorative Accents</span> </a> </li>
                              </ul>
                            </li>
                            <li class="level0 nav-5 parent" onmouseover="toggleMenu(this,1)" onmouseout="toggleMenu(this,0)"> <a href="#"> <span>Sweatshirts</span> </a>
                              <ul class="level0">
                                <li class="level1 nav-5-1 first"> <a href="#"> <span>Women</span> </a> </li>
                                <li class="level1 nav-5-2"> <a href="#"> <span>Men</span> </a> </li>
                                <li class="level1 nav-5-3"> <a href="#"> <span>Accessories</span> </a> </li>
                                <li class="level1 nav-5-4 last"> <a href="#"> <span>Home &amp; Decor</span> </a> </li>
                              </ul>
                            </li>
                            <li class="level0 nav-6"> <a href="#"> <span>Jackets &amp; Coats</span> </a> </li>
                          </ul>
                        </div>
                        <script type="text/javascript">
        jQuery(document).ready(function($){
            $('body').append('<div class="block-slidebar"></div>');
            $('.block-slidebar').html($('.mobile-sidebar').html());
            $('.mobile-sidebar').remove(); 
            $('.block-slidebar').css('display','none') ;          
            $('.btn-sidebar').click(function(){
                if($('body').hasClass('show-menu')){
                    $('body').removeClass('show-menu');
                }else{
                    $('body').addClass('show-menu');
                }
            });
            $('.close-menu').click(function(){
                if($('body').hasClass('show-menu')){
                    $('body').removeClass('show-menu');
                }else{
                    $('body').addClass('show-menu');
                }
            });
            
        });
    </script> 
                      </div>
                    </div>
                    <div class="col-md-2 col-sm-6 col-xs-6">
                      <div class="header-info">
                        <div class="box-account-lc box"> <span class="account-lc-icon style-icon">icon account</span>
                          <div class="box-inner">
                            <ul class="links list-inline text-right">
                              <li class="first" ><a href="#" title="My Account" class="top-link-myaccount">My Account</a></li>
                              <li ><a href="#" title="My Wishlist" class="top-link-wishlist">My Wishlist</a></li>
                              <li ><a href="#" title="Checkout" class="top-link-checkout">Checkout</a></li>
                              <li class=" last" ><a href="#" title="Log In" class="top-link-login">Log In</a></li>
                            </ul>
                            <div class="block block-language form-language">
                              <div class="lg-cur"> <span class="style-lbl">language</span> <span class="style-lbl-home4"> <img src="images/flag_default.gif" alt="English"/> <span>English</span> </span> </div>
                              <ul>
                                <li> <a  class="selected" href="#"> <img src="images/flag_default.gif" alt="flag"/> <span>English</span> </a> </li>
                                <li> <a  href="#"> <img src="images/flag_french.gif" alt="flag"/> <span>French</span> </a> </li>
                                <li> <a  href="#"> <img src="images/flag_german.gif" alt="flag"/> <span>German</span> </a> </li>
                              </ul>
                            </div>
                            <div class="block block-currency">
                              <div class="item-cur"> <span class="style-lbl">Currency</span> 
                                <!--<span class="sym"></span>--> 
                                <span class="style-lbl-home4">USD </span> </div>
                              <ul>
                                <li> <a  href="#"><span class="cur_icon">&euro;</span> EUR</a> </li>
                                <li> <a  href="#"><span class="cur_icon">&yen;</span> JPY</a> </li>
                                <li> <a  class="selected" href="#"><span class="cur_icon">$</span> USD</a> </li>
                              </ul>
                            </div>
                          </div>
                        </div>
                        <div class="block block-cart box "> <span class="cart-icon style-icon"> <a class="total-item" href="#">0</a> </span> </div>
                      </div>
                      <!-- End Header Info --> 
                    </div>
                  </div>
                </div>
              </div>
            </header>
            <!-- End Header --> 