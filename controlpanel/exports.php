<?php


	include("../include/config.php");

	// set session variable for export
	if( $_SERVER['REQUEST_METHOD'] == 'POST' ){

		if($_SESSION['life_user_id']==''){
			echo 'fail';
			exit();
		}
		else{
			$_SESSION['export'] = [
				'page'  => $_REQUEST['page'],
				'image' => $_REQUEST['image'],
				'type'  => $_REQUEST['type']
			];

			echo 'success';
			exit;
		}
	}

	if($_SESSION['life_user_id']==''){
		header("location:index.php");
		exit();
	}

	/**
	  * This function is used to get the files from user's export folder 
	  * @param directory $dir
	  * @param Result ( reference array ) &$results
	  * @return Files Array $results
	  * @return Satish Deshmukh
	  */
	function getDirContents($dir, &$results = array()){
		$files = scandir($dir);

	    foreach($files as $key => $value){
	        $path = realpath($dir.DIRECTORY_SEPARATOR.$value);
	        if(!is_dir($path)) {
	            $results[] = $path;
	        } 
	        else if($value != "." && $value != "..") {
	            getDirContents($path, $results);
	            // $results[] = $path;
	        }
	    }

	    return $results;
	}

	$is_file_in_process = isset($_SESSION['export']) && is_array($_SESSION['export']) && !empty($_SESSION['export']);
?>

<!DOCTYPE html>
<html lang="en">

  	<head>
    	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

	    <!-- Meta, title, CSS, favicons, etc. -->
	    <meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">

	    <title><?php echo SITE_TITLE; ?></title>

	    <!-- Bootstrap -->
	    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

	    <!-- Font Awesome -->
	    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">

	    <!-- iCheck -->
	    <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet"> 
    	<link href="css/custom.css" rel="stylesheet">
    	<link href="css/buttons.dataTables.min.css" rel="stylesheet" type="text/css">

    	<style type="text/css">
    		.custom-input-group-class{
    			background-color: #fff;
    			cursor: pointer; 
    		}
    		input[type="text"]{
    			color: #000 !important;
    			padding: 5px !important;
    		}
    	</style>
  	</head>

  	<body class="nav-md">
  		<div class="container body">
  			<div class="main_container">
  				<div class="col-md-3 left_col">
		          	<?php 
		          		// Include Left Menu
		          		include("left-menu.php");
		          	?>
		        </div>

		        <!-- top navigation -->
		        <div class="top_nav">
		          	<?php 
		          		// Include Top Menu
		          		include("top-menu.php"); 
		          	?>
		        </div>

		        <!-- page content -->
		        <div class="right_col" role="main">

		        	<!--  First box div  -->
		        	<div class="col-md-12 col-lg-12 cnt_area">
		        		<?php if( $is_file_in_process ){ ?>
		        		<div class="x_panel" style="overflow: hidden;">
		        			<div class="x_title text-center" id="warning_text">
		        				<h4 style="color: grey;font-size: 20px; font-weight: 700;">
		        					CSV Generation is in process, Please do not close this page...
		        				</h4>
		        			</div>
		        		</div>
		        		<?php } ?>
		        		<div class="x_panel">
		              		<div class="x_title">
		                		<h4 class="m-b-20">
		                			Exports
		                		</h4>
		                	</div>

		                	<div class="x_content">
		                		<table id="datatable-responsive" class="table table-striped nowrap" cellspacing="0" width="100%">
		                			<thead>
		                				<tr>
		                					<th class="tbl_hdng">
		                						#
		                					</th>

		                					<th class="tbl_hdng">
		                						View Type
		                					</th>

		                					<th class="tbl_hdng">
		                						Export Type
		                					</th>

		                					<th class="tbl_hdng">
		                						Date
		                					</th>

		                					<th class="tbl_hdng">
		                						File
		                					</th>
		                				</tr>
		                			</thead>

		                			<tbody>
	                					<?php

	                						// Check if user ever exported any file
	                						$user_export_uri = $_SERVER['CONTEXT_DOCUMENT_ROOT'].'/staging/lifeenergy/controlpanel/exports/'.$_SESSION['life_user_id'];
												
	                						if( $is_file_in_process ){
	                							$sr_noe = 1;
												echo '<tr>
                										<td>
                											'.$sr_noe.'
                										</td>
	                									<td>
	                										'.$_SESSION["export"]["page"].'
	                									</td>
	                									<td>
	                										'.$_SESSION["export"]["type"].'
	                									</td>
	                									<td>
	                										'.date("m/d/Y").'
	                									</td>
	                									<td>
	                										<a href="javascript:void(0)" id="new_export_link"><img id="new_export_img" src="images/generating.gif" style="width:20%"/></a>
	                									</td>
	                								  </tr>';
											}

	                						if( $files = getDirContents($user_export_uri) ){

	                							foreach ($files as $key => $file) {
	                								$file_name_arr = explode('/', $file);
	                								$filename = end($file_name_arr);

	                								$file_name_arr = explode('_', end($file_name_arr));

	                								$view_type = $file_name_arr[1];
	                								$export_type = end(explode('.', $file_name_arr[2]));

	                								$date = date('M d, Y',$file_name_arr[0]);

	                								$sr_no = ( (isset($sr_noe) && $sr_noe == '1') ? 2 : 1 ) + $key;

	                								$download_link = 'exports/'.$_SESSION['life_user_id'].'/'.$filename;

	                								echo '<tr">
	                										<td>
	                											'.$sr_no.'
	                										</td>
		                									<td>
		                										'.$view_type.'
		                									</td>
		                									<td>
		                										'.$export_type.'
		                									</td>
		                									<td>
		                										'.$date.'
		                									</td>
		                									<td>
		                										<a href="'.$download_link.'" download="'.$file_name_arr[2].'">Click to download</a>
		                									</td>
		                								  </tr>';	
	                							}

	                						}
	                						if( !$is_file_in_process && empty(getDirContents($user_export_uri))){
	                							echo '
													<tr>
														<td></td>
														<td></td>
							                        	<td>
							                        		<font face="Arial, Helvetica, sans-serif" color="#FF0000" size="+1">
							                        			No data in column
							                        		</font>
							                        	</td>
							                        	<td></td>
							                        	<td></td>
							                    	</tr>
												';
	                						}
	                					?>
		                			</tbody>
		                		</table>
		                	</div>
		                </div>
		        	</div>
  				</div>
  				<!-- /page content -->

		        <!-- footer content -->
		        <footer>
		          	<?php
		          		include("footer.php");
		          	?>
		        </footer>
		        <!-- /footer content -->
		    </div>
		</div>
	</body>

	<!-- jQuery -->
    <script src="../vendors/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap -->
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Custom Theme Scripts -->
	<script src="js/custom.js"></script>

	<?php  

		if( isset($_SESSION['export']) && is_array($_SESSION['export']) && !empty($_SESSION['export']) ){ ?>

			<script type="text/javascript">
				function runAjax(){
					$.ajax({
						url : 'export.php',
						method : 'POST',
						data : {
							task : 'start'
						},
						success : function(res){
							if( res == 'fail' ){
								location.href = 'index.php';
							}
							else if( $.inArray(res,['fail','failure','success']) === -1 ){
								$(document).find('#warning_text h4').text('');
								$(document).find('#warning_text h4').html('CSV File Generated! Please <a href="'+res+'">click here</a> to download file');
								$(document).find('#new_export_img').remove();
								$(document).find('#new_export_link').attr('href',res).text('Click to download');
							}
						}
					});
				}

				$(document).ready(function(){
					runAjax();
				});
			</script>
	<?php
		}

	?>
</html>