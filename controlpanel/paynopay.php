<?php
	include("../include/config.php");

	if($_SESSION['life_user_id']==''){
		header("location:index.php");
		exit();
	}
	$num_of_rows_shown_in_table = $_REQUEST['rows'] ? $_REQUEST['rows'] : 100;	
	setlocale(LC_MONETARY, 'en_US');
?>

<!DOCTYPE html>
<html lang="en">

  	<head>
    	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

	    <!-- Meta, title, CSS, favicons, etc. -->
	    <meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">

	    <title><?php echo SITE_TITLE; ?></title>

	    <!-- Bootstrap -->
	    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

	    <!-- Font Awesome -->
	    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">

	    <!-- iCheck -->
	    <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet"> 
    	<link href="css/custom.css" rel="stylesheet">
    	<link href="css/buttons.dataTables.min.css" rel="stylesheet" type="text/css">

    	<style type="text/css">
    		.custom-input-group-class{
    			background-color: #fff;
    			cursor: pointer; 
    		}
    		input[type="text"]{
    			color: #000 !important;
    			padding: 5px !important;
    		}
    		thead{
    			background: #fff;
    		}
    	</style>
  	</head>

  	<body class="nav-md">
  		<div class="container body">
  			<div class="main_container">
  				<div class="col-md-3 left_col">
		          	<?php 
		          		// Include Left Menu
		          		include("left-menu.php");
		          	?>
		        </div>

		        <!-- top navigation -->
		        <div class="top_nav">
		          	<?php 
		          		// Include Top Menu
		          		include("top-menu.php"); 
		          	?>
		        </div>

		        <!-- page content -->
		        <div class="right_col" role="main">

		          	<div class="col-md-12 col-lg-12 col-lg-12 cnt_area">
		            	<div class="page-title">
		              		<div class="title_left">
		                		<h4 class="m-b-20"></h4>
		                	</div>
  							
  							<div class="">
                            <div class="x_title">
					                	<div class="col-md-9">
											<h2>My pay/Nopay Accounts</h2>
										</div>
										<div class="clearfix"></div>
									</div>
				              	<div class="col-md-9 col-lg-9 dt-buttons">
				              		<a class="dt-button buttons-copy buttons-html5 hide" tabindex="0" aria-controls="example" id="copy_paynopay" href="javascript:void(0)">
				                    	<span>Copy</span>
				                    </a>
				                    <a class="dt-button buttons-csv buttons-html5" tabindex="0" aria-controls="example" id="csv_paynopay" data-type="csv" href="javascript:void(0)">
				                    	<span>CSV</span>
				                    </a>
				                    <a class="dt-button buttons-excel buttons-html5" tabindex="0" aria-controls="example" id="excel_paynopay" data-type="xlsx" href="javascript:void(0)">
				                    	<span>Excel</span>
				                    </a>
				                    <a class="dt-button buttons-pdf buttons-html5 hide" tabindex="0" aria-controls="example" id="pdf_paynopay" data-type="pdf" href="javascript:void(0)">
				                    	<span>PDF</span>
				                    </a>
				                    <a class="dt-button buttons-print" tabindex="0" aria-controls="example" id="print_paynopay" data-source="datatable-responsive" data-type="print" href="javascript:void(0)">
				                    	<span>Print</span>
				                    </a>
				                    <form id="export-form" class="hide"></form>
								</div>
  								
  								<div class="col-md-3 col-sm-3 col-xs-12 form-group pull-right top_search hide">
					                <form method="post" class="" action="member-list.php">
					                  	<div class="input-group srch">
					                    	
					                    	<input type="text" name="keyword" class="form-control" value="<?php echo $_REQUEST['keyword']; ?>" placeholder="Search for...">
					                    	<span class="input-group-btn">
					                        	<div class=" advnc search" style="float:right;">
					                               	<a class="btn-sm btn btn-primary" href="commission_detail.php?comicycle=1034">Advanced Search/Sort</a>
					                            </div>
					                        </span>
					                    </div>
					                </form>
					            </div>
  							</div>
  						</div>

  						<div class="clearfix"></div>

  						<div class="row">
  							<div class="col-md-2 col-sm-6 col-xs-12">
  								Show&nbsp;&nbsp;
  								<select class="form-control" id="num_of_rows_shown" style="width: 80px; display: inline;">
  									<option value="100" <?php echo ($_REQUEST['rows'] == 100) ? 'selected="selected"' : ''; ?>>100</option>
  									<option value="200" <?php echo ($_REQUEST['rows'] == 200) ? 'selected="selected"' : ''; ?>>200</option>
  									<option value="500" <?php echo ($_REQUEST['rows'] == 500) ? 'selected="selected"' : ''; ?>>500</option>
  									<option value="1000" <?php echo ($_REQUEST['rows'] == 1000) ? 'selected="selected"' : ''; ?>>1000</option>
  								</select>
  								&nbsp;&nbsp;entries
  							</div>

  							<div class="col-md-12 col-sm-12 col-xs-12">
  								<div class="x_panel">
									<div class="x_content">
										<div class="clearfix"></div>
										<form name="frm" method="post" action="member-del.php" enctype="multipart/form-data">
											<?php 
												require_once 'include/exit_search_sort_view.php';
											?>
											<table id="datatable-responsive" class="table table-hover table-striped dt-responsive nowrap pay_on" cellspacing="0" width="100%">
												
												<thead>
													<tr role="row">
														<?php
															require_once 'include/paynopay/paynopay_thead.php';
														?>
													</tr>
												</thead>

												<?php
													require_once 'include/paynopay/data.php';
												?>
											</table>
											<!-- pagination -->
											<?php
												require_once 'include/paynopay/pagination.php';
											?>
										</form>

									</div>
  								
  								</div>
  							</div>
  						</div>	
  					</div>
  				</div>
  				<!-- /page content -->

		        <!-- footer content -->
		        <footer>
		          	<?php
		          		include("footer.php");
		          	?>
		        </footer>
		        <!-- /footer content -->

  			</div>
  		</div>

  		<!-- jQuery -->
	    <script src="../vendors/jquery/dist/jquery.min.js"></script>

	    <!-- Bootstrap -->
	    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>

	    <!-- Custom Theme Scripts -->
    	<script src="js/custom.js"></script>

    	<script type="text/javascript">
    		var vfg = "<?php echo base64_encode($rawsql_for_js); ?>";
    	</script>

    	<script type="text/javascript" src="include/paynopay/paynopay.js"></script>

    	<script type="text/javascript" src="js/fixedheader.js"></script>

    	<script type="text/javascript">
    		function pageTop(){
				return $(".nav_menu").height();
			}

    		$(document).ready(function(){
    			$('table#datatable-responsive').floatThead({
					top : pageTop,
					position: 'fixed'
				});
    		});

    		$(window).bind('load', function(){
    			$(document).find('.floatThead-container table thead').each(function(e,r){

    				var width = $(r).width();

    				$(document).find('#datatable-responsive tbody').each(function(t,y){
    					if( e == t ){
    						$(y).css('width',width+'px');
    					}
    				});

    			});
    		});
    	</script>
  	</body>

</html>